extern unsigned char fan_speed, fan_defaults, pwm_defaults;
extern unsigned int defaults[3], prog[3];
extern volatile unsigned char pwm_set;

void showPreset(unsigned char addr);
void ZZZ(unsigned char x, unsigned char y);
void EE_Dec_Large(unsigned int val, unsigned char x, unsigned char y);
void EE_Dec(unsigned int val, unsigned char x, unsigned char y);
void EE_Dec_Small(unsigned int val, unsigned char x, unsigned char y);
unsigned char EERead(unsigned char addr);
void EE_Char_Large(unsigned int eesym, unsigned char x, unsigned char y);
void EE_Char_Mid(unsigned char eesym, unsigned char x, unsigned char y);
void EE_Char_Small(unsigned char eesym, unsigned char x, unsigned char y);
void Menu_Selected(unsigned char selected);
void EEWriteMenuArray(unsigned char x, unsigned char y, const unsigned char * msg, unsigned char len);
void EEWriteMenuArrayMid(unsigned char x, unsigned char y, const unsigned char * msg, unsigned char len);
void LCDWriteMenuArray(unsigned char pos, unsigned char * msg, unsigned char len);
void LCD_Dec(unsigned int val, unsigned char pos);
void LCD_Dec_Small(unsigned int val, unsigned char pos);
void EE_Menu(void);
void EE_Menu_2(void);
void EEShow(void);
void EEShowSet(void);
void EEShowConfig(void);
void EE_Config_Menu(void);
void CMenu_Selected(unsigned char selected);

#ifdef	_1CH				
void EE_Menu_Single(void);
void ON_OFF(unsigned char selected, unsigned char x, unsigned char y);
#else
void ON_OFF(unsigned char selected, unsigned char y, unsigned char w);
#endif

#ifdef _I2C
void I2CStart(void);
void I2CStop(void);
void I2CWrite(unsigned char c);
unsigned char I2CRead(void);
#endif

#ifdef	_AVR
void EEWrite(volatile unsigned char * val, unsigned char adr);
#else
void EEWrite(unsigned char * val, unsigned char adr);
#endif
