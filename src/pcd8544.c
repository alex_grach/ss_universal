/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* ------------------------ NOKIA PCD8544 LCD ---------------------------- */
/* --------------------------- 3310 / 5110 ------------------------------- */
/* ----------------------------------------------------------------------- */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif

#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif

#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif

#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif

#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#ifdef	_LOGO
//extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
return;
}
#endif



//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
Lcd_Write(CMD,0x00); // nop;
for(i=0;i<12;i++)
	for(j=0;j<72;j++)
Lcd_Write(DATA,0x00);
Lcd_Write(CMD,0x00); // nop;
return;
}


// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_us(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x20);

Lcd_Write(CMD,0x21);  // LCD Extended Commands.
Lcd_Write(CMD,EERead(eelcd));  // Set LCD Vop (Contrast).
Lcd_Write(CMD,0x06);  // Set Temp coefficent.
Lcd_Write(CMD,0x13);  // LCD bias mode 1:48.
Lcd_Write(CMD,0x20);  // LCD Standard Commands, Horizontal addressing mode.
Lcd_Write(CMD,0x0C);  // LCD in normal mode.
Lcd_Write(CMD,0x0C^((cfg&0x02)>>1));  // Invert display option

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
Lcd_Write(CMD,0x21);  // LCD Extended Commands.
Lcd_Write(CMD,contrast);  // Set LCD Vop (Contrast).
Lcd_Write(CMD,0x0C^((cfg&0x02)>>1));  // Invert display option
return;
}

void Lcd_Write(unsigned char cd, unsigned char c){
unsigned char li;
	wdt_reset();
	cs = 0;
	dc=cd;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;
	}
	sclk=0;
	sda=0;
	cs = 1;
return;
}


void SetXY(unsigned char x, unsigned char y){
	Lcd_Write(CMD,x^0x80);
	Lcd_Write(CMD,y^0x40);
return;
}
