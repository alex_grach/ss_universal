/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* ----------------------- NOKIA SPFD54124 LCD --------------------------- */
/* ----------------------------------------------------------------------- */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif

#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif

#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif

#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif

#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

volatile unsigned char ch,mch=0;

#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
/*
unsigned char i,j,k,ch;
unsigned int count=0;

	for (i=0; i<8; i++) {
		SetXY(40,32+8*i,40+96,7);
		for (j=0; j<96; j++) {
			ch = logo_ss[count++];
			for (k=0; k<8; k++) {
				if (ch & 0x01) {
					Lcd_H();
					Lcd_S();
				}
				else {
					Lcd_H();
					Lcd_H();
				}
				ch >>= 1;
			}
		}
	}
*/
return;
}
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
cs=0;

Lcd_Write(CMD,0x2B);     // x-address
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0x00);
#ifdef	_SINGLE
	Lcd_Write(DATA,0x83);
#else
	Lcd_Write(DATA,0xA1);
#endif

Lcd_Write(CMD,0x2A);	// y-address
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0x00);
#ifdef	_SINGLE
	Lcd_Write(DATA,0xA1);
#else
	Lcd_Write(DATA,0x83);
#endif

Lcd_Write(CMD,0x2C);

	for(i=0;i<198;i++)   
		for(j=0;j<243;j++) {
			if (ch & 0x01)
				Lcd_S();
			else
				Lcd_H();
		}
return;
}

// init LCD
void Lcd_Init(void) {
//unsigned char i;
//SSPEN=0; // Disable SPI
cs = 1;
sclk = 0;
sda = 0;
rst = 0;
delay_ms(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_ms(0x20);
Lcd_Write(CMD,0x01); // Software reset
Lcd_Write(CMD,0xBA); // Data Order
Lcd_Write(DATA,0x01);  	      //
Lcd_Write(DATA,0x07);  	      //
Lcd_Write(DATA,0x01);  	      //
Lcd_Write(DATA,0x15);  	      //
Lcd_Write(CMD,0x25); // Contrast
Lcd_Write(DATA,0x01);  	      //
Lcd_Write(DATA,0x3F);  	      //
Lcd_Write(CMD,0x11);
delay_ms(1);
Lcd_Write(CMD,0x37);          //
Lcd_Write(DATA,0x01);  	      //
Lcd_Write(DATA,0x00);  	      //
Lcd_Write(CMD,0x3A);          //Interface pixel format:bit1,2,3 
Lcd_Write(DATA,0x55);  	      //16-bit mode
//Lcd_Write(DATA,0x01);  	      //
//Lcd_Write(DATA,0x05);  	      //
Lcd_Write(CMD,0x36);        //MEMORY DATA ACCESS CONTROL
#ifdef	_SINGLE
	Lcd_Write(DATA,0x20);	      //MEMORY DATA ACCESS CONTROL
#else
	Lcd_Write(DATA,0x40);	      //MEMORY DATA ACCESS CONTROL
#endif
/*
Lcd_Write(CMD,0x2D); // LUC conversion table
for (i=0;i<32;i++)
	Lcd_Write(DATA,i<<3);
for (i=0;i<64;i++)
	Lcd_Write(DATA,i<<2);
for (i=0;i<32;i++)
	Lcd_Write(DATA,i<<3);
*/
Lcd_Write(CMD,0x13);          //Normal mode
Lcd_Write(CMD,0x20^((cfg&0x02)>>1)); // Invert display option
Lcd_Write(CMD,0x29);          //Display on,0x29=ON,0x28=OFF
delay_ms(0x20);
cs=1;
//Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
	Lcd_Write(CMD,0x25); // Contrast
	Lcd_Write(DATA,0x01);  	      //
	Lcd_Write(DATA,contrast);
	Lcd_Write(CMD,0x20^((cfg&0x02)>>1)); // Invert display option
//	Lcd_Write(CMD,0x36);        //MEMORY DATA ACCESS CONTROL
//	Lcd_Write(DATA,0x00 ^ ((cfg&0x01)<<7) ^ ((cfg&0x08)<<4));
//	Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
//	Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option
}

#ifdef _SPI
void Lcd_Write(unsigned char cd, volatile unsigned char c) {
	wdt_reset();
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = cd;
	sclk = 1;
	SSPEN=1;
	SSPBUF = c;

return;
}

void Lcd_S(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0x00;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}

void Lcd_H(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0xFF;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}
#else
void Lcd_Write(unsigned char cd,unsigned char c){
unsigned char li;
	cs = 0;
	sclk = 0;
	sda = cd;
	sclk = 1;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;
	}
	sclk=0;
	sda=0;
	cs = 1;
return;
}

void Lcd_S(void) {
	Lcd_Write(DATA,0x00);
return;
}

void Lcd_H(void) {
	Lcd_Write(DATA,0xFF);
return;
}
#endif

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) {
cs=0;
#ifdef	_SINGLE
	y+=2;
	x+=4;
#else
	y+=0x08;
#endif
	Lcd_Write(CMD,0x2B);     // x-address
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,x);
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,x+dx);
	
	Lcd_Write(CMD,0x2A);	// y-address
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,y);
	Lcd_Write(DATA,0x00);
	Lcd_Write(DATA,y+dy);
	Lcd_Write(CMD,0x2C);
return;
}