/*
	NOKIA PCF8833 LCD
	2600/6100
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//clear LCD
void Lcd_Clear(void) {
unsigned char i,j;

SetXY(0xFF,0xFF,131,131);
delay_s(30);
cs=1;
SSPEN=1;
cs=0;
//sda=DATA;
	for(i=0;i<198;i++)   
		for(j=0;j<99;j++) {
			SSPBUF = 0xFF;
			/*sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			*/
		}
cs=1;
SSPEN=0;
SSPIF=0;
return;
}


// init LCD
void Lcd_Init(void) {
SSPEN = 0; // Disable SPI
sclk = 1;
sda = 0;
cs = 0;
rst = 0;
delay_s(1);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_s(1);

Lcd_Write(CMD,0x11); //sleep out(PHILLIPS)
Lcd_Write(CMD,0x03);   //Booset On(PHILLIPS)
Lcd_Write(CMD,0x3A);   // Set Color Mode(PHILLIPS)
//Lcd_Write(DATA,0x03); // 12-bit color mode
Lcd_Write(DATA,0x02); // 8-bit color mode
Lcd_Write(CMD,0x36);   // Memory Access Control(PHILLIPS)
Lcd_Write(DATA,0x20);
Lcd_Write(CMD,0x25);   // Set Contrast(PHILLIPS)
Lcd_Write(DATA,0x30);
Lcd_Write(CMD,0x00);     // nop(PHILLIPS)
delay_s(1);
Lcd_Write(CMD,0x29);   // display on(PHILLIPS)

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
Lcd_Write(CMD,0x25); /* CMD   Contrast Lvl & Int. Regul. Resistor Ratio */
Lcd_Write(DATA,contrast); /* DATA: contrast = 0x29 */
return;
}

void Lcd_Write(unsigned char cd, volatile unsigned char c){
	__asm__ ("clrwdt");	
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = cd;
	sclk = 1;
	SSPEN=1;
	SSPBUF = c;
return;
}

void Lcd_S(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0x00;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}

void Lcd_B(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0x07;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}

void Lcd_H(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0xFF;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) { // Set buffer
	x++;
	y++;
	Lcd_Write(CMD,0x2B);     // x-address
	Lcd_Write(DATA,x);
	Lcd_Write(DATA,x+dx);
	
	Lcd_Write(CMD,0x2A);	// y-address
	Lcd_Write(DATA,y);
	Lcd_Write(DATA,y+dy);
	Lcd_Write(CMD,0x2C); // Write diplay RAM
return;
}