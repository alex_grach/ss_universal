#!/bin/bash
#set -x
# ss firmware build script
#cpu_4k=( 16f884 16f883 )
#cpu=( 16f887 )
cpu=( 16f887 16f886 16f877 16f876 16f877a 16f876a )
#cpu=( 16f726 16f727 16f767 16f777 16f76 16f77 )
langs=( _RU _EN )
enc=( _ENCODER _BUTTONS _BUTTONS_14)
channels=( _1CH _2CH _4CH )
channels_spi=( _2CH _4CH )
#disp=( PCF8814 )
disp=( PCF8814 PCD8544 SSD1828 SSD1783 ST7558 SSD1306 )
disp_spi=( LS020 L2F50 LPH9157 SPFD54124 )
#func=( _NORM _IRON_SW _PWM )
func=( _NORM _IRON_SW )
visual=( _SWITCH _SINGLE _DUAL  )
prefix=( ~/sdcc/bin/ )
opts=" -S -mpic14 --stack-size 16 -c --use-non-free --no-extended-instructions --Werror -DCPU "

if [ $# -gt 0 ]; then
	for args in $@
	do	
	case ${args} in
		clean)
			rm *.zip *.lst *.cod *.map *.cof *.o *.asm *.adb *.hex *.p *~ ss_4k.c functions.c;
			echo "Project cleaned"
			;;
		proteus)
			echo "Deleting old firmware..."
			rm *.lst *.cod *.map *.cof *.hex &&
			echo "Done!"
			# Симулятор
			c=16f76
			#c=16f876a
			#c=16f876
			#c=16f873a
			#c=16f877a
			#c=16f886
			#c=16f887
			#d="ssd1783"
			#d="ssd1828"
			#d="ssd1306"
			#d="st7558"
			#d="st7920"
			#d="ks0108_595"
			#d="ls020"
			#d="ssd1289"
			#d="l2f50"
			#d="lph9157"
			#d="seg"
			#d="pcf8535"
			d="pcf8814"
			#d="pcf8833"
			#d="s1d15g10"
			#d="spfd54124"
			#c=16f917
			#blackwolf2000
			#o=" -D_RU -D_3V -D_SHOW -D_MY -D_SSD1828 -D_SPI -D_2CH -D_BUTTONS -D_IRON_SW -D_LOGO"
			#o=" -D_RU -D_3V -D_SHOW -D_MY -D_PCF8814 -D_2CH -D_BUTTONS -D_IRON_SW -D_LOGO"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_BUTTONS -D_RU -D_PCF8814 -D_2CH"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_ENCODER -D_MY -D_24C -D_PCF8814"
			#o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_MY -D_PCD8544"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_I2C -D_PCF8814 -D_2CH"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_SEG -D_BUTTONS"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_L2F50 -D_LARGE -D_BUTTONS -D_SPI -D_2CH"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_LARGE -D_2CH -D_BUTTONS -D_MITRE -D_SPI"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_BUTTONS -D_RU -D_PCF8814 -D_2CH -D_MITRE"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_L2F50 -D_LARGE -D_4CH -D_BUTTONS_14 -D_SPI"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_LARGE -D_4CH -D_BUTTONS"
			#Pavlon90
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_LARGE -D_4CH -D_ENCODER -D_SINGLE -D_IRON_SW -D_SPI"
			#sabbufer1
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_L2F50 -D_LARGE -D_LOGO -D_2CH -D_ENCODER -D_NORM -D_SPI"
			#_AV
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_LARGE -D_AV -D_4CH -D_BUTTONS -D_SINGLE -D_SPI"
			#_76
			o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_PCF8814 -D_2CH -D_ENCODER -D_SPI"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_SPFD54124 -D_LARGE -D_BUTTONS -D_2CH"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_PCF8833 -D_COLOR -D_SPI -D_LARGE -D_BUTTONS -D_2CH"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LPH9157 -D_USART -D_LARGE -D_BUTTONS"
			#o=" -D_RU -D_3V -D_SHOW -D_PCF8814 -D_4CH -D_ENCODER"
			#o=" -D_RU -D_3V -D_MY -D_SHOW -D_PROTEUS -D_SSD1783 -D_1CH"
			#o=" -D_EN -D_5V -D_SHOW -D_PROTEUS -D_MY -D_KS0108 -D_2CH"
			#o=" -D_EN -D_5V -D_SHOW -D_PROTEUS -D_MY -D_ST7920 -D_2CH"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_SPFD54124 -D_SPI -D_4CH -D_BUTTONS -D_LARGE -D_DUAL_LARGE"
			# - rav133 -
			#c=16f877a
			#d=spfd54124
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_SPFD54124 -D_SPI -D_4CH -D_BUTTONS_14 -D_LARGE -D_SINGLE -D_RAV133"
			# ----------
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_SSD1828 -D_AQUA"
					echo ${c} 
					echo "Starting proteus build..."
					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					gpasm -a inhx8m -c ss.asm &&
					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm ${d}.c &&
					gpasm -a inhx8m -c lcd.asm &&
					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					gpasm -a inhx8m -c eeprom.asm &&
					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					gpasm -a inhx8m -c pid.asm &&
					gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
					echo "Building `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		test)
			echo "Deleting old firmware..."
			rm *.lst *.cod *.map *.cof *.hex &&
			echo "Done!"
			# Симулятор
			#c=16f876a
			#c=16f876
			#c=16f877a
			#c=16f873a
			#c=16f887
			#c=16f886
			#d="ssd1783"
			#d="st7558"
			#d="ks0108"
			#d="l2f50"
			#d="lph9157"
			#d="seg"
			#d="pcf8814"
			#d="spfd54124"
			#c=16f917
			#o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_ENCODER -D_MY -D_PCF8814"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_ENCODER -D_MY -D_24C -D_PCF8814"
			#o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_MY -D_PCD8544"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_PCD8544 -D_AQUA -D_ENCODER -D_1CH -D_I2C"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_SEG -D_BUTTONS"
			o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_BUTTONS"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LS020 -D_LARGE -D_BUTTONS"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_SPFD54124 -D_BUTTONS -D_LARGE"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_LPH9157 -D_USART -D_LARGE -D_BUTTONS"
			#o=" -D_RU -D_3V -D_SHOW -D_PROTEUS -D_PCF8814 -D_4CH -D_BUTTONS_14 -D_DUAL_SMALL"
			#o=" -D_RU -D_3V -D_MY -D_SHOW -D_PROTEUS -D_SSD1783 -D_1CH"
			#o=" -D_EN -D_5V -D_SHOW -D_PROTEUS -D_MY -D_KS0108 -D_4CH -D_PWMPH"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_SPFD54124"
			#o=" -D_RU -D_5V -D_SHOW -D_PROTEUS -D_MY -D_SSD1828 -D_AQUA"
					echo ${c} 
					echo "Starting proteus build..."
					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o test.asm test_873a.c &&
					gpasm -a inhx8m -c test.asm &&
					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm ${d}.c &&
					gpasm -a inhx8m -c lcd.asm &&
#					sdcc ${opts} -p${c} -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
#					gpasm -a inhx8m -c eeprom.asm &&
					gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo test_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` test.o lcd.o libsdcc.lib pic${c}.lib &&
					echo "Building `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		debug)
			echo "Deleting old firmware..."
			rm *.lst *.cod *.map *.cof *.hex *.zip &&
			echo "Done!"
			# Симулятор
			c=16f876a
			#c=16f917
			o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_ENCODER -D_MY -D_PCF8814"
			#o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_MY -D_PCD8544"
					echo ${c} 
					echo "Starting debug build..."
					sdcc --debug -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					sdcc --debug -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm lcd.c &&
					sdcc --debug -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					sdcc --debug -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					echo "Building debug firmware finished!"
			;;
		mplab)
			echo "Deleting old firmware..."
			rm *.lst *.cod *.map *.cof *.hex *.zip &&
			echo "Done!"
			# Симулятор
			#c=16f876a
			c=16f887
			#o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_MY -D_PCF8814"
			o=" -D_RU -D_5V -D_HIDE -D_PROTEUS -D_MY -D_PCD8544"
					echo ${c} 
					echo "Starting proteus build..."
					sdcc -S -mpic14 -p${c} --use-non-free --mplab-comp -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					#gpasm -a inhx8m -c ss.asm &&
					sdcc -S -mpic14 -p${c} --use-non-free --mplab-comp -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm lcd.c &&
					#gpasm -a inhx8m -c lcd.asm &&
					sdcc -S -mpic14 -p${c} --use-non-free --mplab-comp -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					#gpasm -a inhx8m -c eeprom.asm &&
					sdcc -S -mpic14 -p${c} --use-non-free --mplab-comp -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					#gpasm -a inhx8m -c pid.asm &&
					#gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
					#rm *.lst *.cod *.map *.cof &&
					#cp ./ss_${c}_proteus.hex ~/Downloads/ss_876/ss.hex
					#cp ./*.asm ~/Downloads/ss_876/
					echo "Building `echo ss_${c}_proteus.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		jozsef)
			echo "Deleting old firmware..."
			rm *.hex *.zip &&
			echo "Done!"
			# Простое управление паяльником вкл/выкл )))
			c=16f877a
			o=" -D_SIMPLE"
			for l in ${langs[@]}
			do
				for v in ${voltage[@]}
				do
					for h in ${hide[@]}
					do
						for i in ${invert[@]}
						do
							echo ${c} 
							echo "Starting build..."
							sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} ${o} -o ss.asm ss.c &&
							gpasm -a inhx8m -c ss.asm &&
							sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} ${o} -o lcd.asm lcd.c &&
							gpasm -a inhx8m -c lcd.asm &&
							sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} ${o} -o eeprom.asm eeprom.c &&
							gpasm -a inhx8m -c eeprom.asm &&
							sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D${v} -D${h} -D${i} ${o} -o pid.asm pid.c &&
							gpasm -a inhx8m -c pid.asm &&
							gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${l}${v}${h}${i}_simple.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
							rm *.lst *.cod *.map *.cof &&
							echo "Building `echo ss_${c}${l}${v}${h}${i}_simple.hex | tr '[:upper:]' '[:lower:]'` finished!"
						done
					done
				done
			done
			
			echo "Creating archive..."
			zip -9 ss_jozsef.zip *.c *.h *.hex *.sh *.php files/* &&
			echo "Done!"
			;;
		legacy)
			echo "Deleting old firmware..."
			rm *.hex *.zip &&
			echo "Done!"
			# Сапоги для сапожника )))
			c=16f887
			o=" -D_RU -D_5V -D_SHOW -D_NORM -D_LEGACY -D_LOGO -D${disp}"
					echo ${c} 
					echo ${o}
					echo "Starting legacy build..."
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					gpasm -a inhx8m -c ss.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm pcf8814.c &&
					gpasm -a inhx8m -c lcd.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					gpasm -a inhx8m -c eeprom.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					gpasm -a inhx8m -c pid.asm &&
					gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${l}${v}${h}${i}_legacy.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
					rm *.lst *.cod *.map *.cof &&
					echo "Building `echo ss_${c}_legacy.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		legacy_enc)
			echo "Deleting old firmware..."
			rm *.hex *.zip &&
			echo "Done!"
			# Сапоги для сапожника )))
			c=16f887
			o=" -D_RU -D_5V -D_SHOW -D_NORM -D_LEGACY -D_ENCODER -D${disp}"
					echo ${c} 
					echo ${o}
					echo "Starting legacy build..."
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o ss.asm ss.c &&
					gpasm -a inhx8m -c ss.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o lcd.asm lcd.c &&
					gpasm -a inhx8m -c lcd.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o eeprom.asm eeprom.c &&
					gpasm -a inhx8m -c eeprom.asm &&
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
					gpasm -a inhx8m -c pid.asm &&
					gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_enc_${c}${l}${v}${h}${i}_legacy.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
					rm *.lst *.cod *.map *.cof &&
					echo "Building `echo ss_${c}_legacy.hex | tr '[:upper:]' '[:lower:]'` finished!"
			;;
		build)
			o="-D_LOGO ";
			echo "Deleting old firmware..."
			rm ss.zip *.hex ;
			for c in ${cpu[@]}
			do
				if [[ "$c" == '16f886' || "$c" == '16f876a' || "$c" == '16f876' ]]
				then
					enc=( _BUTTONS _ENCODER )
					func=( _NORM )
				else
					enc=( _BUTTONS _BUTTONS_14 _ENCODER )
					func=( _NORM _IRON_SW )
				fi
				echo ${c} 
				echo "Starting build..."
				sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
				echo "SDCC";
				gpasm -a inhx8m -c pid.asm &&
				echo "GPUTILS";
				for d in ${disp[@]}
				do
					for l in ${langs[@]}
					do
						for e in ${enc[@]}
						do
							for f in ${func[@]}
							do
								for ch in ${channels[@]}
								do
									if [ "$ch" != '_4CH' ];
									then
										if [ "$e" != '_BUTTONS_14' ];
										then
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D_${d} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
											gpasm -a inhx8m -c lcd.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -o eeprom.asm eeprom.c &&
											gpasm -a inhx8m -c eeprom.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o ss.asm ss.c &&
											gpasm -a inhx8m -c ss.asm &&
											echo "GPUTILS";
											gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
											rm *.lst *.cod *.map *.cof &&
											echo "LINK";
											echo "Building `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` finished!"
										fi
									else
										if [[ ( $d == 'PCF8814' ) && $e != '_BUTTONS' ]];
										then
											for v in ${visual[@]}
											do
												sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D_${d} -D${v} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
												gpasm -a inhx8m -c lcd.asm &&
												sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${v} -o eeprom.asm eeprom.c &&
												gpasm -a inhx8m -c eeprom.asm &&
												sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${v} -D${f} -o ss.asm ss.c &&
												gpasm -a inhx8m -c ss.asm &&
												echo "GPUTILS";
												gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${f}${v}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
												rm *.lst *.cod *.map *.cof &&
												echo "LINK";
												echo "Building `echo ss_${c}${ch}_${d}${e}${l}${f}${v}.hex | tr '[:upper:]' '[:lower:]'` finished!"
											done
										else
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D_${d} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
											gpasm -a inhx8m -c lcd.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -o eeprom.asm eeprom.c &&
											gpasm -a inhx8m -c eeprom.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o ss.asm ss.c &&
											gpasm -a inhx8m -c ss.asm &&
											echo "GPUTILS";
											gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
											rm *.lst *.cod *.map *.cof &&
											echo "LINK";
											echo "Building `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` finished!"
										fi
									fi
								done
							done
						done
					done
				done
			echo "Creating archive ..."
			zip -9r ss_${c}.zip *.c *.h *.hex *.sh *.php &&
			echo "Done!"
			mv *.zip ../firmware/
			rm *.lst *.cod *.map *.cof *.o *.asm *.adb *.hex *.p *~ ss_4k.c functions.c;
			echo "Cleaned"
			done
			;;
		build_spi)
			o="-D_SPI -D_LARGE ";
			echo "Deleting old firmware..."
			rm ss.zip *.hex ;
			for c in ${cpu[@]}
			do
				if [[ "$c" == '16f886' || "$c" == '16f876a' || "$c" == '16f876' ]]
				then
					enc=( _BUTTONS _ENCODER )
					func=( _NORM )
				else
					enc=( _BUTTONS _BUTTONS_14 _ENCODER )
					func=( _NORM _IRON_SW )
				fi
				echo ${c} 
				echo "Starting build..."
				sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -o pid.asm pid.c &&
				echo "SDCC";
				gpasm -a inhx8m -c pid.asm &&
				echo "GPUTILS";
				for d in ${disp_spi[@]}
				do
					for l in ${langs[@]}
					do
						for e in ${enc[@]}
						do
							for f in ${func[@]}
							do
								for ch in ${channels_spi[@]}
								do
									if [ "$ch" != '_4CH' ];
									then
										if [ "$e" != '_BUTTONS_14' ];
										then
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
											gpasm -a inhx8m -c lcd.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o eeprom.asm eeprom.c &&
											gpasm -a inhx8m -c eeprom.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o ss.asm ss.c &&
											gpasm -a inhx8m -c ss.asm &&
											echo "GPUTILS";
											gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
											rm *.lst *.cod *.map *.cof &&
											echo "LINK";
											echo "Building `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` finished!"
										fi
									else
										if [[ ( $d == 'SPFD54124' || $d == 'LS020' ) && $e != '_BUTTONS' ]];
										then
											for v in ${visual[@]}
											do
												sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${v} -D${f} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
												gpasm -a inhx8m -c lcd.asm &&
												sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${v} -D${f} -o eeprom.asm eeprom.c &&
												gpasm -a inhx8m -c eeprom.asm &&
												sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${v} -D${f} -o ss.asm ss.c &&
												gpasm -a inhx8m -c ss.asm &&
												echo "GPUTILS";
												gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${f}${v}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
												rm *.lst *.cod *.map *.cof &&
												echo "LINK";
												echo "Building `echo ss_${c}${ch}_${d}${e}${l}${f}${v}.hex | tr '[:upper:]' '[:lower:]'` finished!"
											done
										else
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
											gpasm -a inhx8m -c lcd.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o eeprom.asm eeprom.c &&
											gpasm -a inhx8m -c eeprom.asm &&
											sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` ${o} -D${l} -D_${d} -D${e} -D${ch} -D${f} -o ss.asm ss.c &&
											gpasm -a inhx8m -c ss.asm &&
											echo "GPUTILS";
											gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
											rm *.lst *.cod *.map *.cof &&
											echo "LINK";
											echo "Building `echo ss_${c}${ch}_${d}${e}${l}${f}.hex | tr '[:upper:]' '[:lower:]'` finished!"
										fi
									fi
								done
							done
						done
					done
				done
			echo "Creating archive ..."
			zip -9r ss_${c}_spi.zip *.c *.h *.hex *.sh *.php &&
			echo "Done!"
			mv *.zip ../firmware/
			rm *.lst *.cod *.map *.cof *.o *.asm *.adb *.hex *.p *~ ss_4k.c functions.c;
			echo "Cleaned"
			done
			;;
		build_dev)
			echo "Deleting old firmware..."
			rm ss.zip *.hex ;
			for c in ${cpu[@]}
			do
				echo ${c} 
				echo "Starting build..."
				sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -o pid.asm pid.c &&
				echo "SDCC";
				gpasm -a inhx8m -c pid.asm &&
				echo "GPUTILS";
				for d in ${disp[@]}
				do
					sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D_${d} -o lcd.asm `echo ${d} | tr '[:upper:]' '[:lower:]'`.c &&
					gpasm -a inhx8m -c lcd.asm &&
					for l in ${langs[@]}
					do
						for e in ${enc[@]}
						do
							for ch in ${channels[@]}
							do
								sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'`  -D${l} -D_${d} -D${e} -D${ch} -o eeprom.asm eeprom.c &&
								gpasm -a inhx8m -c eeprom.asm &&
								if [ "$d" == "PCF8814" ]; then
									echo "${d}"
									for dc in ${func[@]}
									do
										sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D_${d} -D${e} -D${ch} -D${dc} -o ss.asm ss.c &&
										gpasm -a inhx8m -c ss.asm &&
										echo "GPUTILS";
										gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}${dc}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
										rm *.lst *.cod *.map *.cof &&
										echo "LINK";
										echo "Building `echo ss_${c}${ch}_${d}${e}${l}${dc}.hex | tr '[:upper:]' '[:lower:]'` finished!"
									done
								else
									echo "-"
									sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D_${d} -D${e} -D${ch} -o ss.asm ss.c &&
									gpasm -a inhx8m -c ss.asm &&
									echo "GPUTILS";
									gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${ch}_${d}${e}${l}.hex | tr '[:upper:]' '[:lower:]'` ss.o lcd.o eeprom.o pid.o libsdcc.lib pic${c}.lib &&
									rm *.lst *.cod *.map *.cof &&
									echo "LINK";
									echo "Building `echo ss_${c}${ch}_${d}${e}${l}.hex | tr '[:upper:]' '[:lower:]'` finished!"
								fi
							done
						done
					done
				done
			echo "Creating archive ..."
			zip -9r ss_${c}_dev.zip *.c *.h *.hex *.sh *.php &&
			echo "Done!"
			rm *.lst *.cod *.map *.cof *.o *.asm *.adb *.hex *.p *~ ss_4k.c functions.c;
			echo "Cleaned"
			done
			;;		build_4k)
			echo "Deleting old firmware..."
			rm ss_4k.zip *.hex function.c ss_4k.c;
			cat ss.c > ss_4k.c &&
			echo "Done!"
			for c in ${cpu_4k[@]}
			do
				for d in ${disp[@]}
				do
					cat `echo ${d} | tr '[:upper:]' '[:lower:]'`.c pid.c eeprom.c > functions.c &&
					for l in ${langs[@]}
					do
						for e in ${enc[@]}
						do
							echo ${c} 
							echo "Starting build..."
							sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D_${d} -D${e} -o ss.asm ss_4k.c &&
							gpasm -a inhx8m -c ss.asm &&
							sdcc -S -mpic14 -p${c} -c --use-non-free -DCPU -DPIC`echo ${c} | tr '[:lower:]' '[:upper:]'` -D${l} -D_${d} -D${e} -o functions.asm functions.c &&
							gpasm -a inhx8m -c functions.asm &&
							gplink -c -m -w -r -I/opt/local/share/sdcc/lib/pic14 -I/opt/local/share/sdcc/non-free/lib/pic14 -s /opt/local/share/gputils/lkr/${c}_g.lkr -o `echo ss_${c}${d}${e}${l}.hex | tr '[:upper:]' '[:lower:]'` ss.o functions.o libsdcc.lib pic${c}.lib &&
							rm *.lst *.cod *.map *.cof &&
							echo "Building `echo ss_${c}${d}${e}${l}.hex | tr '[:upper:]' '[:lower:]'` finished!"
						done
					done
				done
			done
			exit;
			echo "Creating archive..."
			zip -9r ss_4k.zip *.c *.h *.hex *.sh *.php eeprom &&
			echo "Done!"
			;;
		zip_src)
			echo "Removing old archive..."
			rm ss_src.zip &&
			echo "Creating archive..."
			zip -9r ss_src.zip *.c *.h *.sh *.php &&
			echo "Done!"
			;;
	esac
	done
else
	echo "No option specified!"
	echo "Usage : ./build.sh [options]"
	echo "Options :"
	echo ""
	echo "	build - make firmware for"
	for c in  ${cpu[@]}
	do
		echo "		${c}"
	done
	echo "			MCUs"
	echo ""
	echo "	build_4k - make firmware for"
	for c in  ${cpu_4k[@]}
	do
		echo "		${c}"
	done
	echo "			MCUs"
	echo ""
	echo "	clean - cleanup build files"
	echo ""
fi

