/*
	OLED SSD1306 SPI LCD
	128x64
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
unsigned char i,j;
	for (i=0; i<8; i++) {
		SetXY(0,i);
		for (j=0; j<96; j++)
		#ifdef	_I2C
			I2CWrite(logo_ss[(unsigned int)96*i+j]);
		#else
			Lcd_Write(DATA,logo_ss[(unsigned int)96*i+j]);
		#endif
	}
return;
}
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
	for(i=0;i<8;i++) {
		SetXY(0,i);
		for(j=0;j<128;j++) {
			#ifdef	_I2C
				I2CWrite(0x00);
			#else
				Lcd_Write(DATA,0x00);
			#endif
		}
	}
return;
}


// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_us(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x20);

#ifdef _I2C
	I2CStart();
	I2CWrite(0x78); // LCD Address
	I2CWrite(0x00); // Control byte
	I2CWrite(0xAE); // Set display OFF
	
	I2CWrite(0xD4); // Set Display Clock Divide Ratio / OSC Frequency
	I2CWrite(0x80); // Display Clock Divide Ratio / OSC Frequency 
	
	I2CWrite(0xA8); // Set Multiplex Ratio
	I2CWrite(0x3F); // Multiplex Ratio for 128x64 (64-1)
	
	I2CWrite(0xD3); // Set Display Offset
	I2CWrite(0x00); // Display Offset
	
	I2CWrite(0x40); // Set Display Start Line
	
	I2CWrite(0x8D); // Set Charge Pump
	I2CWrite(0x14); // Charge Pump (0x10 External, 0x14 Internal DC/DC)
	
	I2CWrite(0xA1); // Set Segment Re-Map
	I2CWrite(0xC8); // Set Com Output Scan Direction
	
	I2CWrite(0xDA); // Set COM Hardware Configuration
	I2CWrite(0x12); // COM Hardware Configuration
	
	I2CWrite(0x81); // Set Contrast
	I2CWrite(0xCF); // Contrast
	
	I2CWrite(0xD9); // Set Pre-Charge Period
	I2CWrite(0xF1); // Set Pre-Charge Period (0x22 External, 0xF1 Internal)
	
	I2CWrite(0xDB); // Set VCOMH Deselect Level
	I2CWrite(0x40); // VCOMH Deselect Level
	
	I2CWrite(0xA4); // Set all pixels OFF
	I2CWrite(0xA6); // Set display not inverted
	I2CWrite(0xAF); // Set display On
	I2CStop();
#else
	Lcd_Write(CMD,0xAE); // Set display OFF		
	
	Lcd_Write(CMD,0xD4); // Set Display Clock Divide Ratio / OSC Frequency
	Lcd_Write(CMD,0x80); // Display Clock Divide Ratio / OSC Frequency 
	
	Lcd_Write(CMD,0xA8); // Set Multiplex Ratio
	Lcd_Write(CMD,0x3F); // Multiplex Ratio for 128x64 (64-1)
	
	Lcd_Write(CMD,0xD3); // Set Display Offset
	Lcd_Write(CMD,0x00); // Display Offset
	
	Lcd_Write(CMD,0x40); // Set Display Start Line
	
	Lcd_Write(CMD,0x8D); // Set Charge Pump
	Lcd_Write(CMD,0x14); // Charge Pump (0x10 External, 0x14 Internal DC/DC)
	
	Lcd_Write(CMD,0xA1); // Set Segment Re-Map
	Lcd_Write(CMD,0xC8); // Set Com Output Scan Direction
	
	Lcd_Write(CMD,0xDA); // Set COM Hardware Configuration
	Lcd_Write(CMD,0x12); // COM Hardware Configuration
	
	Lcd_Write(CMD,0x81); // Set Contrast
	Lcd_Write(CMD,0xCF); // Contrast
	
	Lcd_Write(CMD,0xD9); // Set Pre-Charge Period
	Lcd_Write(CMD,0xF1); // Set Pre-Charge Period (0x22 External, 0xF1 Internal)
	
	Lcd_Write(CMD,0xDB); // Set VCOMH Deselect Level
	Lcd_Write(CMD,0x40); // VCOMH Deselect Level
	
	Lcd_Write(CMD,0xA4); // Set all pixels OFF
	Lcd_Write(CMD,0xA6); // Set display not inverted
	Lcd_Write(CMD,0xAF); // Set display On
#endif
/*
Lcd_Write(CMD,0xAE); // DisplayOFF
Lcd_Write(CMD,0xE2); // Software Reset
Lcd_Write(CMD,0xAB); // Start Internal Oscillator 
Lcd_Write(CMD,0x2F); // SetVoltControl 
Lcd_Write(CMD,0x65); // Set Boost Level 
Lcd_Write(CMD,0x25); // Set Internal Resistor Ratio 
Lcd_Write(CMD,0x39); // Set TC value 
Lcd_Write(CMD,0xA4); // Entire Display Select 
Lcd_Write(CMD,0xC8); // Set COM Scan Direction 
Lcd_Write(CMD,0xA1); // Set Segment Re-map 
Lcd_Write(CMD,0xAF); // DisplayON 
delay_us(0x80);
Lcd_Write(CMD,0x81); // Set Contrast Level 
Lcd_Write(CMD,EERead(eelcd)); // Set Contrast Level 

Lcd_Write(CMD,0xA6); // !invert display
Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
//Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option
*/
Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
#ifdef _I2C
	I2CStart();
	I2CWrite(0x78); // LCD Address
	I2CWrite(0x00); // Control byte
	I2CWrite(0x81); // Set Contrast Level 
	I2CWrite(contrast); // Set Contrast Level 
	I2CWrite(0xA6^((cfg&0x02)>>1)); // Invert display option
	I2CWrite(0xC0^(cfg&0x08)); // Mirror Y axis option
	I2CStop();
#else
	Lcd_Write(CMD,0x81); // Set Contrast Level 
	Lcd_Write(CMD,contrast); // Set Contrast Level 
	Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
	Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option
#endif
return;
}

void Lcd_Write(unsigned char cd,unsigned char c){
#ifdef _I2C
	cd=0;
	I2CWrite(c);
#else
unsigned char li;
	cs = 0;
	dc=cd;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;
	}
	sclk=0;
	sda=0;
	cs = 1;
#endif
	__asm__ ("clrwdt");	
return;
}


void SetXY(unsigned char x, unsigned char y){
#ifdef _I2C
	I2CStart();
	I2CWrite(0x78); // LCD Address
	I2CWrite(0x00); // Control byte
	I2CWrite(x & 0x0F);
	I2CWrite(0x10 | ((x>>4)&0x07));
	I2CWrite(0xB0 + y);
	I2CStop();
	I2CStart();
	I2CWrite(0x78); // Set display OFF
	I2CWrite(0x40); // Control byte
#else
	x+=11;
	Lcd_Write(CMD,x & 0x0F);
	Lcd_Write(CMD,0x10 | ((x>>4)&0x07));
	Lcd_Write(CMD,0xB0 + y);
#endif
return;
}
