unsigned char PID(unsigned int settemp, unsigned int currenttemp, unsigned char ch);
void loadPreset(unsigned char addr);

#ifdef	_AVR
void savePreset(unsigned char addr);
#else
void savePreset(volatile unsigned char addr);
#endif