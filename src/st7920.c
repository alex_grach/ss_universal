/*
	128x64 ST7920 LCD
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//unsigned char csbit;

//clear LCD

void Lcd_Clear(void){
Lcd_Write(CMD,0x01); // Clear display;
delay_us(0x50);
return;
}

// init LCD
void Lcd_Init(void) {
cs = 0;
rst = 0;
delay_us(0x50);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x50);
//csbit = 0;
//cs2 = 1;
Lcd_Write(CMD,0x30); // 8-bit mode
delay_us(0x50);
Lcd_Write(CMD,0x0C); // Display ON
//Lcd_Write(CMD,0x0F); // Display ON + Blinky cursor
delay_us(0x50);
Lcd_Write(CMD,0x01); // Clear display;
delay_us(0x50);
Lcd_Write(CMD,0x06); // Set mode
delay_us(0x50);
Lcd_Write(CMD,0x34); // Select extended instruction set. 
delay_us(0x50);
//Lcd_Write(CMD,0x02); // Enable CGRAM address.
//delay_us(0x50);
//Lcd_Write(CMD,0x36); // Graphic display ON. 
Lcd_Write(CMD,0x30); // Select basic insruction set. 
delay_us(0x50);
return;
}

void Lcd_Reinit(unsigned char contrast) {
	contrast=0;
}

void Lcd_Write(unsigned char cd, unsigned char c) {
unsigned char li, cbit;
	__asm__ ("clrwdt");
	cbit = (cd)?0xFA:0xF8;
	cs = 1;
	for(li=0;li<8;li++) {
		sclk = 0;
		;
			if(cbit & 0x80)
				sda = 1;
			else
				sda = 0;
		;
		sclk = 1;
		;
		cbit <<= 1;
	}
	cbit = c & 0xF0;
	for(li=0;li<8;li++) {
		sclk = 0;
		;
			if(cbit & 0x80)
				sda = 1;
			else
				sda = 0;
		;
		sclk = 1;
		;
		cbit <<= 1;
	}
	c <<= 4;
	for(li=0;li<8;li++) {
		sclk = 0;
		;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		;
		sclk = 1;
		;
		c <<= 1;
	}
	sclk = 0;
	sda = 0;
	cs = 0;
return;
}

void SetC(unsigned char pos){
	Lcd_Write(CMD,0x80 | pos);
return;
}

void SetXY(unsigned char x, unsigned char y){
//	x+=11;
	Lcd_Write(CMD,0x80 | x);
	Lcd_Write(CMD,0x80 | y);
return;
}
