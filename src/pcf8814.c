/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* ------------------------ NOKIA PCF8814 LCD ---------------------------- */
/* ----- 1100 / 1110 / 1110i / 1112 / 1200 / 1116 / 2760 (external) ------ */
/* ----------------------- 1202 / 1203 / 1280 ---------------------------- */
/* ----------------------------------------------------------------------- */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif

#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif

#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif

#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif

#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
unsigned char i,j;
Lcd_Write(CMD,0xAE); // disable display;
	for (i=0; i<8; i++) {
		for (j=0; j<96; j++)
			Lcd_Write(DATA,logo_ss[(unsigned int)96*i+j]);
	}
Lcd_Write(CMD,0xAF); // enable display;
return;
}
#endif

//clear LCD
void Lcd_Clear(void){
unsigned int i,j;
Lcd_Write(CMD,0xAE); // disable display;
for(i=0;i<12;i++)
	for(j=0;j<72;j++)
Lcd_Write(DATA,0x00);
Lcd_Write(CMD,0xAF); // enable display;
return;
}


// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
#ifdef _DUAL
	if (!DUAL) { // Reset needed only time
		rst = 0;
		delay_ms(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
		rst = 1;
		delay_ms(0x20);
	}
#else
	rst = 0;
	delay_ms(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
	rst = 1;
	delay_ms(0x20);
#endif

Lcd_Write(CMD,0xE2); // reset
delay_ms(0x20);
Lcd_Write(CMD,0xA4); // all on/normal display
Lcd_Write(CMD,0x2F); // Power control set(charge pump on/off)
Lcd_Write(CMD,0x40); // set start row address = 0
Lcd_Write(CMD,0xB0); // set Y-address = 0
Lcd_Write(CMD,0x10); // set X-address, upper 3 bits
Lcd_Write(CMD,0x00); // set X-address, lower 4 bits
Lcd_Write(CMD,0xAC); // set initial row (R0) of the display
Lcd_Write(CMD,0x00);
Lcd_Write(CMD,0x24); // write VOP register 
Lcd_Write(CMD,EERead(eelcd));
Lcd_Write(CMD,0xAF); // enable display;
Lcd_Write(CMD,0xA7); // invert display
delay_ms(0x20);				// 1/2 Sec delay
Lcd_Write(CMD,0xA6); // !invert display
Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
	Lcd_Write(CMD,0x24); // write VOP register 
	Lcd_Write(CMD,contrast);
	Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
	Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
	Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option
}

#ifdef	_SPI
	void Lcd_Write(unsigned char cd, volatile unsigned char c){
		wdt_reset();
	#ifdef _DUAL
		cs = (DUAL)?0:1;
	#else
		cs = 1;
	#endif
		SSPEN=0;
	#ifdef _DUAL
		cs = (DUAL)?1:0;
	#else
		cs = 0;
	#endif
		sclk = 0;
		sda = cd;
		sclk = 1;
		SSPEN=1;
		SSPBUF = c;
	
	return;
	}
#else
	void Lcd_Write(unsigned char cd,unsigned char c){
	unsigned char li;
		wdt_reset();
	#ifdef _DUAL
		cs = (DUAL)?1:0;
	#else
		cs = 0;
	#endif
		sclk = 0;
		sda = cd;
		sclk = 1;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(c & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			c <<= 1;
	//		Delay10TCYx(20);
		}
		sclk = 0;
		sda = 0;
	#ifdef _DUAL
		cs = (DUAL)?0:1;
	#else
		cs = 1;
	#endif
	return;
	}
#endif

void SetXY(unsigned char x, unsigned char y){
	Lcd_Write(CMD,x & 0x0F);
	Lcd_Write(CMD,0x10 | ((x>>4)&0xF));
	Lcd_Write(CMD,0xB0 + y);
return;
}
