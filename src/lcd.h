#define WIDTH	6

#if defined(_COLOR) || defined(_COLOR_16)
	#define HEIGHT	8
#else
	#define HEIGHT	1
#endif

#define MID_WIDTH	12

#if defined(_COLOR) || defined(_COLOR_16)
	#define MID_HEIGHT	4
#else
	#define MID_HEIGHT	2
#endif

#ifdef _PCD8544	// Nokia 3310
	#ifdef _1CH
		#define CMAX 6
		#define MMAX 2
	#elif _4CH
		#define CMAX 6
		#define MMAX 4
	#else
		#define CMAX 5
		#define MMAX 2
	#endif
#elif _LARGE
	#if _1CH
		#define CMAX 5
		#define MMAX 2
	#elif _4CH
		#define CMAX 5
		#define MMAX 4
	#else
		#define CMAX 4
		#define MMAX 2
	#endif
#else
	#if _1CH
		#define CMAX 8
		#define MMAX 2
	#elif _4CH
		#define CMAX 8
		#define MMAX 4
	#else
		#define CMAX 7
		#define MMAX 2
	#endif
#endif

#define BGC 0xFF
#define DCC 0x03
#define LTC 0x00

#ifdef _ST7558	// Motorola C113
	#define _I2C
	#undef _IRON_SW
	#undef _4CH
	#undef _COMPRESSOR
#endif

void Lcd_Init(void);
#ifdef _SSD1289
	void Lcd_Write(unsigned char cd,unsigned int c);
#elif _SPI
	void Lcd_Write(unsigned char cd,volatile unsigned char c);
#else
	void Lcd_Write(unsigned char cd,unsigned char c);
#endif
void Lcd_Clear(void);
void Lcd_Logo(void);
void Lcd_Reinit(unsigned char contrast);
#if defined(_COLOR) || defined(_COLOR_16)
	void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy);
	void Lcd_H(void);
	void Lcd_S(void);
	#ifndef	_COLOR_16
	void Lcd_B(void);
	#endif
#else
	void SetXY(unsigned char x, unsigned char y);
	void SetC(unsigned char pos);
#endif
