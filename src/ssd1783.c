/*
	MOTOROLA SSD1783 LCD
	V171
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//clear LCD
void Lcd_Clear(void) {
Lcd_Write(CMD,0xAE); // disable display;
Lcd_Write(CMD,0x8E); // Clear Window mode
Lcd_Write(DATA,0x00); // Start X
Lcd_Write(DATA,0x00); // Start Y
Lcd_Write(DATA,0x61); // End X
Lcd_Write(DATA,0x42); // End Y
Lcd_Write(CMD,0x25); // NOP

Lcd_Write(CMD,0x92); // Fill enabled
Lcd_Write(DATA,0x01);

Lcd_Write(CMD,0x84); // Draw Rectangle
Lcd_Write(DATA,0x00); // X
Lcd_Write(DATA,0x00); // Y
#ifdef _PCD8544 // For debugging 84x48
Lcd_Write(DATA,0x54); // End X
Lcd_Write(DATA,0x30); // End Y
#else
Lcd_Write(DATA,0x60); // X
Lcd_Write(DATA,0x42); // Y
#endif
Lcd_Write(DATA,0xE0); // Color
Lcd_Write(DATA,0x00); // Color
Lcd_Write(DATA,BGC); // Color
Lcd_Write(DATA,BGC); // Color
Lcd_Write(CMD,0x25); // NOP

Lcd_Write(CMD,0xAF); // enable display;
return;
}


// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_us(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x20);

Lcd_Write(CMD,0xD1); /* CMD   set internal oscillator on */
Lcd_Write(CMD,0x94); /* CMD   leave sleep mode */
Lcd_Write(CMD,0xBB); /* CMD   Set COM Output Scan Direction: */
Lcd_Write(DATA,0x01); /* DATA: 01: COM0-79, then COM159-80 */
/* -------- DIFFERENT FROM ORIGINAL CODE: -------------- */
/* we use 8bit per pixel packed RGB 332 */
Lcd_Write(CMD,0xBC); /* CMD   Set Data Output Scan Direction */
Lcd_Write(DATA,0x04); /* DATA: column scan, normal rotation, normal display */
Lcd_Write(DATA,0x00); /* DATA: RGB color arrangement R G B R G B ... */
/*-->*/ Lcd_Write(DATA,0x00); /* DATA: 8 bit per pixel mode MSB <RRRGGGBB> LSB */
/* --------- /DIFFERENT ---------- */
Lcd_Write(CMD,0xCE); /* CMD   Set 256 Color Look Up Table LUT */
Lcd_Write(DATA,0x00);	/* DATA red 000 */
Lcd_Write(DATA,0x03);	/* DATA red 001 */
Lcd_Write(DATA,0x05);	/* DATA red 010 */
Lcd_Write(DATA,0x07);	/* DATA red 011 */
Lcd_Write(DATA,0x09);	/* DATA red 100 */
Lcd_Write(DATA,0x0B);	/* DATA red 101 */
Lcd_Write(DATA,0x0D);	/* DATA red 110 */
Lcd_Write(DATA,0x0F);	/* DATA red 111 */
Lcd_Write(DATA,0x00);	/* DATA green 000 */
Lcd_Write(DATA,0x03);	/* DATA green 001 */
Lcd_Write(DATA,0x05);	/* DATA green 010 */
Lcd_Write(DATA,0x07);	/* DATA green 011 */
Lcd_Write(DATA,0x09);	/* DATA green 100 */
Lcd_Write(DATA,0x0B);	/* DATA green 101 */
Lcd_Write(DATA,0x0D);	/* DATA green 110 */
Lcd_Write(DATA,0x0F);	/* DATA green 111 */
Lcd_Write(DATA,0x00);	/* DATA blue 00 */
Lcd_Write(DATA,0x05);	/* DATA blue 01 */
Lcd_Write(DATA,0x0A);	/* DATA blue 10 */
Lcd_Write(DATA,0x0F);	/* DATA blue 11 */
Lcd_Write(CMD,0xCA); /* CMD   Set Display Control - Driver Duty Selection */
Lcd_Write(DATA,0xFF); // can't find description of the values in the original
Lcd_Write(DATA,0x10); // display/ssd1783.c in my datasheet :-(
Lcd_Write(DATA,0x01); //
Lcd_Write(CMD,0xAB); /* CMD   Set Scroll Start */
Lcd_Write(DATA,0x00); /* DATA: Starting address at block 0 */
Lcd_Write(CMD,0x20); /* CMD   Set power control register */
Lcd_Write(DATA,0x0B); /* DATA: booster 6x, reference gen. & int regulator */
Lcd_Write(CMD,0x81); /* CMD   Contrast Lvl & Int. Regul. Resistor Ratio */
Lcd_Write(DATA,0x29); /* DATA: contrast = 0x29 */
Lcd_Write(DATA,0x05); /* DATA: 0x05 = 0b101 -> 1+R2/R1 = 11.37 */
Lcd_Write(CMD,0xA7); /* CMD   Invert Display */
Lcd_Write(CMD,0x82); /* CMD   Set Temperature Compensation Coefficient */
Lcd_Write(DATA,0x00); /* DATA: Gradient is -0.10 % / degC */
Lcd_Write(CMD,0xFB); /* CMD   Set Biasing Ratio */
Lcd_Write(DATA,0x03); /* DATA: 1/10 bias */
Lcd_Write(CMD,0xF2); /* CMD   Set Frame Frequency and N-line inversion */
Lcd_Write(DATA,0x08); /* DATA: 75 Hz (POR) */
Lcd_Write(DATA,0x06); /* DATA: n-line inversion: 6 lines */
Lcd_Write(CMD,0xF7); /* CMD   Select PWM/FRC Select Full Col./8col mode */
Lcd_Write(DATA,0x28); /* DATA: always 0x28 */
Lcd_Write(DATA,0x8C); /* DATA: 4bit PWM + 2 bit FRC */
Lcd_Write(DATA,0x05); /* DATA: full color mode */
Lcd_Write(CMD,0x15); // Set X
Lcd_Write(DATA,0x00); // Start X
Lcd_Write(DATA,0x60); // End Y
Lcd_Write(CMD,0x75); // Set Y
Lcd_Write(DATA,0x00); // Start Y
Lcd_Write(DATA,0x40); // End X
Lcd_Write(CMD,0xAF); /* CMD   Display On */

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
Lcd_Write(CMD,0x81); /* CMD   Contrast Lvl & Int. Regul. Resistor Ratio */
Lcd_Write(DATA,contrast); /* DATA: contrast = 0x29 */
Lcd_Write(DATA,0x05); /* DATA: 0x05 = 0b101 -> 1+R2/R1 = 11.37 */
return;
}

void Lcd_Write(unsigned char cd,unsigned char c){
unsigned char li;
	__asm__ ("clrwdt");	
	cs = 0;
	sclk = 0;
	sda = cd;
	sclk = 1;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;
//		Delay10TCYx(20);
	}
	sclk=0;
	sda=0;
	cs = 1;
return;
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) { // Set buffer
	x++;
	y++;
	Lcd_Write(CMD,0x15);     // x-address
	Lcd_Write(DATA,x);
	Lcd_Write(DATA,x+dx);
	
	Lcd_Write(CMD,0x75);	// y-address
	Lcd_Write(DATA,y);
	Lcd_Write(DATA,y+dy);
	Lcd_Write(CMD,0x5C); // Write diplay RAM
return;
}