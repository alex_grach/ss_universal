/*
	SSD1289 LCD
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j,k;

//SetXY(0,0,320,240);
Lcd_H();
//cs = 0;
for (i=0;i<32;i++)
	for (j=0;j<24;j++) 
		for (k=0;k<100;k++) {
			cs = 0;
			cs = 1;
		}
//cs = 1;
//Lcd_Write(CMD,0xAE); // disable display;
//Lcd_Write(CMD,0xAF); // enable display;
return;
}

void Lcd_Write_Com_Data(unsigned int com, unsigned int data) {
Lcd_Write(CMD,com);
Lcd_Write(DATA,data);
return;
}

// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 1;
rst = 0;
delay_s(0x1);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_s(0x1);

Lcd_Write_Com_Data(0x0000,0x0001); //Open Crystal
Lcd_Write_Com_Data(0x0003,0xA8A4);  //0xA8A4
Lcd_Write_Com_Data(0x000C,0x0000);
Lcd_Write_Com_Data(0x000D,0x080C);
Lcd_Write_Com_Data(0x000E,0x2B00);
Lcd_Write_Com_Data(0x001E,0x00B7);
Lcd_Write_Com_Data(0x0001,0x2B3F);  //320*240  0x6B3F
Lcd_Write_Com_Data(0x0002,0x0600);
Lcd_Write_Com_Data(0x0010,0x0000);
Lcd_Write_Com_Data(0x0011,0x6060);       //0x4030
Lcd_Write_Com_Data(0x0005,0x0000);
Lcd_Write_Com_Data(0x0006,0x0000);
Lcd_Write_Com_Data(0x0016,0xEF1C);
Lcd_Write_Com_Data(0x0017,0x0003);
Lcd_Write_Com_Data(0x0007,0x0233);       //0x0233
Lcd_Write_Com_Data(0x000B,0x0000);
Lcd_Write_Com_Data(0x000F,0x0000);       //start address
Lcd_Write_Com_Data(0x0041,0x0000);
Lcd_Write_Com_Data(0x0042,0x0000);
Lcd_Write_Com_Data(0x0048,0x0000);
Lcd_Write_Com_Data(0x0049,0x013F);
Lcd_Write_Com_Data(0x004A,0x0000);
Lcd_Write_Com_Data(0x004B,0x0000);
Lcd_Write_Com_Data(0x0044,0xEF00);
Lcd_Write_Com_Data(0x0045,0x0000);
Lcd_Write_Com_Data(0x0046,0x013F);
Lcd_Write_Com_Data(0x0030,0x0707);
Lcd_Write_Com_Data(0x0031,0x0204);
Lcd_Write_Com_Data(0x0032,0x0204);
Lcd_Write_Com_Data(0x0033,0x0502);
Lcd_Write_Com_Data(0x0034,0x0507);
Lcd_Write_Com_Data(0x0035,0x0204);
Lcd_Write_Com_Data(0x0036,0x0204);
Lcd_Write_Com_Data(0x0037,0x0502);
Lcd_Write_Com_Data(0x003A,0x0302);
Lcd_Write_Com_Data(0x003B,0x0302);
Lcd_Write_Com_Data(0x0023,0x0000);
Lcd_Write_Com_Data(0x0024,0x0000);
Lcd_Write_Com_Data(0x0025,0x8000);
Lcd_Write_Com_Data(0x004f,0);        //
Lcd_Write_Com_Data(0x004e,0);        //
Lcd_Write(CMD,0x0022);  
/*
Lcd_Write(CMD,0x0007);
Lcd_Write(DATA,0x0021);   //далее записываем в регистры значения
Lcd_Write(CMD,0x0000);
Lcd_Write(DATA,0x0001);
Lcd_Write(CMD,0x0007);
Lcd_Write(DATA,0x0023);
Lcd_Write(CMD,0x0010);
Lcd_Write(DATA,0x0000);
delay_s(1);
Lcd_Write(CMD,0x0007);
Lcd_Write(DATA,0x0033);
Lcd_Write(CMD,0x0011);
Lcd_Write(DATA,0x6838);
Lcd_Write(CMD,0x0002);
Lcd_Write(DATA,0x0600);
*/

//Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
contrast = 0;
return;
}

void Lcd_Write(unsigned char cd, unsigned int c){
unsigned char li;
unsigned char ch;
	__asm__ ("clrwdt");	
	dc = cd;
//	cs = 0;
	ch = c>>8;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(ch & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		ch <<= 1;
	}
	ch = c & 0xFF;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(ch & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		ch <<= 1;
	}
	sclk=0;
	sclk=1;
	sclk=0;
	sda=0;
	cs = 0;
	cs = 1;
return;
}

void Lcd_S(void) {
	Lcd_Write(DATA, 0x0000);
}

void Lcd_H(void) {
	Lcd_Write(DATA, 0xFFFF);
}

void Lcd_B(void) {
	Lcd_Write(DATA, 0x000F);
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) {
	y++;
	Lcd_Write(CMD,0x0044);
	Lcd_Write(DATA,((y+dy)<<8)+y);
	Lcd_Write(CMD,0x0045);
	Lcd_Write(DATA,x);
	Lcd_Write(CMD,0x0046);
	Lcd_Write(DATA,x+dx);
	Lcd_Write(CMD,0x004E);
	Lcd_Write(DATA,y);
	Lcd_Write(CMD,0x004F);
	Lcd_Write(DATA,x);
	Lcd_Write(CMD,0x0022);
}