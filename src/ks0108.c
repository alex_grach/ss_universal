/*
	128x64 KS0108 LCD
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#define	cs1	RC5
#define cs2 RC6
#define en	RC4

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
//Lcd_Write(CMD,0xAE); // disable display;
cs2=0;
cs1=0;
for(i=0;i<8;i++) {
	SetXY(0,i);
	for(j=0;j<64;j++) {
		Lcd_Write(DATA,0x00);
	}
}
/*cs1=1;
cs2=0;
for(i=0;i<8;i++) {
	SetY(i);
	for(j=0;j<64;j++) {
		Lcd_Write(DATA,0x00);
	}
}*/
//Lcd_Write(CMD,0xAF); // enable display;
return;
}

// init LCD
void Lcd_Init(void) {
en = 0;
cs1 = 0;
cs2 = 0;
rst = 0;
delay_us(0x50);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x50);
//cs2 = 1;
Lcd_Write(CMD,0x3F); // Display ON
Lcd_Write(CMD,0xC0); // Set display start line = 0;
/*cs1 = 1;
cs2 = 0;
Lcd_Write(CMD,0x3F); // Display ON
Lcd_Write(CMD,0xC0); // Set display start line = 0;
*/
Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
	Lcd_Write(CMD,0x24); // write VOP register 
	Lcd_Write(CMD,contrast);
	Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
	Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
	Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option
}

void Lcd_Write(unsigned char cd, unsigned char c) {
	__asm__ ("clrwdt");
	dc = cd;
	PORTD = c;
	delay_us(0x02);
	en = 1;
	delay_us(0x02);
	en = 0;
	delay_us(0x02);
return;
}


void SetXY(unsigned char x, unsigned char y){
	x+=11;
	if (x>64) {
		cs1=1;
		cs2=0;
		x-=64;
	} else {
		cs2=1;
		cs1=0;
	}
	delay_us(0x20);
	Lcd_Write(CMD,0x40 | x );
	Lcd_Write(CMD,0xB8 | y);
return;
}
