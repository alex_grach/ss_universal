/*
	TIC PCF8535 LCD
	TIC150
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

extern volatile typedef union {
	unsigned char ACTION_STATUS;
	struct {
	  unsigned char DUAL:1; // Second lcd select bit
	  unsigned char PRESET:1; // Load preset flag
	  unsigned char SAVECK:1; // Save check flag
	  unsigned char ZERO:1; // Zero-cross detected
	  unsigned char T12_2:1; // PWMPlus T12 handle
	  unsigned char T12:1; // Iron T12 handle
	};
} __SS_as_bits_t;
extern volatile __SS_as_bits_t SS_as_bits;

#define DUAL		SS_as_bits.DUAL

//clear LCD
void Lcd_Clear(void){
unsigned int i,j;
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x00);
	I2CWrite(0x01);
	I2CWrite(0x40);
	I2CWrite(0x80);
	I2CStart();
	I2CWrite(0x78 );
	I2CWrite(0x40); // write data
	for(i=0;i<8;i++)
		for(j=0;j<127;j++) {
			I2CWrite(0x00);
		}
	I2CStop();
return;
}


// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
#ifdef _DUAL
	if (!DUAL) { // Reset needed only time
		rst = 0;
		delay_us(0x80);			// 5mS so says the stop watch(less than 5ms will not work)
		rst = 1;
		delay_us(0x80);
	}
#else
	rst = 0;
	delay_us(0x80);			// 5mS so says the stop watch(less than 5ms will not work)
	rst = 1;
	delay_us(0x80);
#endif

	I2CStart(); // 1
	I2CWrite(0x78); // 2 PCF8535 Device address
	I2CWrite(0x00); // 3 write command
	I2CWrite(0x01); // 4 H[2:0] independent command; select function and RAM command page H[1:0] = 111
	I2CWrite(0x10); // 5 function and RAM command page; PD = 0, V = 0
	I2CWrite(0x0E); // 6 function and RAM command page; select display setting command page H[1:0] = 110
	I2CWrite(0x12); // 7 display setting command page; set bias system to 1/9 (BS[2:0] = 010)
	I2CWrite(0x06); // 8 display setting command page; set normal mode (D = 1, E = 0)
	I2CWrite(0x0C); // 8 External display control; set normal mode (MX = 0, MY = 1)
	I2CWrite(0x84); // 9 select Mux rate 1 : 65
	I2CWrite(0x01); // 10 H[2:0] independent command; select function and RAM command page H[1:0] = 111
	I2CWrite(0x0D); // 11 function and RAM command page; select HV-gen command page H[2:0] = 101
	I2CWrite(0x08); // 12 HV-gen command page; select voltage multiplication factor 3 S[1:0] = 01
	I2CWrite(0x12); // 13 HV-gen command page; select temperature coefficient 2 TC[2:0] = 010
	I2CWrite(0x80); // 14 HV-gen command page; set VLCD = 12.02 V; VOP[6:0] = 0101000
	I2CWrite(0x07); // 15 HV-gen command page; select HIGH VLCD programming range (PRS = 1), voltage multiplier on (HVE = 1)
	I2CWrite(0x01); // 16 H[2:0] independent command; select function and RAM command page H[1:0] = 111
	I2CWrite(0x0B); // 17 function and RAM command page; special feature command page H[2:0] = 011
	I2CWrite(0x58); // 18 COG/TCP TRS = 1, BRS = 1
//	I2CWrite(0x0A); // 19 Oscillator setting EC = 1
//	I2CWrite(0x04); // 20 State control DOF = 0, DM = 0
	I2CStop();

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
	contrast = 0;
/*	I2CStart();
	I2CWrite(0x78); // PCF8535 Device address
	I2CWrite(0x00); // write command?
	I2CWrite(0x01); // write command?
	I2CWrite(0x0E); // Extended instruction set H=1
	I2CWrite(0x80 & contrast); // Set contrast Vop
	I2CStop();*/
}

void Lcd_Write(unsigned char cd, unsigned char c){
	__asm__ ("clrwdt");	
	cd=0;
	I2CWrite(c);
return;
}


void SetXY(unsigned char x, unsigned char y){
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x00);
	I2CWrite(0x01);
	I2CWrite(0x40 | y);//row
	I2CWrite(0x20); // page 0
	I2CWrite(0x80 | x);//column
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x40);
return;
}
