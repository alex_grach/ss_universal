/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */
/* ----------------- ATMega8(a)/48/88/168/328 MCUs ----------------------- */
/* ----------------------------------------------------------------------- */

#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

void PWM_On(void);
void PWM_Off(void);
unsigned int ADC_Get(unsigned char ch);

void ON_OFF(unsigned char selected, unsigned char y, unsigned char width);

#ifdef	atmega328
#define	EESIZE	1024
#else
#define	EESIZE	512
#endif

unsigned char EEMEM	__EEPROM[EESIZE] = {
0x3E, 0x51, 0x49, 0x45, 0x3E,// 0
0x00, 0x42, 0x7F, 0x40, 0x00,// 1
0x42, 0x61, 0x51, 0x49, 0x46,// 2
0x21, 0x41, 0x45, 0x4B, 0x31,// 3
0x18, 0x14, 0x12, 0x7F, 0x10,// 4
0x27, 0x45, 0x45, 0x45, 0x39,// 5
0x3C, 0x4A, 0x49, 0x49, 0x30,// 6
0x01, 0x71, 0x09, 0x05, 0x03,// 7
0x36, 0x49, 0x49, 0x49, 0x36,// 8
0x06, 0x49, 0x49, 0x29, 0x1E,// 9
0x00, 0x00, 0x00, 0x00, 0x00,// пробел
0x00, 0x08, 0x08, 0x08, 0x00,// -

// Stored temperature values

// prog[0] - address: 0x3C
// IronL, IronH, HotAirL, HotAirH, PWMPlusL, PWMPlusH, Fan, PWM
0x68, 0x01, 0x1C, 0x02, 0x1C, 0x02, 0x32, 0x1F,
// prog[1] - address: 0x44
0x1C, 0x02, 0x90, 0x01, 0x68, 0x01, 0x64, 0x20,
// prog[2] - address: 0x4C
0x1D, 0x02, 0x64, 0x01, 0x1C, 0x02, 0x54, 0xF0,
/*
// Current - address: 0x3C
0x68, 0x01, 0x1C, 0x02, 0x32,

// -- Prog0 & Prog1 & Prog2 - address: 0x41 --
// Iron_P0, HotAir_P0, Fan_P0
0x68, 0x01, 0x90, 0x01, 0x32,
// Iron_P1, HotAir_P1, Fan_P1
0x1C, 0x02, 0x54, 0x01, 0x64,
// Iron_P2, HotAir_P2, Fan_P2
0x1D, 0x02, 0x64, 0x01, 0x54,
// -----------------------------------
*/

0x00, // Last prog used
// Fan off temperature
// Address: 0x4D
0x28,  // Fan off temp
0x32, // Sleep timer 50 min

#ifdef _PCD8544	// Nokia 3310
0xC8,
#elif _SSD1828
0x29,
#else
0x90,
#endif

// SS Configuration bit (eeconfig)
// Mirror X axis
#ifdef _MX 
#define	__MX	0b00000001
#else
#define	__MX	0b00000000
#endif
// Invert
#ifdef _INV 
#define	__INV	0b00000010
#else
#define	__INV	0b00000000
#endif
// PWM+ Preheat / Iron
#ifdef _PWMPH 
#define	__PWMPH	0b00000100
#else
#define	__PWMPH	0b00000000
#endif
// Mirror Y axis
#ifdef _MY 
#define	__MY	0b00001000
#else
#define	__MY	0b00000000
#endif
// Hide (T<50)
#ifdef _HIDE 
#define	__HIDE	0b00100000
#else
#define	__HIDE	0b00000000
#endif

(0x00^__PWMPH^__MY^__MX^__INV^__HIDE^0x00),

0x05, // Iron sleep timer
// < 
0x7F, 0x3E, 0x1C, 0x08, 0x00,
// >
0x00, 0x08, 0x1C, 0x3E, 0x7F,
// % 
0x23, 0x13, 0x08, 0x64, 0x62,
// space
0x00, 0x00, 0x00, 0x00, 0x00,
// dash
0x00, 0x08, 0x08, 0x08, 0x00,
// dot
0x3C, 0x24, 0x24, 0x3C, 0x00,
// +
0x08, 0x08, 0x3E, 0x08, 0x08,
#ifdef _RU
// 0xa - 'А'
0x7E, 0x11, 0x11, 0x11, 0x7E,
// 0xb - 'Б'
0x7f, 0x49, 0x49, 0x49, 0x31,
// В
0x7F, 0x49, 0x49, 0x49, 0x36,
// Г
0x00, 0x7F, 0x01, 0x01, 0x01,
// Д
0x60, 0x3E, 0x21, 0x3F, 0x60,
// 0xf - 'Е'
0x7f, 0x49, 0x49, 0x49, 0x41,
// 0x12 - 'И'
0x7f, 0x20, 0x10, 0x08, 0x7f,
// 0x14 - 'К'
0x7f, 0x08, 0x14, 0x22, 0x41,
// 0x15 - 'Л'
0x40, 0x7e, 0x01, 0x01, 0x7f,
// 'М'
0x7F, 0x02, 0x04, 0x02, 0x7F, 
// 0x17 - 'Н'
0x7f, 0x08, 0x08, 0x08, 0x7f,
// 0x18 - 'О'
0x3E, 0x41, 0x41, 0x41, 0x3E,
// 0x19 - 'П'
0x7F, 0x01, 0x01, 0x01, 0x7f,
// 0x1A - 'Р'
0x7F, 0x09, 0x09, 0x09, 0x06,
// 0x1B - 'С'
0x3e, 0x41, 0x41, 0x41, 0x22,
// 0x1C - 'Т'
0x01, 0x01, 0x7f, 0x01, 0x01,
// 0x1D - 'У'
0x27, 0x48, 0x48, 0x48, 0x3f,
// 0x1E - 'Ф'
0x0e, 0x11, 0x7f, 0x11, 0x0e,
// 'Х'
0x63, 0x14, 0x08, 0x14, 0x63,
// 'Ш'
0x7F, 0x40, 0x7F, 0x40, 0x7F,
// 0x25 - 'Ы'
0x7f, 0x48, 0x48, 0x30, 0x7f,
// 0x26 - 'Ь'
0x7f, 0x48, 0x48, 0x48, 0x30,
// 0x29 - 'Я'
0x66, 0x19, 0x09, 0x09, 0x7f,
#else
// 0x14 - 'A'
0x7E, 0x11, 0x11, 0x11, 0x7E,
// 0x1B - 'С'
0x3e, 0x41, 0x41, 0x41, 0x22,
// 0x25 - 'D'
0x7F, 0x41, 0x41, 0x22, 0x1C,
// 0x1B - 'E'
0x7f, 0x49, 0x49, 0x49, 0x41,
// 'F'
0x7F, 0x09, 0x09, 0x09, 0x01,
// 0x1C - 'G'
0x3E, 0x41, 0x41, 0x51, 0x32,
// 0x26 - 'H'
0x7F, 0x08, 0x08, 0x08, 0x7F,
// 0x1E - 'I'
0x00, 0x41, 0x7F, 0x41, 0x00,
// 'L'
0x7F, 0x40, 0x40, 0x40, 0x40,
// 'M'
0x7F, 0x02, 0x04, 0x02, 0x7F, 
// 0x15 - 'N'
0x7F, 0x04, 0x08, 0x10, 0x7F,
// 0x29 - 'O'
0x3e, 0x41, 0x41, 0x41, 0x3e,
// 0x1A - 'Р'.
0x7F, 0x09, 0x09, 0x09, 0x06,
// 0xa - 'R'
0x7F, 0x09, 0x19, 0x29, 0x46,
// 0x1D - 'S'
0x46, 0x49, 0x49, 0x49, 0x31,
// 0x1C - 'T'
0x01, 0x01, 0x7f, 0x01, 0x01,
// 'V'
0x1F, 0x20, 0x40, 0x20, 0x1F,
// 'W'
0x7F, 0x20, 0x18, 0x20, 0x7F,
// 'X'
0x63, 0x14, 0x08, 0x14, 0x63, 
// 'Y'
0x03, 0x04, 0x78, 0x04, 0x03,
#endif
};

__SS_bits_t SS_bits;
__SS_config_bits_t SS_config_bits;
__SS_status_bits_t SS_status_bits;
__SS_action_bits_t SS_action_bits;
__SS_sleep_status_bits_t SS_sleep_status_bits;
__SS_as_bits_t SS_as_bits;

unsigned char i, j, k, set, fan_speed, fan_defaults, pwm_defaults;
volatile unsigned char menu_max, bt_delay, on_off_delay, on_off=0;
volatile unsigned char sec, min, ssleep, isleep, pwm_iron=0, prevpwm=0, pwm_ha=0, pwm_fan=0, prevpwm_fan=0, testpwm=0;

unsigned int itemp=0; // Температура паяльника
unsigned int itempprev=0; // Предыдущее показание температуры паяльника
unsigned int itempav=0; // Усредненная температура паяльника
unsigned int i2temp=0; // Температура паяльника 2
unsigned int i2tempprev=0; // Предыдущее показание температуры паяльника 2
unsigned int i2tempav=0; // Усредненная температура паяльника 2
unsigned int hatemp=0; // Температура фена
unsigned int hatempprev=0; // Предыдущее показание температуры фена
unsigned int hatempav=0; // Усредненная температура фена
unsigned int hadt=0; // Разность температур
unsigned int prog[3]; // Текушие установки температуры
unsigned int iron_sleep=200; // Iron sleep temperature
unsigned int defaults[3]; // Сохраненные значения температуры из EEPROM
unsigned char fan_off; // Температура выключения фена при остывании
unsigned char iron_correction; // Коррекция температуры паяльника в покое (ШИМ)
unsigned char sleep; // Время засыпания в минутах
unsigned char sleepi; // Время засыпания наяльника в минутах
unsigned char menu_selected=0; // Выбранный пункт меню
unsigned char cfg;
unsigned char contrast; // Контрастность дисплея
unsigned char prog_set=0; // Выбранный профиль

volatile unsigned char ha_set=0x7F, ha_on=0x00, ha_off=0xFF;
volatile unsigned char pwm_set=0x7F, pwm_on=0x00, pwm_off=0xFF;
volatile unsigned char pwmPlus_set=0x7F, pwmPlus_on=0x00, pwmPlus_off=0xFF;

int main(void) {
// -- Init config and status bytes --
	on_off_delay=20;
	bt_delay=20;
	SS=0;
	CFG=0;
	SSTATUS=0;
	ACTION=0;
	SLEEP_STATUS=0;
	ACTION_STATUS=0;
// --------------------

#ifdef _ENCODER
	ENC=0;
#else
	ENC=1;
#endif

#ifdef _1CH
	if (cfg&0x04)
		menu_selected=1;
#endif

// Get stored values from EEPROM
	prog_set=EERead(eeprog);
	fan_off=EERead(eetoff)<<1;
	CFG=cfg=EERead(eeconfig);
	sleep=EERead(eesleep);
	sleepi=EERead(eeisleep);
	contrast=EERead(eelcd);
#ifdef _IRON_SW
	isleep=sleepi;
#endif
// -----------------------------
// Define NOSLEEP flag in case of sleep delay value = 0
	if (!sleep)
		NOSLEEP=1;
// -----------------------------
// Set configuration bits for main operation mode
	SHOW=1;
	MENU=1;
	PRESET=1;
// -----------------------------
// Set configuration bits for default on behaveour
	IRON=0;
	HOTAIR=0;
	SAVE=0;
// -----------------------------
// Set first run configuration bit
	INIT=1;
// -----------------------------
// Configure microcontroller
#ifdef _IRON_SW
	DDRB = 0xEE; // PB1-PB3, PB5-PB7 is now outputs, PB4 is now input
	PORTB = 0x11; // Enable PB0, PB4 pull-ups
#else
	DDRB = 0xEF; // PB0-PB3, PB5-PB7 is now outputs, PB4 is now input
	PORTB = 0x10; // Enable PB4 pull-up
#endif
#ifdef	_4CH
	DDRC = 0x3E; // PC1-PC5 is now outputs
#else
	DDRC = 0x3C; // PC2-PC5 is now outputs
#endif
	DDRD = 0; // PD0-PD7 is now inputs
	PORTD = 0xFF; // Turn on pull-ups on PD0-PD7
// -----------------------------
// Configure PWM
#if !defined(_SIMPLE) && !defined(_PREHEAT)
	OCR1A = 0;
	TCCR1A = 0x81; // Fast PWM, 8-bit, Non-inverting mode
	TCCR1B = 0x08;
#endif
#ifdef	atmega8
	OCR2 = 0;
	TCCR2 = 0x08; // Fast PWM, Non-inverting mode, Clock/64
#else
	OCR2A = 0;
	TCCR2A = 0x81; // Fast PWM, 8-bit, Non-inverting mode, Clock/64
	TCCR2B = 0x08;	
#endif
// Configure INT0 interrupt
#ifdef	_ENCODER
	#ifdef	atmega8
		MCUCR |= ((1 << ISC00) | (1 << ISC01));    // set INT0 to trigger on rising edge
		GICR |= (1 << INT0);      // Turns on INT0
	#else
		EICRA |= ((1 << ISC00) | (1 << ISC01));    // set INT0 to trigger on rising edge
		EIMSK |= (1 << INT0 );     // Turns on INT0
	#endif
#endif

// -----------------------------
// Init LCD
	Lcd_Init();
#ifdef _DUAL
	DUAL=1;
	Lcd_Init();
	DUAL=0;
#endif
// -----------------------------

#ifdef	atmega8
	TIMSK |= (1 << TOIE0);
	TCCR0 = 0x04; // Timer0 prescaler 1:256 start
#else
	TIMSK0 |= (1 << TOIE0);
	TCCR0B = 0x04;
#endif
// --------------------
// ----- Show logo ----
	led=1;
	#ifdef	_LOGO
		Lcd_Logo();
		delay_ms(0xFFFF);
	#endif
// --------------------
	sei(); // Enable interrupts
	
	while (INIT) ; // Wait (configure or work)
	while (1) {
//		led=(CHECK)?1:0;
		wdt_reset();		
		if (CHECK) { // Read temperature values
			#ifdef _1CH
			if (!(cfg&0x04))
			#endif
			{
				// Iron
				itempprev=itemp;
				T12=1;
				itemp=ADC_Get(ironAD);
				T12=0;
// Simple ON-OFF controll
#if defined(_SIMPLE) || defined(_PREHEAT)				
				if (IRON) {
					if (itemp>prog[0]) iron=0; else iron=1;
				} else {
					iron=0;
				}
// Induction heater
#elif	_INDUCTOR
/*				if (IRON) {
					if (itemp>prog[0]) pwm_iron=0; else pwm_iron=0x02;
				} else {
					pwm_iron=0;
				}
				 */
				if (IRON) {
					if (itemp>prog[0]) CCP1CON=0; else CCP1CON=0x0C;
				} else {
					CCP1CON=0;
				}
				 
// Use PWM
#else
				if (IRON) {
				#ifdef _IRON_SW
					if (ISLEEP) // Iron sleep mode
						if (!isleep)
							pwm_iron = 0;
						else
							pwm_iron = PID(iron_sleep, itemp, 0);
					else
				#endif
						if (itemp<50)
							pwm_iron=MAXPWM>>1;
						else
							pwm_iron = PID(prog[0], itemp, 0);
				} else {
					pwm_iron=0;
				}
#endif
			}
			#ifdef _1CH
			else
			#endif
			{
				// HotAir
				hatemp=ADC_Get(hotAirAD);
				if (HOTAIR && !HASWITCH) {
					if (hatemp>prog[1]) 
						pwm_ha=0; 
					else {
						hadt = prog[1] - hatemp;
						if (hadt>32)
							pwm_ha=0xFF;
						else
							pwm_ha=(unsigned char)hadt;
					}
//					hotAir=PD(prog[1], hatemp, hatempprev);
					pwm_fan=(fan_speed<<1)+0x28;
				} else {
					pwm_ha=0;
					hotAir=0;
					if (hatemp<fan_off) { pwm_fan=0; fan=0;}
					else if (hatemp>fan_off+100) pwm_fan=MAXPWM;
				}
			}
#if defined(_4CH)
				i2tempprev=i2temp;
				if (!(cfg&0x04)) { // Режим паяльника
//					if (T12_2)
//					T12=1;
						i2temp=ADC_Get(pwmPlusAD);
//					T12=0;
					if (PWMPLUS) {
						if (i2temp<40) pwmPlus_set=MAXPWM>>1;
						else pwmPlus_set = PID(prog[2], i2temp, 1);
					}
				} else { // Режим нижнего подогрева
					i2temp=ADC_Get(pwmPlusAD);
					if (PWMPLUS)
						if (i2temp>prog[2]) 
							pwmPlus=0; 
						else 
							pwmPlus=1;
					else
						pwmPlus=0; 
				}
#endif
				// Если изменились значения ШИМ - выставить флаг обновления
				if (pwm_iron!=prevpwm) 
					IPWM=1;
				if (pwm_fan!=prevpwm_fan)
					FPWM=1;
				prevpwm=pwm_iron;
				prevpwm_fan=pwm_fan;

				itempav += itemp;
				itempav >>= 1;
				hatempav += hatemp;
				hatempav >>= 1;
#if defined(_4CH)
				i2tempav += i2temp;
				i2tempav >>= 1;
#endif

		CHECK=0;
		}
		// Показать установленные значения температуры
		if (SHOWSET) EEShowSet();
		if (SHOW) { // Redraw
			if (SSCONFIG) EEShowConfig(); else { // Work
			if (PRESET) { // Load preset values
				loadPreset(prog_set);
				PRESET=0;
			}
			if (MENU) { // Show menu
				SHOWSET=1;
				Lcd_Clear();
				#ifndef _DUAL // Single LCD
					if (!MENU_2) {
					#ifdef	_1CH
						EE_Menu_Single();
					#else
						EE_Menu();
					#endif
					}
					else
						EE_Menu_2();
				#else // Dual LCD
					EE_Menu();
					DUAL=1;
					EE_Menu_2();
					DUAL=0;
				#endif
				MENU=0;
			}
			#ifdef _ENCODER
				Menu_Selected(menu_selected);
			#endif
			
			// Show preset number
			showPreset(prog_set);

			if (SLEEP) {
				IRON=0;
				HOTAIR=0;
				led=0;
			}
			else led=1;
			
			EEShow();
			
			// Check if SAVE is needed
			if (SAVECK) {
				SAVE=0;
				cli(); // Disable interrupts
				if (defaults[0]!=prog[0]) SAVE=1;
				if (defaults[1]!=prog[1]) SAVE=1;
				if (defaults[2]!=prog[2]) SAVE=1;
				if (fan_defaults!=fan_speed) SAVE=1;
				if (pwm_defaults!=pwm_set) SAVE=1;
				sei(); // Enable interrupts
				SAVECK=0;
			}

			// Save configuration
				if (SAVE) { // Save temperature
					savePreset(prog_set);
					SAVE=0;
				}
				if (CSAVE) { // Save configuration
					EEWrite(&contrast, eelcd);
					EEWrite(&cfg, eeconfig);
					EEWrite(&sleep, eesleep);
					EEWrite(&sleepi, eeisleep);
					fan_off>>=1;
					EEWrite(&fan_off, eetoff);
					fan_off<<=1;
					CSAVE=0;
				}
			// Manage sleep modes
			// Iron sleep
			#ifdef	_IRON_SW
			// Configure
				#ifdef _1CH
					if (!(cfg&0x04))
						ISWITCH=(!ironSwitch)?1:0;
					else
						ISWITCH=1;
				#else
					ISWITCH=(!ironSwitch)?1:0;
				#endif
	
			// Set action flag
				if (ISWITCH)
					ISLEEP=(!isleep)?1:0;
			#endif
			// HotAir
			// Configure
			#ifdef _1CH
				if (cfg&0x04)
					HASWITCH=(!hotAirSwitch)?1:0;
				else
					HASWITCH=1;
			#else
				HASWITCH=(!hotAirSwitch)?1:0;
			#endif
	
			#ifdef _IRON_SW
				SLEEPEN=(!NOSLEEP && ISWITCH && HASWITCH)?1:0;
			#else
				SLEEPEN=(!NOSLEEP && HASWITCH)?1:0;
			#endif
			// Set action flag
			SLEEP=(SLEEPEN && !ssleep)?1:0;
			}
		SHOW=0;
//		Lcd_Write(CMD,0xA9);
//		cs=1;
		}
//			debug(200);
	}
}

void debug(unsigned char count) {
	led=1;
	_delay_loop_2(count);
	led=0;
	_delay_loop_2(count);
	return;
}


unsigned int ADC_Get(unsigned char cfg) {
unsigned int res;
#ifndef	_PREHEAT
	if (T12) {
//		EE_Dec_Small(cfg, 0, 0);
		#ifndef	_SIMPLE
			TCCR1A = 0x01;
			TCCR1B = 0x08;
		#endif
		iron=0;
		_delay_loop_2(20000);
	}
#endif
	ADMUX = cfg;
	ADMUX |= (1 << REFS0);    // use AVcc as the reference
    ADMUX &= ~(1 << ADLAR);   // clear for 10 bit resolution
    
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);    // 128 prescale for 8Mhz
    ADCSRA |= (1 << ADEN);    // Enable the ADC

    ADCSRA |= (1 << ADSC);    // Start the ADC conversion

    while(ADCSRA & (1 << ADSC));      // Wait until done

	res=ADCL;
	res=(ADCH << 8) + res;
	#if	!defined(_SIMPLE) && !defined(_PREHEAT)
	if (T12 && IRON) {
		TCCR1A = 0x81; // Fast PWM, 8-bit, Non-inverting mode
		TCCR1B = 0x0B;
	}
	#endif
	return res;
}

ISR (INT0_vect) { // int0 external interrupt
	wdt_reset();
	if (!ENC) {
		if (ENCC==1) // Plus action
			PACT=1;
		else // Minus action
			MACT=1;
	}

	if (PACT) { // Plus action
		PACT=0;
		if (!SSCONFIG) { // Normal operation mode
				switch (menu_selected) {
					case 0:
						if (prog[0]!=IronMax) prog[0]+=2; break;
					case 1:
						if (prog[1]!=HotAirMax) prog[1]+=2; break;
					case 2:
						if (fan_speed!=FanMax) fan_speed++; break;
					case 3:
						if (prog[2]!=IronMax) prog[2]+=2; break;
					case 4:
						pwm_set++; break;
				}
		} else { // Configuration mode
			switch (menu_selected) {
			#ifdef _PCD8544	// Nokia 3310
				case 0:
					if (++contrast>0xD0) contrast=0xC0; LCDINIT=1; break;
				case 1:
					cfg^=0x02; LCDINIT=1; break;
				case 2:
					sleep++; break;
				case 3:
					fan_off+=2; break;
				case 4:
					if (sleepi < sleep) sleepi++; break;
				case 5:
					cfg^=0x04; break;
			#elif _SSD1828 // Motorola c330
				case 0:
					if (++contrast>0x3F) contrast=0x01; LCDINIT=1; break;
				case 1:
					cfg^=0x01; LCDINIT=1; MENU=1; break;
				case 2:
					cfg^=0x08; LCDINIT=1; MENU=1; break;
				case 3:
					cfg^=0x02; LCDINIT=1; break;
				case 4:
					sleep++; break;
				case 5:
					fan_off+=2; break;
				case 6:
					if (sleepi < sleep) sleepi++; break;
				case 7:
					cfg^=0x04; break;
			#elif _LARGE
				case 0:
					cfg^=0x02; LCDINIT=1; break;
				case 1:
					sleep++; break;
				case 2:
					fan_off+=2; break;
				case 3:
					if (sleepi < sleep) sleepi++; break;
				case 4:
					cfg^=0x04; break;
			#elif _KS0108 // 128x64
				case 0:
					sleep++; break;
				case 1:
					fan_off+=2; break;
				case 2:
					if (sleepi < sleep) sleepi++; break;
				case 3:
					cfg^=0x04;
					#ifdef _4CH
						MENU=1;
					#endif
					break;
			#else
				case 0:
					if (++contrast>0x90) contrast=0x80; LCDINIT=1; break;
				case 1:
					cfg^=0x01; LCDINIT=1; MENU=1; break;
				case 2:
					cfg^=0x08; LCDINIT=1; MENU=1; break;
				case 3:
					cfg^=0x02; LCDINIT=1; break;
				case 4:
					sleep++; break;
				case 5:
					fan_off+=2; break;
				case 6:
					if (sleepi < sleep) sleepi++; break;
				case 7:
					cfg^=0x04;
					#ifdef _4CH
						MENU=1;
					#endif
					break;
			#endif
			}
		}
	}
	if (MACT) { // Minus action
		MACT=0;
		if (!SSCONFIG) { // Normal operation mode
				switch (menu_selected) {
				case 0:
					if (prog[0]!=IronMin) prog[0]-=2; break;
				case 1:
					if (prog[1]!=HotAirMin) prog[1]-=2; break;
				case 2:
					if (fan_speed!=FanMin) fan_speed--; break;
				case 3:
					if (prog[2]!=IronMin) prog[2]-=2; break;
				case 4:
					pwm_set--; break;
				}
		} else { // Configuration mode
			switch (menu_selected) {
			#ifdef _PCD8544	// Nokia 3310
				case 0:
					if (--contrast<0xC0) contrast=0xD0; LCDINIT=1;  break;
				case 1:
					cfg^=0x02; LCDINIT=1; break;
				case 2:
					sleep--; break;
				case 3:
					fan_off-=2; break;
				case 4:
					if (sleepi) sleepi--; break;
				case 5:
					cfg^=0x04; break;
			#elif _SSD1828 // Motorola c330
				case 0:
					if (--contrast<0x01) contrast=0x3F; LCDINIT=1;  break;
				case 1:
					cfg^=0x01; LCDINIT=1; MENU=1; break;
				case 2:
					cfg^=0x08; LCDINIT=1; MENU=1; break;
				case 3:
					cfg^=0x02; LCDINIT=1; break;
				case 4:
					sleep--; break;
				case 5:
					fan_off-=2; break;
				case 6:
					if (sleepi) sleepi--; break;
				case 7:
					cfg^=0x04; break;
			#elif _LARGE
				case 0:
					cfg^=0x02; LCDINIT=1; break;
				case 1:
					sleep--; break;
				case 2:
					fan_off-=2; break;
				case 3:
					if (sleepi) sleepi--; break;
				case 4:
					cfg^=0x04; break;
			#elif _KS0108 // 128x64
				case 0:
					sleep--; break;
				case 1:
					fan_off-=2; break;
				case 2:
					if (sleepi) sleepi--; break;
				case 3:
					cfg^=0x04;
					#ifdef _4CH
						MENU=1;
					#endif
					break;
			#else
				case 0:
					if (--contrast<0x80) contrast=0x90; LCDINIT=1;  break;
				case 1:
					cfg^=0x01; LCDINIT=1; MENU=1; break;
				case 2:
					cfg^=0x08; LCDINIT=1; MENU=1; break;
				case 3:
					cfg^=0x02; LCDINIT=1; break;
				case 4:
					sleep--; break;
				case 5:
					fan_off-=2; break;
				case 6:
					if (sleepi) sleepi--; break;
				case 7:
					cfg^=0x04;
					#ifdef _4CH
						MENU=1;
					#endif
					break;
			#endif
			}
		}
	}
	#ifndef	_ENCODER
	if (ONOFFACT) {
		#ifdef _4CH
			#if !defined(_SINGLE) && !defined(_DUAL)
				MENU_2=!MENU_2;
				MENU=1;
			#endif						
		#endif
		IACT=0;
		HAACT=0;
		#ifdef _BUTTONS_14
			PWMACT=0;
			PWMPACT=0;
		#endif
		bt_delay=10;
		KEYPRESSED=0;
	} 
	if (IACT) {
		IACT=0;
		SAVECK=1;
		#ifdef _BUTTONS_14
		IRON=!IRON;
		#else
		if (!MENU_2)
			IRON=!IRON;
		else {
			PWMPLUS=!PWMPLUS;
			#if defined(_3CH)
				PWM=0;
			#endif
		}
		#endif
	}
	if (HAACT) {
		HAACT=0;
		SAVECK=1;
		#ifdef _BUTTONS_14
		HOTAIR=!HOTAIR;
		#else
		if (!MENU_2)
			HOTAIR=!HOTAIR;
		else {
			PWM=!PWM;
			#if defined(_3CH)
				PWMPLUS=0;
			#endif
		}
		#endif
	}
	#ifdef _BUTTONS_14
	if (PWMACT) {
		PWMACT=0;
		SAVECK=1;
		PWM=!PWM;
	}
	if (PWMPACT) {
		PWMPACT=0;
		SAVECK=1;
		PWMPLUS=!PWMPLUS;
	}
	#endif
	#endif
}

ISR (TIMER0_OVF_vect) { // timer0 overflow interrupt
	if(!T12)
		if (IPWM) {
			OCR1AL=pwm_iron;
			if (!pwm_iron) {
				TCCR1A = 0x01;
				TCCR1B = 0x08;
				iron=0;
			}
			else
				TCCR1A = 0x81; // Fast PWM, 8-bit, Non-inverting mode
				TCCR1B = 0x0B;
			IPWM=0;
		}
	if (FPWM) {
#ifdef	atmega8
		OCR2=pwm_fan;
#else
		OCR2A=pwm_fan;
#endif
		if (!pwm_fan) {
			#ifdef	atmega8
				TCCR2=0x00;
			#else
				TCCR2A = 0x01;
				TCCR2B = 0x08;
			#endif
			fan=0;
		}
		else
			#ifdef	atmega8
				TCCR2=0x6C;
			#else
				TCCR2A = 0x81; // Fast PWM, 8-bit, Non-inverting mode
				TCCR2B = 0x0B;
			#endif
		FPWM=0;
	}
// CHECK!!!			
#ifdef _ENCODER
	if (ENC) {
		ENC=0;
	}
#endif
				
	if (on_off_delay) on_off_delay--;
	if (bt_delay) bt_delay--;
	
	// HotAir heater control
	hotAir=(pwm_ha>sec)?1:0;
		
//			CHECK=1;
	if (!sec--) {
		CHECK=1;
		sec=0xFF;
		min--;

		if (!SLEEPEN) {
			min=MINUTE;
			ssleep=sleep;
			SLEEP=0;
		}
		#ifdef _IRON_SW
			if (!ISWITCH) {
				isleep=sleepi;
				ISLEEP=0;
			}
		#endif
		SHOW=1;
	}
	if (!min) {
		#ifdef _IRON_SW
			if (!ISLEEP && ISWITCH)
				if (isleep)
					isleep--;
//					else
//						isleep = IRON_SLEEP;
		#endif
		if (SLEEPEN && !SLEEP)
			ssleep--;
//				else
//					ssleep=sleep;
		min=MINUTE;
	}

#ifndef _ENCODER // Buttons
	if (!bt_delay) {
		if(INIT) { // Enter configuration menu?
		#ifdef _COMPRESSOR
			SSCONFIG=((BUTTONS | 0x80)!=0xFF || !HAON)?1:0; // Set CONFIG flag if any key pressed
		#else
			SSCONFIG=(BUTTONS!=0xFF)?1:0; // Set CONFIG flag if any key pressed
		#endif
			INIT=0; // Unset INIT flag
			bt_delay=20;
		} else		
		#ifdef _BUTTONS_14
			if(BUTTONS!=0xFF || (PWMPORT | 0x1F)!=0xFF || (PWMPPORT | 0xF8)!=0xFF)
		#else
			if(BUTTONS!=0xFF)
		#endif
		{
			bt_delay=(!KEYPRESSED)?10:1;
			KEYPRESSED=1;
			if (!SSCONFIG) { // Normal operation mode
				if (!IP) {
					menu_selected=0;
					PACT=1;
					INTF;
				}
				if (!IM) {
					menu_selected=0;
					MACT=1;
					INTF;
				}
				if (!HAP) {
					menu_selected=1;
					PACT=1;
					INTF;
				}
				if (!HAM) {
					menu_selected=1;
					MACT=1;
					INTF;
				}
				if (!FP) {
					menu_selected=2;
					PACT=1;
					INTF;
				}
				if (!FM) {
					menu_selected=2;
					MACT=1;
					INTF;
				}
				if (!ION) {
				#ifdef _BUTTONS_14
					#if !defined(_SINGLE) && !defined(_DUAL)
						if (MENU_2) {
							MENU_2=0;
							MENU=1;
							bt_delay=20;
						} else
					#endif
				#endif
					IACT=1;
				}
				if (!HAON) {
				#ifdef _BUTTONS_14
					#if !defined(_SINGLE) && !defined(_DUAL)
						if (MENU_2) {
							MENU_2=0;
							MENU=1;
							bt_delay=20;
						} else
					#endif
				#endif
					HAACT=1;
				}
				#ifdef _BUTTONS_14
					if (!PWMPP) {
						menu_selected=3;
						PACT=1;
						INTF;
					}
					if (!PWMPM) {
						menu_selected=3;
						MACT=1;
						INTF;
					}
					if (!PWMP) {
						menu_selected=4;
						PACT=1;
						INTF;
					}
					if (!PWMM) {
						menu_selected=4;
						MACT=1;
						INTF;
					}
					if (!PWMON) {
					#if !defined(_SINGLE) && !defined(_DUAL)
						if (!MENU_2) {
							MENU_2=1;
							MENU=1;
							bt_delay=20;
						} else
					#endif
							PWMACT=1;
					}
					if (!PWMPON) {
					#if !defined(_SINGLE) && !defined(_DUAL)
						if (!MENU_2) {
							MENU_2=1;
							MENU=1;
							bt_delay=20;
						} else
					#endif
							PWMPACT=1;
					}
				if (!ION || !HAON || !PWMON || !PWMPON)
				#else
				if (!ION || !HAON) 
				#endif
				{
					if (on_off++>5) {
						IACT=0;
						HAACT=0;
						#ifdef _BUTTONS_14
						PWMACT=0;
						PWMPACT=0;
						#endif
						KEYPRESSED=0;
						ONOFF=1;
						on_off=0;
						ONOFFACT=1;
						INTF;
						SHOW=1;
					}
					bt_delay=5;
				}
				#ifndef _BUTTONS_14
				if (IACT && HAACT) {
					IACT=0;
					HAACT=0;
					ONOFF=0;
					ONOFFACT=0;
					KEYPRESSED=0;
					on_off=0;
					on_off_delay=20;
					bt_delay=20;
					if (++prog_set>2) prog_set=0;
					PRESET=1;
					SHOW=1;
				}
					#ifdef _4CH
					if (MENU_2)
						switch(menu_selected) {
						case 0: menu_selected=3; break;
						case 1:
						case 2: menu_selected=4; break;
						}
					#endif
				#endif
			} else { // Configuration mode
				if (!IP || !HAP || !FP) { // Plus action
					PACT=1;
					INTF;						
				}
				if (!IM || !HAM || !FM) { // Minus action
					MACT=1;
					INTF;
				}
				if (!ION || !HAON) {
					if (on_off++>5) {
						ONOFF=1;
						on_off=0;
						on_off_delay=10;
						SSCONFIG=0;
						MENU=1;
						CSAVE=1;
					}
					bt_delay=10;
				}
			}
		} else {
			if (SSCONFIG) {
				if (on_off && !ONOFF)
					if (++menu_selected>CMAX-1) menu_selected=0;
				ONOFF=on_off=0;
			} else {
				if (!ONOFFACT) {
				#ifdef _BUTTONS_14
					if (IACT || HAACT || PWMACT || PWMPACT)
				#else
					if (IACT || HAACT)
				#endif
					{
						bt_delay=10;
						INTF;
					}
				} else {
					ONOFFACT=0;
					IACT=0;
					HAACT=0;
					#ifdef _BUTTONS_14
						PWMACT=0;
						PWMPACT=0;
					#endif
				}
				ONOFF=on_off=0;
			}
			KEYPRESSED=0;
		}
	}
#else // Encoder ON/OFF action + buttons plus/minus actions
	if (!bt_delay) {
		if(INIT) { // Enter configuration menu?
			SSCONFIG=((BUTTONS | 0x06)!=0xFF)?1:0; // Set CONFIG flag if any key pressed
			INIT=0; // Unset INIT flag
			bt_delay=20;
		} else		
		if((BUTTONS | 0x06)!=0xFF) {
		bt_delay=(!KEYPRESSED)?10:1;
		KEYPRESSED=1;
		if (!ENCBT) {
			if (on_off++>ENC_ONOFF) {
				if (!SSCONFIG) {
					switch (menu_selected) {
					case 0:
						IRON=!IRON; break;
					case 1:
					case 2:
						HOTAIR=!HOTAIR; break;
					#ifdef _4CH
					case 3:
						PWMPLUS=!PWMPLUS; break;
					case 4:
						PWM=!PWM; break;
					#endif
					}
					SAVECK=1;
				} else { // Save & exit from configuration menu
					on_off_delay=10;
					SSCONFIG=0;
					MENU=1;
					CSAVE=1;
					menu_selected=0;
				}
				ONOFF=1;
				on_off=0;
			}
			SHOW=1;
			bt_delay=10;
		}
		// Load preset
		if (!PROG0) {
			prog_set=0x00;
			PRESET=1;
			SHOW=1;
		}
		if (!PROG1) {
			prog_set=0x01;
			PRESET=1;
			SHOW=1;
		}
		if (!PROG2) {
			prog_set=0x02;
			PRESET=1;
			SHOW=1;
		}
		
		} else {
			if (on_off && !ONOFF) {
				if (SSCONFIG) { // Config is common for all
					if (++menu_selected>(CMAX-1)) menu_selected=0;
				} else {
				#ifdef _1CH // Одноканальная прошивка
					if (!(cfg&0x04)) { // Паяльник - Iron
						menu_selected=0;
					} else { // Фен - HotAir
						if (++menu_selected>MMAX) menu_selected=1;
					}
				#else
						if (++menu_selected>MMAX) menu_selected=0;
						#if !defined(_SINGLE) && !defined(_DUAL) && defined(_4CH)
							if (!menu_selected || menu_selected==3) {
								MENU=1;
								MENU_2=!MENU_2;
							}
						#endif
				#endif
				}
			}
			ONOFF=on_off=0;
			KEYPRESSED=0;
		}
   }
#endif
}

ISR (TIMER2_OVF_vect) { // timer2 overflow interrupt
	if (PWM)
		if (!pwm_off)
			if (!pwm_on) {
				pwm=0;
				pwm_on=pwm_set;
				pwm_off=0xFF;
				pwm_off^=pwm_set;
			} else {
				pwm=1;
				pwm_on--;
			}
		else
			pwm_off--;
	else
		pwm=0;

	if (!(cfg&0x04)) {
		if (PWMPLUS && !T12) {
			if (!pwmPlus_off) {
				if (!pwmPlus_on) {
					pwmPlus=0;
					pwmPlus_on=pwmPlus_set;
					pwmPlus_off=0xFF;
					pwmPlus_off^=pwmPlus_set;
				} else {
					pwmPlus=1;
					pwmPlus_on--;
				}
			}
			else
				pwmPlus_off--;
		}
		else
			pwmPlus=0;
	}
}