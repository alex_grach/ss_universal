/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* ---------------------- MOTOROLA SSD1828 LCD --------------------------- */
/* ------------------------------- C330 ---------------------------------- */
/* ----------------------------------------------------------------------- */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif

#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif

#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif

#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif

#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
unsigned char i,j;
Lcd_Write(CMD,0xAE); // disable display;
for(i=0;i<8;i++) {
	SetXY(0,i);
	for(j=0;j<96;j++) 
		Lcd_Write(DATA,logo_ss[(unsigned int)96*i+j]);
}
Lcd_Write(CMD,0xAF); // enable display;
return;
}                                                       
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
Lcd_Write(CMD,0xAE); // disable display;
for(i=0;i<8;i++) {
	SetXY(0,i);
	for(j=0;j<96;j++) {
		Lcd_Write(DATA,0x00);
	}
}
Lcd_Write(CMD,0xAF); // enable display;
return;
}

// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_us(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x20);

Lcd_Write(CMD,0xAE); // DisplayOFF
Lcd_Write(CMD,0xE2); // Software Reset
Lcd_Write(CMD,0xAB); // Start Internal Oscillator 
Lcd_Write(CMD,0x2F); // SetVoltControl 
Lcd_Write(CMD,0x65); // Set Boost Level 
Lcd_Write(CMD,0x25); // Set Internal Resistor Ratio 
Lcd_Write(CMD,0x39); // Set TC value 
Lcd_Write(CMD,0xA4); // Entire Display Select 
Lcd_Write(CMD,0xC8); // Set COM Scan Direction 
Lcd_Write(CMD,0xA1); // Set Segment Re-map 
Lcd_Write(CMD,0xAF); // DisplayON 
delay_us(0x80);
Lcd_Write(CMD,0x81); // Set Contrast Level 
Lcd_Write(CMD,EERead(eelcd)); // Set Contrast Level 

Lcd_Write(CMD,0xA6); // !invert display
Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
//Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
Lcd_Write(CMD,0x81); // Set Contrast Level 
Lcd_Write(CMD,contrast); // Set Contrast Level 
Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option
return;
}

#ifdef _SPI
	void Lcd_Write(unsigned char cd, volatile unsigned char c){
		wdt_reset();
		dc = cd;
		cs = 0;
		SSPBUF = c;
		while (!SSPIF) ;
		SSPIF = 0;
		if (cd==DATA) { // Dummy write
			SSPBUF = 0x00;
			while (!SSPIF) ;
			SSPIF = 0;
		}
		cs = 1;
	return;
	}
#else
	void Lcd_Write(unsigned char cd,unsigned char c){
	unsigned char li;
		wdt_reset();
		cs = 0;
		dc=cd;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(c & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			c <<= 1;
		}
		if (cd==DATA) { // Dummy write
		sclk=0;
		sda=0;
			for(li=0;li<8;li++) {
				sclk = 0;
				sclk = 1;
			}
		}
		sclk=0;
		sda=0;
		cs = 1;
	return;
	}
#endif

void SetXY(unsigned char x, unsigned char y){
	Lcd_Write(CMD,x & 0x0F);
	Lcd_Write(CMD,0x10 | ((x>>4)&0x07));
	Lcd_Write(CMD,0xB0 + y);
return;
}
