/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* --------------------- MCU independant config -------------------------- */
/* ----------------------------------------------------------------------- */

/* -------------------- Configuration and status bits -------------------- */

volatile typedef union {
	unsigned char SS;
	struct {
	  unsigned char PWM:1;
	  unsigned char SSCONFIG:1;
	  unsigned char KEYPRESSED:1;
	  unsigned char SAVE:1;
	  unsigned char CHECK:1;
	  unsigned char :1;
	  unsigned char HOTAIR:1;
	  unsigned char IRON:1;
	};
} __SS_bits_t;
extern __SS_bits_t SS_bits;

#define SAVE			SS_bits.SAVE
#define CHECK			SS_bits.CHECK
#define HOTAIR		SS_bits.HOTAIR
#define IRON			SS_bits.IRON
#define KEYPRESSED      SS_bits.KEYPRESSED
#define SSCONFIG		SS_bits.SSCONFIG
#define PWM			SS_bits.PWM
#define SS			SS_bits.SS

volatile typedef union {
	unsigned char CFG;
	struct {
	  unsigned char M_X:1;
	  unsigned char INV:1;
	  unsigned char I_HA:1;
	  unsigned char M_Y:1;
	  unsigned char CH_4:1;
	  unsigned char HIDE:1;
	  unsigned char IPWM:1;
	  unsigned char FPWM:1;
	};
} __SS_config_bits_t;
extern __SS_config_bits_t SS_config_bits;

#define M_X			SS_config_bits.M_X
#define M_Y			SS_config_bits.M_Y
#define INV			SS_config_bits.INV
#define I_HA		SS_config_bits.I_HA
#define CH_4		SS_config_bits.CH_4
#define HIDE		SS_config_bits.HIDE
#define IPWM		SS_config_bits.IPWM
#define FPWM		SS_config_bits.FPWM
#define CFG			SS_config_bits.CFG

volatile typedef union {
	unsigned char SSTATUS;
	struct {
	  unsigned char PWMPLUS:1;
	  unsigned char MENU_2:1;
	  unsigned char ENC:1;
	  unsigned char ONOFF:1;
	  unsigned char INIT:1; // First run bit
	  unsigned char CSAVE:1; // Save config bit
	  unsigned char MENU:1; // Redraw menu bit
	  unsigned char LCDINIT:1; // LCD reinit bit
	};
} __SS_status_bits_t;
extern __SS_status_bits_t SS_status_bits;

#define PWMPLUS		SS_status_bits.PWMPLUS
#define MENU_2		SS_status_bits.MENU_2
#define ENC			SS_status_bits.ENC
#define ONOFF		SS_status_bits.ONOFF
#define INIT		SS_status_bits.INIT
#define CSAVE		SS_status_bits.CSAVE
#define MENU		SS_status_bits.MENU
#define LCDINIT		SS_status_bits.LCDINIT
#define SSTATUS		SS_status_bits.SSTATUS

volatile typedef union {
	unsigned char ACTION;
	struct {
	  unsigned char ACT:1;
	  unsigned char ONOFFACT:1; // On/OFF action
	  unsigned char PWMPACT:1;
	  unsigned char PWMACT:1;
	  unsigned char HAACT:1; // HotAir on action
	  unsigned char IACT:1; // Iron on action
	  unsigned char MACT:1; // Minus action
	  unsigned char PACT:1; // Plus action
	};
} __SS_action_bits_t;
extern __SS_action_bits_t SS_action_bits;

#define ACT		SS_action_bits.ACT
#define ONOFFACT		SS_action_bits.ONOFFACT
#define PWMPACT		SS_action_bits.PWMPACT
#define PWMACT		SS_action_bits.PWMACT
#define HAACT		SS_action_bits.HAACT
#define IACT		SS_action_bits.IACT
#define MACT		SS_action_bits.MACT
#define PACT		SS_action_bits.PACT
#define ACTION		SS_action_bits.ACTION

volatile typedef union {
	unsigned char SLEEP_STATUS;
	struct {
	  unsigned char :1;
	  unsigned char :1;
	  unsigned char ISLEEP:1; // Iron sleep status
	  unsigned char SLEEPEN:1; // Enable sleep counter
	  unsigned char NOSLEEP:1; // No sleep
	  unsigned char SLEEP:1; // Soldering station sleep status
	  unsigned char ISWITCH:1; // Iron switch active
	  unsigned char HASWITCH:1; // HotAir switch active
	};
} __SS_sleep_status_bits_t;
extern __SS_sleep_status_bits_t SS_sleep_status_bits;

#define SLEEP_STATUS	SS_sleep_status_bits.SLEEP_STATUS
#define ISLEEP		SS_sleep_status_bits.ISLEEP
#define SLEEPEN		SS_sleep_status_bits.SLEEPEN
#define NOSLEEP		SS_sleep_status_bits.NOSLEEP
#define SLEEP	SS_sleep_status_bits.SLEEP
#define ISWITCH		SS_sleep_status_bits.ISWITCH
#define HASWITCH		SS_sleep_status_bits.HASWITCH

volatile typedef union {
	unsigned char ACTION_STATUS;
	struct {
	  unsigned char SHOW:1; // Show all
	  unsigned char SHOWSET:1; // Show set values
	  unsigned char DUAL:1; // Second lcd select bit
	  unsigned char PRESET:1; // Load preset flag
	  unsigned char SAVECK:1; // Save check flag
	  unsigned char ZERO:1; // Zero-cross detected
	  unsigned char T12_2:1; // PWMPlus T12 handle
	  unsigned char T12:1; // Iron T12 handle
	};
} __SS_as_bits_t;
extern __SS_as_bits_t SS_as_bits;

#define SHOW		SS_as_bits.SHOW
#define SHOWSET		SS_as_bits.SHOWSET
#define DUAL		SS_as_bits.DUAL
#define PRESET		SS_as_bits.PRESET
#define SAVECK		SS_as_bits.SAVECK
#define ZERO		SS_as_bits.ZERO
#define T12_2		SS_as_bits.T12_2
#define T12		SS_as_bits.T12
#define ACTION_STATUS		SS_as_bits.ACTION_STATUS

/* ----------------------------------------------------------------------- */
/* ------------------------ LCD type configuration ----------------------- */

#if defined(_SSD1783) || defined(_LS020) || defined(_LPH9157) || defined(_L2F50) || defined(_SSD1289)
	#define _COLOR
#endif

#if defined(_SPFD54124)
	#define _COLOR_16
#endif

/* ----------------------------------------------------------------------- */
/* --------------------- LCD specific configuration ---------------------- */

#if defined(_LS020) 
	#define CMD	1
	#define DATA	0
#else
	#define CMD	0
	#define DATA	1
#endif

/* ----------------------------------------------------------------------- */
/* ----------------- Font & config address definitions ------------------- */

#define _24CADDR	0xA0
		
#define eepr0	0x3C
#define eepr1	eepr0+0x08
#define eepr2	eepr1+0x08
#define eeprog	eepr2+0x08
#define eetoff	eeprog+0x01
#define eesleep	eetoff+0x01
#define eelcd	eesleep+0x01
#define eeconfig	eelcd+0x01
#define eeisleep	eeconfig+0x01

// Digits
	#define ee_0 	0x0 
	#define ee_1 	0x1 
	#define ee_2 	0x2 
	#define ee_3 	0x3 
	#define ee_4 	0x4 
	#define ee_5 	0x5 
	#define ee_6 	0x6 
	#define ee_7 	0x7 
	#define ee_8 	0x8 
	#define ee_9 	0x9 

	#define ee_ls 	0x12 
	#define ee_gt 	ee_ls+1 
	#define ee_ps 	ee_gt+1 
	#define ee_sp 	ee_ps+1 
	#define ee_dash 	ee_sp+1 
	#define ee_dot 	ee_dash+1 
	#define ee_plus 	ee_dot+1 
// RUS
#ifdef _RU
	#define ee_a 	ee_plus+1 
	#define ee_b 	ee_a+1 
	#define ee_v 	ee_b+1 
	#define ee_g 	ee_v+1 
	#define ee_d 	ee_g+1 
	#define ee_e 	ee_d+1 
	#define ee_i 	ee_e+1 
	#define ee_k 	ee_i+1 
	#define ee_l 	ee_k+1 
	#define ee_m 	ee_l+1 
	#define ee_n 	ee_m+1 
	#define ee_o 	ee_n+1 
	#define ee_p 	ee_o+1 
	#define ee_r 	ee_p+1 
	#define ee_s 	ee_r+1 
	#define ee_t 	ee_s+1 
	#define ee_u 	ee_t+1 
	#define ee_f 	ee_u+1 
	#define ee_h 	ee_f+1 
	#define ee_sh 	ee_h+1 
	#define ee_iu 	ee_sh+1 
	#define ee_mz 	ee_iu+1 
	#define ee_ya 	ee_mz+1
// ENG
#else
	#define ee_a 	ee_plus+1 
	#define ee_c 	ee_a+1 
	#define ee_d 	ee_c+1 
	#define ee_e 	ee_d+1 
	#define ee_f 	ee_e+1 
	#define ee_g 	ee_f+1 
	#define ee_h 	ee_g+1 
	#define ee_i 	ee_h+1 
	#define ee_l 	ee_i+1 
	#define ee_m 	ee_l+1 
	#define ee_n 	ee_m+1 
	#define ee_o 	ee_n+1 
	#define ee_p 	ee_o+1 
	#define ee_r 	ee_p+1 
	#define ee_s 	ee_r+1 
	#define ee_t 	ee_s+1 
	#define ee_v 	ee_t+1 
	#define ee_w 	ee_v+1 
	#define ee_x 	ee_w+1 
	#define ee_y 	ee_x+1 
#endif

#ifdef	_ST7920

	#define lcd_sdig 0x30
	#define lcd_dig_h	0xA3
	#define lcd_dig 0xB0
	#define lcd_0 	lcd_dig 
	#define lcd_1 	lcd_dig+0x1 
	#define lcd_2 	lcd_dig+0x2 
	#define lcd_3 	lcd_dig+0x3 
	#define lcd_4 	lcd_dig+0x4 
	#define lcd_5 	lcd_dig+0x5 
	#define lcd_6 	lcd_dig+0x6 
	#define lcd_7 	lcd_dig+0x7 
	#define lcd_8 	lcd_dig+0x8 
	#define lcd_9 	lcd_dig+0x9 

	#define lcd_ls 	0x11
	#define lcd_gt 	0x10
	#define lcd_ps 	0x25
	#define lcd_sp 	0xA0
	#define lcd_dash 	0xAD
	#define lcd_dot 	0x2E
	#define lcd_plus 	0x2B
	
	#define lcd_a 	0x41
	#define lcd_c 	0x43
	#define lcd_d 	0x44
	#define lcd_e 	0x45
	#define lcd_f 	0x46
	#define lcd_g 	0x47
	#define lcd_h 	0x48
	#define lcd_i 	0x49
	#define lcd_l 	0x4C
	#define lcd_m 	0x4D
	#define lcd_n 	0x4E
	#define lcd_o 	0x4F
	#define lcd_p 	0x50 
	#define lcd_r 	0x52 
	#define lcd_s 	0x53 
	#define lcd_t 	0x54 
	#define lcd_v 	0x56 
	#define lcd_w 	0x57 
	#define lcd_x 	0x58 
	#define lcd_y 	0x59 
#endif

/* ----------------------------------------------------------------------- */
/* ----------------------- Temperature limits ---------------------------- */

#define IronMax 800
#define IronMin 0
#define HotAirMax 1000
#define HotAirMin 0
#define FanMax 100
#define FanMin 0
#define ENC_ONOFF	0x01

/* ----------------------------------------------------------------------- */
/* ------------------------ Maximun PWM limit ---------------------------- */

#define MAXPWM	0xF0

/* ----------------------------------------------------------------------- */

extern unsigned char cfg;
extern unsigned char sleep;
