@echo off
SET "OPTS= -S -mpic14 --stack-size 16 -c --use-non-free --no-extended-instructions --Werror -DCPU -D_MY -D_LOGO"
SET "INC= -I.\include -I.\include\pic14 -I.\non-free -I.\non-free\include -I.\non-free\include\pic14 "
cls
echo Deleting old build files
del *.o *.cod *.cof *.map *.lst *.asm *.hex
echo Starting firmware build...
echo.
echo Select you MCU
echo.
echo 1 - pic16f876
echo 2 - pic16f876a
echo 3 - pic16f877
echo 4 - pic16f877a
echo 5 - pic16f886
echo 6 - pic16f887
echo.
SET /P M=Type 1,2,3,4,5 or 6 then press Enter: 
IF %M%==1 ( 
	SET MCU=16f876
	SET PIC=16F876
)
IF %M%==2 (
	SET MCU=16f876a
	SET PIC=16F876A
)
IF %M%==3 ( 
	SET MCU=16f877
	SET PIC=16F877
)
IF %M%==4 (
	SET MCU=16f877a
	SET PIC=16F877A
)
IF %M%==5 ( 
	SET MCU=16f886
	SET PIC=16F886
)
IF %M%==6 (
	SET MCU=16f887
	SET PIC=16F887
)
echo %MCU%
echo.
echo Select language
echo.
echo 1 - Russian
echo 2 - English
echo.
SET /P M=Type 1 or 2 then press Enter: 
IF %M%==1 SET LANG=_RU
IF %M%==2 SET LANG=_EN
echo %LANG%
echo.
echo Set number of channels
echo.
SET /P CH=Type 1, 2 or 4 then press Enter: 
echo %CH%
echo.
echo Select LCD type
echo.
echo 1 - PCF8814 (Nokia 1100/1110i/1202...)
echo 2 - PCD8544 (Nokia 3310/5110...)
echo 3 - SPFD54124 (Nokia 1616/c1-01...)
echo 4 - SSD1783 (Motorola V171...)
echo 5 - SSD1828 (Motorola C330...)
echo 6 - LS020 (Siemens S65...)
echo 7 - L2F50 (Siemens S65...)
SET /P M=Type 1, 2, 3, 4, 5, 6 or 7 then press Enter: 
IF %M%==1 (
	SET LCD=PCF8814
	SET LCDF=pcf8814
)
IF %M%==2 (
	SET LCD=PCD8544
	SET LCDF=pcd8544
)
IF %M%==3 (
	SET LCD=SPFD54124 -D_LARGE -D_SPI
	SET LCDF=spfd54124
)
IF %M%==4 (
	SET LCD=SSD1783
	SET LCDF=ssd1783
)
IF %M%==5 (
	SET LCD=SSD1828 -D_SPI
	SET LCDF=ssd1828
)
IF %M%==6 (
	SET LCD=LS020 -D_LARGE -D_SPI
	SET LCDF=ls020
)
IF %M%==7 (
	SET LCD=L2F50 -D_LARGE -D_SPI
	SET LCDF=l2f50
)
echo %LCD%
echo.
echo Select control type
echo 1 - 8 buttons
echo 2 - 14 buttons
echo 3 - encoder
SET /P M=Type 1, 2 or 3 then press Enter: 
IF %M%==1 SET CTRL=_BUTTONS
IF %M%==2 SET CTRL=_BUTTONS14
IF %M%==3 SET CTRL=_ENCODER
echo %CTRL%
echo.
echo Use IronSW ?
echo 1 - No
echo 2 - Yes
SET /P M=Type 1, 2 then press Enter: 
IF %M%==1 SET ISW=_NORM
IF %M%==2 SET ISW=_IRON_SW
echo %ISW%
SET "o= -D%LANG% -D_3V -D_SHOW -D_%LCD% -D_%CH%CH -D%CTRL% -D%ISW%"
echo "Starting build..."
echo on
	.\bin\sdcc %OPTS% -p%MCU% -DPIC%PIC% %o% -o ss.asm ss.c
	.\bin\gpasm -I header -a inhx8m -c ss.asm
	.\bin\sdcc %OPTS% -p%MCU% -DPIC%PIC% %o% -o eeprom.asm eeprom.c
	.\bin\gpasm -I header -a inhx8m -c eeprom.asm
	.\bin\sdcc %OPTS% -p%MCU% -DPIC%PIC% %o% -o lcd.asm %LCDF%.c
	.\bin\gpasm -I header -a inhx8m -c lcd.asm
	.\bin\sdcc %OPTS% -p%MCU% -DPIC%PIC% %o% -o pid.asm pid.c
	.\bin\gpasm -I header -a inhx8m -c pid.asm
	.\bin\gplink -c -m -w -r -I.\lib\pic14 -I.\non-free\lib\pic14 -s .\lkr\%MCU%_g.lkr -o ss_%MCU%.hex ss.o lcd.o eeprom.o pid.o libsdcc.lib pic%MCU%.lib
echo Deleting build files
del *.o *.cod *.cof *.map *.lst *.asm
pause