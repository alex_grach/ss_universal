/*
	SIEMENS S65 LCD
	LS020
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#define	_DX	0x08
#define	_DY	0x08

// INIT1:  ; 24bytes
const unsigned char init0[24] = { 0xFD, 0xFD, 0xFD, 0xFD, 0xEF, 0x00, 0xEE, 0x04, 0x1B, 0x04, 0xFE, 0xFE, 0xFE, 0xFE, 0xEF, 0x90, 0x4A, 0x04,
0x7F, 0x3F, 0xEE, 0x04, 0x43, 0x06 }; 

// INIT2:  ; 40bytes
const unsigned char init1[40] = { 0xEF, 0x90,  0x09, 0x83,  0x08, 0x00,  0x0B, 0xAF,  0x0A, 0x00,  0x05, 0x00,  0x06, 0x00,
0x07, 0x00,  0xEF, 0x00,  0xEE, 0x0C,  0xEF, 0x90,  0x00, 0x80,  0xEF, 0xB0,  0x49, 0x02,
0xEF, 0x00,  0x7F, 0x01,  0xE1, 0x81,  0xE2, 0x02,  0xE2, 0x76,  0xE1, 0x83 };

// INIT3:  ; 6bytes
const unsigned char init2[6] = { 0x80, 0x01, 0xEF, 0x90, 0x00, 0x00 };

#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
unsigned char i,j,k,ch;
	SetXY(0-_DX,0-_DY,63,95);

	for (i=0; i<8; i++)
		for (j=0; j<96; j++) {
			ch = logo_ss[(unsigned int)96*i+j];
			for (k=0; k<8; k++) {
				if (ch & 0x80)
					Lcd_S();
				else
					Lcd_H();
				ch <<= 1;
			}
		}
return;
}
#endif

//clear LCD
void Lcd_Clear(void) {
unsigned char i,j;
#ifdef	_SINGLE
	SetXY(0-_DX,0-_DY,131,175);
#else
	SetXY(0-_DX,0-_DY,175,131);
#endif
cs = 0;
dc = DATA;
for (i=0;i<176;i++)
	for (j=0;j<132;j++) {
	SSPBUF = 0xFF;
	}
SSPIF = 0;
cs = 1;
return;
}

// init LCD
void Lcd_Init(void) {
unsigned char i;
//sclk = 0;
//sda = 0;
cs = 0;
delay_s(0x01);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 0;
delay_s(0x01);
rst = 1;
delay_s(0x01);			// 5mS so says the stop watch(less than 5ms will not work)
for (i=0;i<sizeof(init0);i++) {
	Lcd_Write(CMD, init0[i]);
}
delay_s(0x01);
for (i=0;i<sizeof(init1);i++) {
	Lcd_Write(CMD, init1[i]);
}
delay_s(0x01);
for (i=0;i<sizeof(init2);i++) {
	Lcd_Write(CMD, init2[i]);
}
delay_s(0x01);

// Negative mode
Lcd_Reinit(0);
Lcd_Write(CMD,0xEF);
Lcd_Write(CMD,0x90);
// ----Set orientation
#ifdef	_SINGLE
	// Vertical
	Lcd_Write(CMD,0x01);
	Lcd_Write(CMD,0x00);
	Lcd_Write(CMD,0x05);
	Lcd_Write(CMD,0x04);
#else
	// Horisontal
	Lcd_Write(CMD,0x01);
	Lcd_Write(CMD,0x40);
	Lcd_Write(CMD,0x05);
	Lcd_Write(CMD,0x00);
#endif
// ----Set bit mode to 8 bit---RRGGGGBB
Lcd_Write(CMD,0xE8);
Lcd_Write(CMD,0x00);
cs=1;
//Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
contrast=0;
Lcd_Write(CMD,0xEF);
Lcd_Write(CMD,0xB0);
Lcd_Write(CMD,0x49);
Lcd_Write(CMD,0x02^((cfg&0x02)>>1)); // Negative mode
Lcd_Write(CMD,0xEF);
Lcd_Write(CMD,0x90);
Lcd_Write(CMD,0x00);
Lcd_Write(CMD,0x00);
//Lcd_Write(CMD,0x81); /* CMD   Contrast Lvl & Int. Regul. Resistor Ratio */
//Lcd_Write(DATA,contrast); /* DATA: contrast = 0x29 */
//Lcd_Write(DATA,0x05); /* DATA: 0x05 = 0b101 -> 1+R2/R1 = 11.37 */
return;
}

#ifdef _SPI
	void Lcd_Write(unsigned char cd, volatile unsigned char c){
		__asm__ ("CLRWDT");
		dc = cd;
		cs = 0;
		SSPBUF = c;
		while (!SSPIF) ;
		SSPIF = 0;
		cs = 1;
	return;
	}
#else
	// Buggy
	void Lcd_Write(unsigned char cd, unsigned char c){
	unsigned char li;
		__asm__ ("CLRWDT");
		dc = cd;
		cs = 0;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(c & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			c <<= 1;
		}
		sclk=0;
		sda=0;
		cs = 1;
	return;
	}
#endif

void Lcd_S(void) {
	Lcd_Write(DATA, 0x00);
return;
}

void Lcd_H(void) {
	Lcd_Write(DATA, 0xFF);
return;
}

void Lcd_B(void) {
	Lcd_Write(DATA, 0x02);
return;
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) { // Set buffer
__asm__ ("CLRWDT");
cs=0;
	x+=_DX;
	y+=_DY;
/*	Lcd_Write(CMD,0xEF);
	Lcd_Write(CMD,0x90);

	Lcd_Write(CMD,0x01);
	Lcd_Write(CMD,0x40);
	Lcd_Write(CMD,0x05);
	Lcd_Write(CMD,0x00);
*/	
#ifdef	_SINGLE
	Lcd_Write(CMD,0xEF);
	Lcd_Write(CMD,0x90);
	Lcd_Write(CMD,0x08); // x-address
	Lcd_Write(CMD,x);
	Lcd_Write(CMD,0x09);
	Lcd_Write(CMD,x+dx);

	Lcd_Write(CMD,0x0A); // y-address
	Lcd_Write(CMD,y);
	Lcd_Write(CMD,0x0B);
	Lcd_Write(CMD,y+dy);

	Lcd_Write(CMD,0x06);
	Lcd_Write(CMD,x);
	Lcd_Write(CMD,0x07);
	Lcd_Write(CMD,y);
#else
	Lcd_Write(CMD,0xEF);
	Lcd_Write(CMD,0x90);
	Lcd_Write(CMD,0x08); // x-address
	Lcd_Write(CMD,y);
	Lcd_Write(CMD,0x09);
	Lcd_Write(CMD,y+dy);

	Lcd_Write(CMD,0x0A); // y-address
	Lcd_Write(CMD,175-x);
	Lcd_Write(CMD,0x0B);
	Lcd_Write(CMD,175-x-dx);

	Lcd_Write(CMD,0x06);
	Lcd_Write(CMD,y);
	Lcd_Write(CMD,0x07);
	Lcd_Write(CMD,175-x);
#endif

return;
}