#ifndef _4K
void debug(unsigned char count);
#endif

#ifndef	_AVR
void delay_s(unsigned char count);
void delay_us(unsigned char count);
void delay_ms(unsigned char count);
#endif