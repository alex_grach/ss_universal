/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */

#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif

#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif

#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif

#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

extern unsigned char menu_selected, set, contrast, fan_off, ssleep, sleepi;
extern unsigned int itempav, i2tempav, hatempav;
#ifdef	_24C
	extern const unsigned char __EEPROM[];
#endif

unsigned char ii;

#ifdef _LARGE
const unsigned char BigNumbers[]  =
{
0x00, 0x00, 0x00, 0xFC, 0xEF, 0x7F, 0xFA, 0xC7, 0xBF, 0xF6, 0x83, 0xDF, 0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xE0, 0xF6, 0x83, 0xDF, 0xFA, 0xC7, 0xBF, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 0
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x83, 0x1F, 0xF8, 0xC7, 0x3F, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 1 
0x00, 0x00, 0x00, 0x00, 0xE0, 0x7F, 0x02, 0xD0, 0xBF, 0x06, 0xB8, 0xDF, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0xF6, 0x3B, 0xC0, 0xFA, 0x17, 0x80, 0xFC, 0x0F, 0x00, 0x00, 0x00, 0x00,// 2
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x10, 0x80, 0x06, 0x38, 0xC0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0xF6, 0xBB, 0xDF, 0xFA, 0xD7, 0xBF, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 3
0x00, 0x00, 0x00, 0xFC, 0x0F, 0x00, 0xF8, 0x17, 0x00, 0xF0, 0x3B, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0xF0, 0xBB, 0x1F, 0xF8, 0xD7, 0x3F, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 4
0x00, 0x00, 0x00, 0xFC, 0x0F, 0x00, 0xFA, 0x17, 0x80, 0xF6, 0x3B, 0xC0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x06, 0xB8, 0xDF, 0x02, 0xD0, 0xBF, 0x00, 0xE0, 0x7F, 0x00, 0x00, 0x00,// 5
0x00, 0x00, 0x00, 0xFC, 0xEF, 0x7F, 0xFA, 0xD7, 0xBF, 0xF6, 0xBB, 0xDF, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x06, 0xB8, 0xDF, 0x02, 0xD0, 0xBF, 0x00, 0xE0, 0x7F, 0x00, 0x00, 0x00,// 6
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x06, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x0E, 0x00, 0x00, 0xF6, 0x83, 0x1F, 0xFA, 0xC7, 0x3F, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 7
0x00, 0x00, 0x00, 0xFC, 0xEF, 0x7F, 0xFA, 0xD7, 0xBF, 0xF6, 0xBB, 0xDF, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0xF6, 0xBB, 0xDF, 0xFA, 0xD7, 0xBF, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 8
0x00, 0x00, 0x00, 0xFC, 0x0F, 0x00, 0xFA, 0x17, 0x80, 0xF6, 0x3B, 0xC0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0x0E, 0x38, 0xE0, 0xF6, 0xBB, 0xDF, 0xFA, 0xD7, 0xBF, 0xFC, 0xEF, 0x7F, 0x00, 0x00, 0x00,// 9
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// space
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x38, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,// -
};
#endif

unsigned char conv[16] = {0x00, 0x03, 0x0C, 0x0F, 0x30, 0x33, 0x3C, 0x3F, 0xC0, 0xC3, 0xCC, 0xCF, 0xF0, 0xF3, 0xFC, 0xFF}; // 10x14

#ifdef _PCD8544	// Nokia 3310
	#ifdef _1CH
		unsigned char menu_position[3]={1,2,5}; // Координата Y пункта меню
	#else
		unsigned char menu_position[5]={1,3,5,1,3}; // Координата Y пункта меню
	#endif
#else
	#ifdef	_SINGLE
		#ifdef	_LARGE
			unsigned char menu_position[5]={1*HEIGHT-1,5*HEIGHT+1,9*HEIGHT+3,11*HEIGHT+3,15*HEIGHT+5}; // Координата Y пункта меню
		#else
			unsigned char menu_position[5]={1,3,3,5,7}; // Координата Y пункта меню
		#endif
	#else
		#ifdef	_LARGE
			unsigned char menu_position[5]={2*HEIGHT-1,7*HEIGHT+1,12*HEIGHT+3,2*HEIGHT,7*HEIGHT}; // Координата Y пункта меню
		#else
			unsigned char menu_position[5]={1,4,7,1,4}; // Координата Y пункта меню
		#endif
	#endif
#endif

#ifdef	_ST7920
const unsigned char menu_0[5] = {lcd_gt, lcd_s, lcd_e, lcd_t, lcd_ls};
const unsigned char menu_1[5] = {lcd_gt, lcd_g, lcd_e, lcd_t, lcd_ls};
const unsigned char menu_2[4] = {lcd_i, lcd_r, lcd_o, lcd_n};
const unsigned char menu_3[6] = {lcd_h, lcd_o, lcd_t, lcd_a, lcd_i, lcd_r};
const unsigned char menu_4[5] = {lcd_s, lcd_p, lcd_e, lcd_e, lcd_d};
const unsigned char menu_5[1] = {lcd_ps};
const unsigned char menu_6[3] = {lcd_p, lcd_w, lcd_m};
const unsigned char menu_7[6] = {lcd_h, lcd_e, lcd_a, lcd_t, lcd_e, lcd_r};
const unsigned char menu_8[5] = {lcd_s, lcd_l, lcd_e, lcd_e, lcd_p};
// Configurtion menu
const unsigned char cmenu_0[8] = {lcd_c, lcd_o, lcd_n, lcd_t, lcd_r, lcd_a, lcd_s, lcd_t};
const unsigned char cmenu_1[2] = {lcd_m, lcd_x};
const unsigned char cmenu_2[2] = {lcd_m, lcd_y};
const unsigned char cmenu_3[6] = {lcd_i, lcd_n, lcd_v, lcd_e, lcd_r, lcd_t};
//sconst unsigned char cmenu_4[4] = {lcd_h, lcd_i, lcd_d, lcd_e};
const unsigned char cmenu_4[9] = {ee_t, ee_1, ee_2, ee_sp, ee_s, ee_l, ee_e, ee_e, ee_p};
const unsigned char cmenu_5[6] = {lcd_s, lcd_l, lcd_e, lcd_e, lcd_p, lcd_sp};
const unsigned char cmenu_6[6] = {lcd_t, lcd_sp, lcd_o, lcd_f, lcd_f, lcd_sp};
#elif _RU
const unsigned char menu_0[5] = {ee_gt, ee_u, ee_s, ee_t, ee_ls};
const unsigned char menu_1[5] = {ee_gt, ee_t, ee_e, ee_k, ee_ls};
const unsigned char menu_2[8] = {ee_p, ee_a, ee_ya, ee_l, ee_mz, ee_n, ee_i, ee_k};
const unsigned char menu_3[3] = {ee_f, ee_e, ee_n};
const unsigned char menu_4[7] = {ee_o, ee_b, ee_o, ee_r, ee_o, ee_t, ee_iu};
const unsigned char menu_5[1] = {ee_ps};
const unsigned char menu_6[3] = {ee_sh, ee_i, ee_m};
const unsigned char menu_7[8] = {ee_p, ee_o, ee_d, ee_o, ee_g, ee_r, ee_e, ee_v};
const unsigned char menu_8[3] = {ee_s, ee_o, ee_n};
// Configurtion menu
const unsigned char cmenu_0[8] = {ee_k, ee_o, ee_n, ee_t, ee_r, ee_a, ee_s, ee_t};
const unsigned char cmenu_1[2] = {ee_m, ee_h};
const unsigned char cmenu_2[2] = {ee_m, ee_u};
const unsigned char cmenu_3[8] = {ee_i, ee_n, ee_v, ee_e, ee_r, ee_s, ee_i, ee_ya};
const unsigned char cmenu_4[7] = {ee_t, ee_1, ee_2, ee_sp, ee_s, ee_o, ee_n};
//const unsigned char cmenu_4[7] = {ee_p, ee_r, ee_ya, ee_t, ee_a, ee_t, ee_mz};
const unsigned char cmenu_5[3] = {ee_s, ee_o, ee_n};
const unsigned char cmenu_6[6] = {ee_t, ee_sp, ee_o, ee_t, ee_k, ee_l};
#else
const unsigned char menu_0[5] = {ee_gt, ee_s, ee_e, ee_t, ee_ls};
const unsigned char menu_1[5] = {ee_gt, ee_g, ee_e, ee_t, ee_ls};
const unsigned char menu_2[4] = {ee_i, ee_r, ee_o, ee_n};
const unsigned char menu_3[6] = {ee_h, ee_o, ee_t, ee_a, ee_i, ee_r};
const unsigned char menu_4[5] = {ee_s, ee_p, ee_e, ee_e, ee_d};
const unsigned char menu_5[1] = {ee_ps};
const unsigned char menu_6[3] = {ee_p, ee_w, ee_m};
const unsigned char menu_7[6] = {ee_h, ee_e, ee_a, ee_t, ee_e, ee_r};
const unsigned char menu_8[5] = {ee_s, ee_l, ee_e, ee_e, ee_p};
// Configurtion menu
const unsigned char cmenu_0[8] = {ee_c, ee_o, ee_n, ee_t, ee_r, ee_a, ee_s, ee_t};
const unsigned char cmenu_1[2] = {ee_m, ee_x};
const unsigned char cmenu_2[2] = {ee_m, ee_y};
const unsigned char cmenu_3[6] = {ee_i, ee_n, ee_v, ee_e, ee_r, ee_t};
//const unsigned char cmenu_4[4] = {ee_h, ee_i, ee_d, ee_e};
const unsigned char cmenu_4[9] = {ee_t, ee_1, ee_2, ee_sp, ee_s, ee_l, ee_e, ee_e, ee_p};
const unsigned char cmenu_5[5] = {ee_s, ee_l, ee_e, ee_e, ee_p};
const unsigned char cmenu_6[5] = {ee_t, ee_sp, ee_o, ee_f, ee_f};
//const unsigned char cmenu_7[6] = {ee_h, ee_o, ee_t, ee_a, ee_i, ee_r};
#endif

#ifdef _1CH
const unsigned char iron_pic[48] = {
	0x02, 0x01, 0x0D, 0x12, 0x20, 0x20, 0x10, 0x08, 
	0x1C, 0x22, 0x22, 0x22, 0x22, 0x22, 
	0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 
	0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x7F,
	0x41, 0x7F, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 
	0x1C, 0x14, 0x1C, 0x08, 0x08, 0x08, 0x08, 0x08
};

const unsigned char hotair_pic[48] = {
	0x02, 0x01, 0x0D, 0x12, 0x20, 0x20, 0x10, 0x08,
	0x1C, 0x14, 0x36, 0x63, 0x41, 0x41,
	0x63, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 
	0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x3E, 0x22, 
	0x3E, 0x22, 0x3E, 0x14, 0x14, 0x14, 0x14, 0x14,
	0x1C, 0x00, 0x00, 0x14, 0x00, 0x2A, 0x00, 0x55
};
#endif

#ifdef	_I2C
void I2CStart(void) {
	sda=1;
	sclk=1;
	__asm__ ("NOP");
	sda=0;
	__asm__ ("NOP");
	sclk=0;
	__asm__ ("NOP");
	return;
}

void I2CStop(void) {
	sda=0;
	sclk=0;
	__asm__ ("NOP");
	sclk=1;
	__asm__ ("NOP");
	sda=1;
	__asm__ ("NOP");
	return;
}


void I2CWrite(unsigned char c) {
	for (ii=0; ii<8; ii++) {
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;		
	}
	sclk=0;
	sdaT=1;
	sclk=1;
	sclk=0;
	sdaT=0;
	sda=0;
	return;
}

unsigned char I2CRead(void) {
unsigned char data=0;
	sclk=0;
	sdaT=1;
	for (ii=0; ii<8; ii++) {
		sclk=1;
			data <<= 1;
			if (sda)
				data ^= 0x01;
		sclk=0;
	}
	sdaT=0;
	sda=1;
	sclk=1;
	sclk=0;
	sda=0;
	return data;
}
#endif

#ifdef	_AVR
unsigned char EERead(unsigned char adr) {
	return eeprom_read_byte((uint8_t) adr);
}

void EEWrite(volatile unsigned char * val, unsigned char adr) {
	eeprom_update_byte(adr, *val);
	eeprom_busy_wait();
	return;
}
#else
	#ifdef	_24C
	unsigned char EERead(unsigned char adr) {
	unsigned char sym = __EEPROM[adr];
		return sym;
	}
	
	void EEWrite(unsigned char * val, unsigned char adr) {
		adr = val[0];
		return;
	}
	#else
	unsigned char EERead(unsigned char adr) {
	
		EEADR=adr;
		RD=1;
		return EEDATA;
	}
	
	void EEWrite(unsigned char * val, unsigned char adr) {
		EEADR=adr;
		EEDATA=*val;
		EEPGD=0;
		EEIF=0;
		WREN=1;
		GIE=0;
		while(GIE) ;
		EECON2=0x55;
		EECON2=0xAA;
		WR=1;
		while (!EEIF);
		EEIF=0;
		GIE=1;
		WREN=0;
		return;
	}
	#endif
#endif

void showPreset(unsigned char addr) {
#ifdef	_ST7920
	Lcd_Write(CMD, 0x80 | 0x05);
	Lcd_Write(DATA, 0x20);
	Lcd_Write(DATA, lcd_gt);
	Lcd_Write(DATA, lcd_sdig+addr);
	Lcd_Write(DATA, lcd_ls);
#else
	#ifdef	_1CH
		#define	_YPOS	2
	#else
		#define	_YPOS	0
	#endif
	//unsigned char i, sym;
	#if defined(_COLOR) || defined(_COLOR_16)
		#ifdef _LARGE
			#ifdef	_SINGLE
				EE_Char_Small(addr, 12*WIDTH+4, _YPOS);
			#else
				EE_Char_Small(addr, 7*MID_WIDTH+6, _YPOS);
			#endif
		#else
			EE_Char_Small(addr, 8*WIDTH+4, _YPOS);
		#endif
	#else
		SetXY(8*WIDTH,_YPOS);
		Lcd_Write(DATA, 0x08);
		Lcd_Write(DATA, 0x1C);
		Lcd_Write(DATA, 0x08);
		Lcd_Write(DATA, 0x00);
		#ifdef _KS0108
			EE_Char_Small(addr, 8*WIDTH+6, _YPOS);
		#else
			EE_Char_Small(addr, 8*WIDTH+4, _YPOS);
		#endif
		Lcd_Write(DATA, 0x00);
		Lcd_Write(DATA, 0x08);
		Lcd_Write(DATA, 0x1C);
		Lcd_Write(DATA, 0x08);
	#endif
#endif
return;		
}

#ifdef	_ST7920
void LCDChar(unsigned char sym, unsigned char pos) {
	SetC(pos);
	Lcd_Write(DATA, 0xA7);
	Lcd_Write(DATA, sym);
return;
}


void LCDWriteMenuArray(unsigned char pos, unsigned char * msg, unsigned char len) {
unsigned char i;

	Lcd_Write(CMD, 0x80 | pos);
	for (i=0;i<len;i++)
		Lcd_Write(DATA, msg[i]);
return;
}

void LCD_Dec(unsigned int val, unsigned char pos) {
//unsigned int k;
unsigned char show, k=0;

Lcd_Write(CMD, 0x80 | pos);

if(cfg&0x20)
	if (val<50) {
		Lcd_Write(DATA, lcd_dig_h);
		Lcd_Write(DATA, lcd_dash);
		Lcd_Write(DATA, lcd_dig_h);
		Lcd_Write(DATA, lcd_dash);
		Lcd_Write(DATA, lcd_dig_h);
		Lcd_Write(DATA, lcd_dash);
		return;
  }
  show=0;
  	while (val>=100) {
  		k++;
  		val-=100;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	Lcd_Write(DATA, lcd_dig_h);
	Lcd_Write(DATA, lcd_dig+k);
	k=0;
  	while (val>=10) {
  		k++;
  		val-=10;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	Lcd_Write(DATA, lcd_dig_h);
	Lcd_Write(DATA, lcd_dig+k);
	Lcd_Write(DATA, lcd_dig_h);
	Lcd_Write(DATA, lcd_dig+val);

return;
}

void LCD_Dec_Small(unsigned int val, unsigned char pos) {
unsigned char show, k=0;

	Lcd_Write(CMD, 0x80 | pos);
	Lcd_Write(DATA, lcd_sp);

	show=0;

  	while (val>=100) {
  		k++;
  		val-=100;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	Lcd_Write(DATA, lcd_sdig + k);
	k=0;
  	while (val>=10) {
  		k++;
  		val-=10;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	Lcd_Write(DATA, lcd_sdig + k);
	Lcd_Write(DATA, lcd_sdig + val);
return;
}
#endif

#if defined(_COLOR) || defined(_COLOR_16)
void EE_Char_Large(unsigned int eesym, unsigned char x, unsigned char y) {
unsigned char i;
unsigned char sym, pix;
	SetXY(x, y, 0x0C, 0x17);
		for (i=0;i<42;i++) {
			sym=BigNumbers[(unsigned int)(42*eesym+i)];
			for (pix=0; pix<8 ; pix++) {
				if(sym&0x01)                   
				{
					Lcd_S();
					#ifdef _COLOR_16
						Lcd_S();
					#endif
				}
				else
				{
					Lcd_H(); 
					#ifdef _COLOR_16
						Lcd_H(); 
					#endif
				}
				sym>>=1;
			}
		}

return;
}

void EE_Char_Mid(unsigned char eesym, unsigned char x, unsigned char y) {
	unsigned char line,sym,j,s1,s2,pix;

	SetXY(x, y, 0x0C, 0x0F);
	for (line=0;line<5;line++) {
		sym=EERead((eesym<<2)+eesym+line);
		for(j=0;j<2;j++) {
			s1=conv[sym&0x0F];
			s2=conv[sym>>4];
			for (pix=0; pix<8 ; pix++) {
				if(s1&0x01)                   
				{
					#ifdef _COLOR
						#ifdef _SINGLE
							Lcd_S();
						#else
							Lcd_B();
						#endif
					#endif
					#ifdef _COLOR_16
						Lcd_S(); 
						#ifdef _SINGLE
							Lcd_S();
						#else
							Lcd_H();
						#endif
					#endif					
				}
				else
				{
					Lcd_H(); 
					#ifdef _COLOR_16
						Lcd_H(); 
					#endif
				}
				s1>>=1;
			}
			for (pix=0; pix<8 ; pix++) {
				if(s2&0x01)                   
				{
					#ifdef _COLOR
						#ifdef _SINGLE
							Lcd_S();
						#else
							Lcd_B();
						#endif
					#endif
					#ifdef _COLOR_16
						Lcd_S(); 
						#ifdef _SINGLE
							Lcd_S();
						#else
							Lcd_H();
						#endif
					#endif					
				}
				else
				{
					Lcd_H(); 
					#ifdef _COLOR_16
						Lcd_H(); 
					#endif
				}
				s2>>=1;
			}
		}
	}

return;
}

void EE_Char_Small(unsigned char eesym, unsigned char x, unsigned char y) {
	unsigned char line,sym;
	unsigned char pix;
	SetXY(x, y, 0x06, 0x07);

	for (line=0;line<5;line++) {
		sym=EERead((eesym<<2)+eesym+line);
		for (pix=0; pix<8 ; pix++) {
            if(sym&0x01)                   
            {
				Lcd_S(); 
				#ifdef _COLOR_16
					Lcd_H(); 
				#endif
			}
			else
			{
				Lcd_H(); 
				#ifdef _COLOR_16
					Lcd_H(); 
				#endif
			}
           	sym>>=1;
		}
	}

return;
}

#else
void EE_Char_Mid(unsigned char eesym, unsigned char x, unsigned char y) {
	unsigned char line,sym, j;

	for (line=0;line<5;line++) {
		SetXY(x,y);
		sym=EERead((eesym<<2)+eesym+line);
		for(j=0;j<2;j++)
			Lcd_Write(DATA, conv[sym&0x0F]);
			SetXY(x,++y);
		for(j=0;j<2;j++)
			Lcd_Write(DATA, conv[sym>>4]);
		y--;
		x+=2;
	}
return;
}
void EE_Char_Small(unsigned char eesym, unsigned char x, unsigned char y) {
	unsigned char line,sym;
	SetXY(x,y);

	for (line=0;line<5;line++) {
		sym=EERead((eesym<<2)+eesym+line);
		Lcd_Write(DATA, sym);
	}
return;
}
#endif

#ifdef _LARGE
void ZZZ(unsigned char x, unsigned char y) {
	EE_Char_Large(0x0B,x,y);
	x+=MID_WIDTH+2;
	EE_Char_Large(0x0B,x,y);
	x+=MID_WIDTH+2;
	EE_Char_Large(0x0B,x,y);
return;
}

void EE_Dec_Large(unsigned int val, unsigned char x, unsigned char y) {
//unsigned int k;
unsigned char show, k=0;
/*if(cfg&0x20)
	if (val<50) {
		EE_Char_Large(0x0B,x,y);
		x+=MID_WIDTH+2;
		EE_Char_Large(0x0B,x,y);
		x+=MID_WIDTH+2;
		EE_Char_Large(0x0B,x,y);
		return;
  }
*/
	show=0;
  	while (val>=100) {
  		k++;
  		val-=100;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	EE_Char_Large(k,x,y);
	k=0;
  	while (val>=10) {
  		k++;
  		val-=10;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	x+=MID_WIDTH+2;
	EE_Char_Large(k,x,y);
	x+=MID_WIDTH+2;
	EE_Char_Large(val,x,y);

return;
}
#else
void ZZZ(unsigned char x, unsigned char y) {
	EE_Char_Mid(0x0B,x,y);
	x+=MID_WIDTH;
	EE_Char_Mid(0x0B,x,y);
	x+=MID_WIDTH;
	EE_Char_Mid(0x0B,x,y);
return;
}
#endif

#ifndef	_ST7920
void EE_Dec(unsigned int val, unsigned char x, unsigned char y) {
//unsigned int k;
unsigned char show, k=0;
/*
	if(cfg&0x20)
	if (val<50) {
		EE_Char_Mid(0x0B,x,y);
		x+=MID_WIDTH;
		EE_Char_Mid(0x0B,x,y);
		x+=MID_WIDTH;
		EE_Char_Mid(0x0B,x,y);
		return;
  }
 */
  show=0;
  	while (val>=100) {
  		k++;
  		val-=100;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	EE_Char_Mid(k,x,y);
	k=0;
  	while (val>=10) {
  		k++;
  		val-=10;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	x+=MID_WIDTH;
	EE_Char_Mid(k,x,y);
	x+=MID_WIDTH;
	EE_Char_Mid(val,x,y);

return;
}

void EE_Dec_Small(unsigned int val, unsigned char x, unsigned char y) {
//unsigned int k;
unsigned char show, k=0;
// 	if (val<50) {
// 		EE_Char_Mid(0x0B,1,position);
// 		EE_Char_Mid(0x0B,2,position);
// 		EE_Char_Mid(0x0B,3,position);
// 		return;
// 	}
	show=0;

  	while (val>=100) {
  		k++;
  		val-=100;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	EE_Char_Small(k,x,y);
	k=0;
  	while (val>=10) {
  		k++;
  		val-=10;
  	}
	if (k) show=1;
	if (!show) k=10; // space
	x+=WIDTH;
	EE_Char_Small(k,x,y);
	x+=WIDTH;
	EE_Char_Small(val,x,y);
/*	k=val/100;
	if (k) show=1;
	val=val-k*100;
	if (!show) k=10;
	EE_Char_Small(k,x*WIDTH,y*HEIGHT);
	k=val/10;
	if (k) show=1;
	val=val-k*10;
	if (!show) k=10;
	EE_Char_Small(k,(++x)*WIDTH,y*HEIGHT);
	EE_Char_Small(val,(++x)*WIDTH,y*HEIGHT);
*/
return;
}
#endif

void EE_Menu(void) {
#ifdef	_ST7920
	// ПАЯЛЬНИК / IRON
	LCDWriteMenuArray(1,menu_2,sizeof(menu_2));
	// ФЕН / HOTAIR
	LCDWriteMenuArray(9,menu_3,sizeof(menu_3));
#elif _PCD8544	// Nokia 3310
	// УСТ / SET
	EEWriteMenuArray(2*WIDTH,0,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArray(8*WIDTH,0,menu_1,sizeof(menu_1));
	// ПАЯЛЬНИК / IRON
	EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
	// ФЕН / HOTAIR
	EEWriteMenuArray(WIDTH,3*HEIGHT,menu_3,sizeof(menu_3));
	// СОН
	if (sleep)
		EEWriteMenuArray(8*WIDTH,3*HEIGHT,menu_8,sizeof(menu_8));
	// ОБОРОТЫ / SPEED
	EEWriteMenuArray(WIDTH,5*HEIGHT,menu_4,sizeof(menu_4));
	// %
	EEWriteMenuArray(13*WIDTH,5*HEIGHT,menu_5,sizeof(menu_5));
#elif _SINGLE
	#ifdef	_LARGE
		// УСТ / SET
		EEWriteMenuArray(6*WIDTH+4,0,menu_0,sizeof(menu_0));
		// ТЕК / GET
		EEWriteMenuArray(14*WIDTH+4,0,menu_1,sizeof(menu_1));
		// ПАЯЛЬНИК / IRON
		EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
		// ФЕН / HOTAIR
		EEWriteMenuArray(WIDTH,5*HEIGHT+2,menu_3,sizeof(menu_3));
		// СОН
		if (sleep)
			EEWriteMenuArray(12*WIDTH,5*HEIGHT+2,menu_8,sizeof(menu_8));
		// ОБОРОТЫ / SPEED
		EEWriteMenuArray(WIDTH,9*HEIGHT+4,menu_4,sizeof(menu_4));
		// %
		EEWriteMenuArray(19*WIDTH,9*HEIGHT+4,menu_5,sizeof(menu_5));
		if(cfg&0x04)
		// Подогрев / Preheat
			EEWriteMenuArray(WIDTH,11*HEIGHT+4,menu_7,sizeof(menu_7));
		else 
		// ПАЯЛЬНИК / IRON
			EEWriteMenuArray(WIDTH,11*HEIGHT+4,menu_2,sizeof(menu_2));
		// ШИМ / PWM
		EEWriteMenuArray(WIDTH,15*HEIGHT+6,menu_6,sizeof(menu_6));
	#else
		// УСТ / SET
		EEWriteMenuArray(2*WIDTH+4,0,menu_0,sizeof(menu_0));
		// ТЕК / GET
		EEWriteMenuArray(10*WIDTH+4,0,menu_1,sizeof(menu_1));
		// ПАЯЛЬНИК / IRON
		EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
		// ФЕН / HOTAIR
		EEWriteMenuArray(WIDTH,3*HEIGHT,menu_3,sizeof(menu_3));
		if(cfg&0x04)
		// Подогрев / Preheat
			EEWriteMenuArray(WIDTH,5*HEIGHT,menu_7,sizeof(menu_7));
		else 
		// ПАЯЛЬНИК / IRON
			EEWriteMenuArray(WIDTH,5*HEIGHT,menu_2,sizeof(menu_2));
		// ШИМ / PWM
		EEWriteMenuArray(WIDTH,7*HEIGHT,menu_6,sizeof(menu_6));
		// СОН
		if (sleep)
			EEWriteMenuArray(10*WIDTH,7*HEIGHT,menu_8,sizeof(menu_8));
		// ОБОРОТЫ / SPEED
		// %
		EEWriteMenuArray(8*WIDTH,3*HEIGHT,menu_5,sizeof(menu_5));
	#endif
#elif defined(_SPFD54124) || defined(_LARGE)
	// УСТ / SET
	EEWriteMenuArrayMid(4*WIDTH,0,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArrayMid(18*WIDTH-4,0,menu_1,sizeof(menu_1));
	// ПАЯЛЬНИК / IRON
	EEWriteMenuArrayMid(2*WIDTH,2*HEIGHT,menu_2,sizeof(menu_2));
	// ФЕН / HOTAIR
	EEWriteMenuArrayMid(2*WIDTH,7*HEIGHT+2,menu_3,sizeof(menu_3));
	// СОН
	if (sleep)
		EEWriteMenuArrayMid(14*WIDTH,7*HEIGHT+2,menu_8,sizeof(menu_8));
	// ОБОРОТЫ / SPEED
	EEWriteMenuArrayMid(2*WIDTH,12*HEIGHT+4,menu_4,sizeof(menu_4));
	// %
	EEWriteMenuArrayMid(24*WIDTH,12*HEIGHT+4,menu_5,sizeof(menu_5));
#else
	// УСТ / SET
	EEWriteMenuArray(2*WIDTH+4,0,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArray(10*WIDTH+4,0,menu_1,sizeof(menu_1));
	// ПАЯЛЬНИК / IRON
	EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
	// ФЕН / HOTAIR
	EEWriteMenuArray(WIDTH,4*HEIGHT,menu_3,sizeof(menu_3));
	// СОН
	if (sleep)
		EEWriteMenuArray(8*WIDTH,4*HEIGHT,menu_8,sizeof(menu_8));
	// ОБОРОТЫ / SPEED
	EEWriteMenuArray(WIDTH,7*HEIGHT,menu_4,sizeof(menu_4));
	// %
	EEWriteMenuArray(15*WIDTH,7*HEIGHT,menu_5,sizeof(menu_5));
#endif
return;
}
void EE_Menu_2(void) {
#ifdef _PCD8544	// Nokia 3310
	// УСТ / SET
	EEWriteMenuArray(2*WIDTH,0,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArray(8*WIDTH,0,menu_1,sizeof(menu_1));
	// ПАЯЛЬНИК / IRON
	EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
	// ШИМ / PWM
	#ifndef _3CH
		EEWriteMenuArray(WIDTH,3*HEIGHT,menu_6,sizeof(menu_6));
	#endif
	// СОН
	if (sleep)
		EEWriteMenuArray(8*WIDTH,3*HEIGHT,menu_8,sizeof(menu_8));
#elif defined(_SPFD54124) || defined(_LARGE)
	// УСТ / SET
	EEWriteMenuArrayMid(4*WIDTH,0,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArrayMid(18*WIDTH-4,0,menu_1,sizeof(menu_1));
	// ПАЯЛЬНИК / IRON
	if(cfg&0x04)
		// Подогрев / Preheat
		EEWriteMenuArrayMid(2*WIDTH,2*HEIGHT,menu_7,sizeof(menu_7));
	else
		EEWriteMenuArrayMid(2*WIDTH,2*HEIGHT,menu_2,sizeof(menu_2));
	// ШИМ / PWM
	EEWriteMenuArrayMid(2*WIDTH,7*HEIGHT+2,menu_6,sizeof(menu_6));
	// СОН
	if (sleep)
		EEWriteMenuArrayMid(14*WIDTH,7*HEIGHT+2,menu_8,sizeof(menu_8));
#else
	// УСТ / SET
	EEWriteMenuArray(2*WIDTH+4,0,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArray(10*WIDTH+4,0,menu_1,sizeof(menu_1));
	// ПАЯЛЬНИК / IRON
//	EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
	if(cfg&0x04)
	// Подогрев / Preheat
		EEWriteMenuArray(WIDTH,1*HEIGHT,menu_7,sizeof(menu_7));
	else 
		EEWriteMenuArray(WIDTH,1*HEIGHT,menu_2,sizeof(menu_2));
	// ШИМ / PWM
	#ifndef _3CH
		EEWriteMenuArray(WIDTH,4*HEIGHT,menu_6,sizeof(menu_6));
	#endif
	// СОН
	if (sleep)
		EEWriteMenuArray(8*WIDTH,4*HEIGHT,menu_8,sizeof(menu_8));
#endif
}

void EEShowConfig(void) {
 // Configuration
				led=1;
				if (LCDINIT) { // Reinit display
					LCDINIT=0;
					Lcd_Reinit(contrast);
				}
				if (MENU) { // Show menu
					Lcd_Clear();
					EE_Config_Menu();
					MENU=0;
				}
				CMenu_Selected(menu_selected);
				#ifdef _ST7920
				#elif _PCD8544	// Nokia 3310
					// Contrast
					EE_Dec_Small(contrast, 10*WIDTH, 0*HEIGHT);
					// INV
					set = (cfg&0x02) ? 0x01 : 0x00;
					EE_Dec_Small(set, 10*WIDTH, 1*HEIGHT);
					EE_Dec_Small(sleep,10*WIDTH,2*HEIGHT);
					EE_Dec_Small(fan_off>>1,10*WIDTH,3*HEIGHT);
					EE_Dec_Small(sleepi, 10*WIDTH, 4*HEIGHT);
					#if defined(_1CH) || defined(_4CH)
						set = (cfg&0x04) ? 0x01 : 0x00;
						EE_Dec_Small(set, 10*WIDTH, 5*HEIGHT);
					#endif
				#elif _KS0108
					// SHOW
					EE_Dec_Small(sleep,10*WIDTH,0*HEIGHT);
					EE_Dec_Small(fan_off>>1,10*WIDTH,1*HEIGHT);
					EE_Dec_Small(sleepi, 10*WIDTH, 2*HEIGHT);
					#if defined(_1CH) || defined(_4CH)
						set = (cfg&0x04) ? 0x01 : 0x00;
						EE_Dec_Small(set, 10*WIDTH, 3*HEIGHT);
					#endif
				#elif _LARGE
					// INV
					set = (cfg&0x02) ? 0x01 : 0x00;
					EE_Dec_Small(set, 13*WIDTH, 0*HEIGHT);
					EE_Dec_Small(sleep, 13*WIDTH, 1*HEIGHT);
					EE_Dec_Small(fan_off>>1, 13*WIDTH, 2*HEIGHT);
					EE_Dec_Small(sleepi, 13*WIDTH, 3*HEIGHT);
					#if defined(_1CH) || defined(_4CH)
						set = (cfg&0x04) ? 0x01 : 0x00;
						EE_Dec_Small(set, 13*WIDTH, 4*HEIGHT);
					#endif
				#else
					// Contrast
					EE_Dec_Small(contrast, 13*WIDTH, 0*HEIGHT);
					// MX
					set = (cfg&0x01) ? 0x01 : 0x00;
					EE_Dec_Small(set, 13*WIDTH, 1*HEIGHT);
					// MY
					set = (cfg&0x08) ? 0x01 : 0x00;
					EE_Dec_Small(set, 13*WIDTH, 2*HEIGHT);
					// INV
					set = (cfg&0x02) ? 0x01 : 0x00;
					EE_Dec_Small(set, 13*WIDTH, 3*HEIGHT);
					EE_Dec_Small(sleep, 13*WIDTH, 4*HEIGHT);
					EE_Dec_Small(fan_off>>1, 13*WIDTH, 5*HEIGHT);
					EE_Dec_Small(sleepi, 13*WIDTH, 6*HEIGHT);
					#if defined(_1CH) || defined(_4CH)
						set = (cfg&0x04) ? 0x01 : 0x00;
						EE_Dec_Small(set, 13*WIDTH, 7*HEIGHT);
					#endif
				#endif
}

void EEShow(void) {
			#ifdef _ST7920	// ST7920
			if (!MENU_2) {
				#ifdef _1CH
					if (!(cfg&0x04)) {
						// Iron
						EE_Dec(itempav>>1,8*WIDTH,2*HEIGHT);
						ON_OFF(IRON, 0,4*HEIGHT);
					} else {
						//EE_Dec(prog[1]>>1,1*WIDTH,2*HEIGHT); 
						EE_Dec(hatempav>>1,8*WIDTH,2*HEIGHT);
						//EE_Dec_Small(fan_speed,10*WIDTH,5*HEIGHT);
						ON_OFF(HOTAIR, 0, 4*HEIGHT);
					}
					if (!NOSLEEP)
						EE_Dec_Small(ssleep,11*WIDTH,4*HEIGHT);
				#else
					// Iron
					//EE_Dec_Small(prog[0]>>1,3*WIDTH,2*HEIGHT);
					LCD_Dec(itempav>>1,0x15);
					/*#ifdef _RU
						ON_OFF(IRON, 1, 9);
					#else
						ON_OFF(IRON, 1, 5);
					#endif*/
					// Hot Air
					//EE_Dec_Small(prog[1]>>1,3*WIDTH,4*HEIGHT); 
					LCD_Dec(hatempav>>1,0x1D);
					/*
					#ifdef _RU
						ON_OFF(HOTAIR, 3, 4);
					#else
						ON_OFF(HOTAIR, 3, 7);
					#endif*/
					//if (!NOSLEEP)
						//EE_Dec_Small(ssleep,11*WIDTH,3*HEIGHT);
					// Fan speed
					LCD_Dec_Small(fan_speed,0x0D);
					Lcd_Write(DATA, lcd_ps);
					Lcd_Write(DATA, 0x20);
				#endif
			} else {
			}
			#elif _PCD8544	// Nokia 3310 84x48
			if (!MENU_2) {
				#ifdef _1CH
					if (!(cfg&0x04)) {
						// Iron
						//EE_Dec(prog[0]>>1,1*WIDTH,2*HEIGHT);
						EE_Dec(itempav>>1,8*WIDTH,2*HEIGHT);
						ON_OFF(IRON, 0,4*HEIGHT);
					} else {
						//EE_Dec(prog[1]>>1,1*WIDTH,2*HEIGHT); 
						EE_Dec(hatempav>>1,8*WIDTH,2*HEIGHT);
						//EE_Dec_Small(fan_speed,10*WIDTH,5*HEIGHT);
						ON_OFF(HOTAIR, 0, 4*HEIGHT);
					}
					if (!NOSLEEP)
						EE_Dec_Small(ssleep,11*WIDTH,4*HEIGHT);
				#else
					// Iron
					//EE_Dec_Small(prog[0]>>1,3*WIDTH,2*HEIGHT);
					EE_Dec_Small(itempav>>1,9*WIDTH,2*HEIGHT);
					#ifdef _RU
						ON_OFF(IRON, 1, 9);
					#else
						ON_OFF(IRON, 1, 5);
					#endif
					// Hot Air
					//EE_Dec_Small(prog[1]>>1,3*WIDTH,4*HEIGHT); 
					EE_Dec_Small(hatempav>>1,9*WIDTH,4*HEIGHT);
					#ifdef _RU
						ON_OFF(HOTAIR, 3, 4);
					#else
						ON_OFF(HOTAIR, 3, 7);
					#endif
					if (!NOSLEEP)
						EE_Dec_Small(ssleep,11*WIDTH,3*HEIGHT);
					// Fan speed
					//EE_Dec_Small(fan_speed,10*WIDTH,5*HEIGHT);
				#endif
			} else {
			}
			#else
				#ifdef _SINGLE
					#ifdef _LARGE
						// Iron
						//EE_Dec(prog[0]>>1,6*WIDTH,2*HEIGHT+4);
						#ifdef _IRON_SW
							if (ISWITCH)
								ZZZ(14*WIDTH-4,2*HEIGHT);
							else
								EE_Dec_Large(itempav>>1,14*WIDTH-4,2*HEIGHT);
						#else
							EE_Dec_Large(itempav>>1,14*WIDTH-4,2*HEIGHT);
						#endif
						#ifdef _RU
							ON_OFF(IRON, 1*HEIGHT, 9);
						#else
							ON_OFF(IRON, 1*HEIGHT, 5);
						#endif
						// Sleep timer
						if (!NOSLEEP)
							EE_Dec_Small(ssleep,16*WIDTH,5*HEIGHT+2);
						// Hot Air
						//EE_Dec(prog[1]>>1,6*WIDTH,6*HEIGHT+2+4); 
							if (HASWITCH)
								ZZZ(14*WIDTH-4,6*HEIGHT+2);
							else
								EE_Dec_Large(hatempav>>1,14*WIDTH-4,6*HEIGHT+2);
						#ifdef _RU
							ON_OFF(HOTAIR, 5*HEIGHT+2, 4);
						#else
							ON_OFF(HOTAIR, 5*HEIGHT+2, 7);
						#endif
						// Fan speed
						//EE_Dec_Small(fan_speed,16*WIDTH,9*HEIGHT+4);
						// PWMPlus
						//EE_Dec(prog[2]>>1,6*WIDTH,12*HEIGHT+4+4);
						EE_Dec_Large(i2tempav>>1,14*WIDTH-4,12*HEIGHT+4);
						#ifdef _RU
							ON_OFF(PWMPLUS, 11*HEIGHT+4, 9);
						#else
							ON_OFF(PWMPLUS, 11*HEIGHT+4, (!(cfg&0x04))?5:7);
						#endif
						#ifndef _3CH
							//EE_Dec_Large(pwm_set,14*WIDTH-4,16*HEIGHT+6); 
							#ifdef _RU
								ON_OFF(PWM, 15*HEIGHT+6, 4);
							#else
								ON_OFF(PWM, 15*HEIGHT+6, 4);
							#endif
						#endif
					#else
					// Iron
						//EE_Dec_Small(prog[0]>>1,3*WIDTH+4,2*HEIGHT);
	//					EE_Dec_Small(itempav>>1,11*WIDTH+4,2*HEIGHT);
						EE_Dec(itempav>>1,5*MID_WIDTH,1*HEIGHT);
						#ifdef _RU
							ON_OFF(IRON, 1, 9);
						#else
							ON_OFF(IRON, 1, 5);
						#endif
						// Hot Air
						//EE_Dec_Small(prog[1]>>1,3*WIDTH+4,4*HEIGHT); 
	//					EE_Dec_Small(hatempav>>1,11*WIDTH+4,4*HEIGHT);
						EE_Dec(hatempav>>1,5*MID_WIDTH,3*HEIGHT);
						#ifdef _RU
							ON_OFF(HOTAIR, 3, 4);
						#else
							ON_OFF(HOTAIR, 3, 7);
						#endif
						// PWMPlus
						//EE_Dec_Small(prog[2]>>1,3*WIDTH+4,6*HEIGHT);
	//					EE_Dec_Small(i2tempav>>1,11*WIDTH+4,6*HEIGHT);
						EE_Dec(i2tempav>>1,5*MID_WIDTH,5*HEIGHT);
						#ifdef _RU
							ON_OFF(PWMPLUS, 5, 9);
						#else
							ON_OFF(PWMPLUS, 5, (!(cfg&0x04))?5:7);
						#endif
						// PWM
						//EE_Dec_Small(pwm_set,6*WIDTH,7*HEIGHT);
						#ifdef _RU
							ON_OFF(PWM, 7, 4);
						#else
							ON_OFF(PWM, 7, 4);
						#endif
						if (!NOSLEEP)
							EE_Dec_Small(ssleep,13*WIDTH,7*HEIGHT);
						// Fan speed
						//EE_Dec_Small(fan_speed,5*WIDTH,3*HEIGHT);
					#endif
				
				#elif !defined(_DUAL) && !defined(_SINGLE)
				if (!MENU_2) {
					#ifdef _1CH
						if (!(cfg&0x04)) {
							// Iron
							//EE_Dec(prog[0]>>1,1*MID_WIDTH,4*HEIGHT);
							EE_Dec(itempav>>1,5*MID_WIDTH,4*HEIGHT);
							ON_OFF(IRON, 2*WIDTH+4, 3*HEIGHT);
						} else {
							//EE_Dec(prog[1]>>1,1*MID_WIDTH,4*HEIGHT); 
							EE_Dec(hatempav>>1,5*MID_WIDTH,4*HEIGHT);
							//EE_Dec_Small(fan_speed,12*WIDTH,7*HEIGHT);
							ON_OFF(HOTAIR, 2*WIDTH+4, 3*HEIGHT);
						}
						if (!NOSLEEP)
							EE_Dec_Small(ssleep,13*WIDTH,6*HEIGHT);
					#else
						#ifdef _LARGE
							// Iron
							//EE_Dec_Large(prog[0]>>1,5*WIDTH,4*HEIGHT);
							EE_Dec_Large(itempav>>1,19*WIDTH-4,4*HEIGHT);
							#ifdef _RU
								ON_OFF(IRON, 2*HEIGHT, 9);
							#else
								ON_OFF(IRON, 2*HEIGHT, 5);
							#endif
							// Sleep timer
							if (!NOSLEEP)
								EE_Dec(ssleep,20*WIDTH,7*HEIGHT+2);
							// Hot Air
							//EE_Dec_Large(prog[1]>>1,5*WIDTH,9*HEIGHT+2); 
							EE_Dec_Large(hatempav>>1,19*WIDTH-4,9*HEIGHT+2);
							#ifdef _RU
								ON_OFF(HOTAIR, 7*HEIGHT+2, 4);
							#else
								ON_OFF(HOTAIR, 7*HEIGHT+2, 7);
							#endif
							// Fan speed
							//EE_Dec(fan_speed,18*WIDTH,12*HEIGHT+4);
						#else
							// Iron
							//EE_Dec(prog[0]>>1,1*MID_WIDTH,2*HEIGHT);
							#ifdef _IRON_SW
								if (ISWITCH)
									ZZZ(5*MID_WIDTH,2*HEIGHT);
								else
									EE_Dec(itempav>>1,5*MID_WIDTH,2*HEIGHT);
							#else
								EE_Dec(itempav>>1,5*MID_WIDTH,2*HEIGHT);
							#endif
							#ifdef _RU
								ON_OFF(IRON, 1, 9);
							#else
								ON_OFF(IRON, 1, 5);
							#endif
							// Hot Air
							//EE_Dec(prog[1]>>1,1*MID_WIDTH,5*HEIGHT); 
							if (HASWITCH)
								ZZZ(5*MID_WIDTH,5*HEIGHT);
							else
								EE_Dec(hatempav>>1,5*MID_WIDTH,5*HEIGHT);
							#ifdef _RU
								ON_OFF(HOTAIR, 4, 4);
							#else
								ON_OFF(HOTAIR, 4, 7);
							#endif
							if (!NOSLEEP)
								EE_Dec_Small(ssleep,13*WIDTH,4*HEIGHT);
							// Fan speed
							//EE_Dec_Small(fan_speed,12*WIDTH,7*HEIGHT);
						#endif
					#endif
				} else {
					#ifdef _LARGE
						// Iron
						//EE_Dec_Large(prog[2]>>1,5*WIDTH,4*HEIGHT);
						EE_Dec_Large(i2tempav>>1,19*WIDTH-4,4*HEIGHT);
						#ifdef _RU
							ON_OFF(PWMPLUS, 2*HEIGHT, 9);
						#else
							ON_OFF(PWMPLUS, 2*HEIGHT, (!(cfg&0x04))?5:7);
						#endif
						// Sleep timer
						if (!NOSLEEP)
							EE_Dec(ssleep,20*WIDTH,7*HEIGHT+2);
						#ifndef _3CH
							//EE_Dec_Large(pwm_set,5*WIDTH,9*HEIGHT+2); 
							#ifdef _RU
								ON_OFF(PWM, 7*HEIGHT+2, 4);
							#else
								ON_OFF(PWM, 7*HEIGHT+2, 4);
							#endif
						#endif
					#else
						// Iron
						//EE_Dec(prog[2]>>1,1*MID_WIDTH,2*HEIGHT);
						EE_Dec(i2tempav>>1,5*MID_WIDTH,2*HEIGHT);
						#ifdef _RU
							ON_OFF(PWMPLUS, 1, 9);
						#else
							ON_OFF(PWMPLUS, 1, (!(cfg&0x04))?5:7);
						#endif
						#ifndef _3CH
							//EE_Dec(pwm_set,5*MID_WIDTH,5*HEIGHT);
							#ifdef _RU
								ON_OFF(PWM, 4, 4);
							#else
								ON_OFF(PWM, 4, 4);
							#endif
						#endif
						if (!NOSLEEP)
							EE_Dec_Small(ssleep,13*WIDTH,4*HEIGHT);
					#endif
				}
				#else
					// Iron
					//EE_Dec(prog[0]>>1,1*MID_WIDTH,2*HEIGHT);
					EE_Dec(itempav>>1,5*MID_WIDTH,2*HEIGHT);
					#ifdef _RU
						ON_OFF(IRON, 1, 9);
					#else
						ON_OFF(IRON, 1, 5);
					#endif
					// Hot Air
					//EE_Dec(prog[1]>>1,1*MID_WIDTH,5*HEIGHT); 
					EE_Dec(hatempav>>1,5*MID_WIDTH,5*HEIGHT);
					#ifdef _RU
						ON_OFF(HOTAIR, 4, 4);
					#else
						ON_OFF(HOTAIR, 4, 7);
					#endif
					if (!NOSLEEP)
						EE_Dec_Small(ssleep,13*WIDTH,4*HEIGHT);
					// Fan speed
					//EE_Dec_Small(fan_speed,12*WIDTH,7*HEIGHT);
					DUAL=1;
					// PWMPlus
					//EE_Dec(prog[2]>>1,1*MID_WIDTH,2*HEIGHT);
					EE_Dec(i2tempav>>1,5*MID_WIDTH,2*HEIGHT);
					#ifdef _RU
						ON_OFF(PWMPLUS, 1, 9);
					#else
						ON_OFF(PWMPLUS, 1, (!(cfg&0x04))?5:7);
					#endif
					//EE_Dec(pwm_set,5*MID_WIDTH,5*HEIGHT);
					#ifdef _RU
						ON_OFF(PWM, 4, 4);
					#else
						ON_OFF(PWM, 4, 4);
					#endif
					if (!NOSLEEP)
						EE_Dec_Small(ssleep,13*WIDTH,4*HEIGHT);
					DUAL=0;
				#endif
			#endif
}

void EEShowSet(void) {
			#ifdef _ST7920	// ST7920
			if (!MENU_2) {
				#ifdef _1CH
					if (!(cfg&0x04)) {
						// Iron
						LCD_Dec(prog[0]>>1,0x09);
					} else {
						LCD_Dec(prog[1]>>1,0x09); 
						LCD_Dec_Small(fan_speed,0x19);
					}
				#else
					// Iron
					LCD_Dec(prog[0]>>1,0x11);
					// Hot Air
					LCD_Dec(prog[1]>>1,0x19); 
					// Fan speed
					//LCD_Dec_Small(fan_speed,0x14);
				#endif
			} else {
			}
			#elif _PCD8544	// Nokia 3310 84x48
			if (!MENU_2) {
				#ifdef _1CH
					if (!(cfg&0x04)) {
						// Iron
						EE_Dec(prog[0]>>1,1*WIDTH,2*HEIGHT);
					} else {
						EE_Dec(prog[1]>>1,1*WIDTH,2*HEIGHT); 
						EE_Dec_Small(fan_speed,10*WIDTH,5*HEIGHT);
					}
				#else
					// Iron
					EE_Dec_Small(prog[0]>>1,3*WIDTH,2*HEIGHT);
					// Hot Air
					EE_Dec_Small(prog[1]>>1,3*WIDTH,4*HEIGHT); 
					// Fan speed
					EE_Dec_Small(fan_speed,10*WIDTH,5*HEIGHT);
				#endif
			} else {
			}
			#else
				#ifdef _SINGLE
					#ifdef _LARGE
						// Iron
						EE_Dec(prog[0]>>1,6*WIDTH,2*HEIGHT+4);
						// Hot Air
						EE_Dec(prog[1]>>1,6*WIDTH,6*HEIGHT+2+4); 
						// Fan speed
						EE_Dec_Small(fan_speed,16*WIDTH,9*HEIGHT+4);
						// PWMPlus
						EE_Dec(prog[2]>>1,6*WIDTH,12*HEIGHT+4+4);
						#ifndef _3CH
							EE_Dec_Large(pwm_set,14*WIDTH-4,16*HEIGHT+6); 
						#endif
					#else
						// Iron
						EE_Dec_Small(prog[0]>>1,3*WIDTH+4,2*HEIGHT);
						// Hot Air
						EE_Dec_Small(prog[1]>>1,3*WIDTH+4,4*HEIGHT); 
						// PWMPlus
						EE_Dec_Small(prog[2]>>1,3*WIDTH+4,6*HEIGHT);
						// PWM
						EE_Dec_Small(pwm_set,6*WIDTH,7*HEIGHT);
						// Fan speed
						EE_Dec_Small(fan_speed,5*WIDTH,3*HEIGHT);
					#endif
				
				#elif !defined(_DUAL)
				if (!MENU_2) {
					#ifdef _1CH
						if (!(cfg&0x04)) {
							// Iron
							EE_Dec(prog[0]>>1,1*MID_WIDTH,4*HEIGHT);
						} else {
							EE_Dec(prog[1]>>1,1*MID_WIDTH,4*HEIGHT); 
							EE_Dec_Small(fan_speed,12*WIDTH,7*HEIGHT);
						}
					#else
						#ifdef _LARGE
							// Iron
							EE_Dec_Large(prog[0]>>1,5*WIDTH,4*HEIGHT);
							// Hot Air
							EE_Dec_Large(prog[1]>>1,5*WIDTH,9*HEIGHT+2); 
							// Fan speed
							EE_Dec(fan_speed,18*WIDTH,12*HEIGHT+4);
						#else
							// Iron
							EE_Dec(prog[0]>>1,1*MID_WIDTH,2*HEIGHT);
							// Hot Air
							EE_Dec(prog[1]>>1,1*MID_WIDTH,5*HEIGHT); 
							// Fan speed
							EE_Dec_Small(fan_speed,12*WIDTH,7*HEIGHT);
						#endif
					#endif
				} else {
					#ifdef _LARGE
						// Iron
						EE_Dec_Large(prog[2]>>1,5*WIDTH,4*HEIGHT);
						#ifndef _3CH
							EE_Dec_Large(pwm_set,5*WIDTH,9*HEIGHT+2); 
						#endif
					#else
						// Iron
						EE_Dec(prog[2]>>1,1*MID_WIDTH,2*HEIGHT);
						#ifndef _3CH
							EE_Dec(pwm_set,5*MID_WIDTH,5*HEIGHT);
						#endif
					#endif
				}
				#else
					// Iron
					EE_Dec(prog[0]>>1,1*MID_WIDTH,2*HEIGHT);
					// Hot Air
					EE_Dec(prog[1]>>1,1*MID_WIDTH,5*HEIGHT); 
					// Fan speed
					EE_Dec_Small(fan_speed,12*WIDTH,7*HEIGHT);
					DUAL=1;
					// PWMPlus
					EE_Dec(prog[2]>>1,1*MID_WIDTH,2*HEIGHT);
					EE_Dec(pwm_set,5*MID_WIDTH,5*HEIGHT);
					DUAL=0;
				#endif
			#endif
}

#ifdef _1CH
void EE_Menu_Single(void) {
#ifdef _PCD8544	// Nokia 3310
	if(!(cfg&0x04))
		#ifdef _PREHEAT
			// Подогрев / Preheat
			EEWriteMenuArray(2*WIDTH,0*HEIGHT,menu_7,sizeof(menu_7));
		#else
			// ПАЯЛЬНИК / IRON
			EEWriteMenuArray(2*WIDTH,0*HEIGHT,menu_2,sizeof(menu_2));
		#endif
	else
		// ФЕН / HOTAIR
		EEWriteMenuArray(5*WIDTH,0*HEIGHT,menu_3,sizeof(menu_3));
	// УСТ / SET
	EEWriteMenuArray(1*WIDTH+3,1*HEIGHT,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArray(8*WIDTH+3,1*HEIGHT,menu_1,sizeof(menu_1));
	// СОН
	if (sleep)
		EEWriteMenuArray(8*WIDTH,4*HEIGHT,menu_8,sizeof(menu_8));
	if (cfg&0x04) {
		// ОБОРОТЫ / SPEED
		EEWriteMenuArray(WIDTH,5*HEIGHT,menu_4,sizeof(menu_4));
		// %
		EEWriteMenuArray(13*WIDTH,5*HEIGHT,menu_5,sizeof(menu_5));
	}
#else
	if(!(cfg&0x04))
		#ifdef _PREHEAT
			// Подогрев / Preheat
			EEWriteMenuArrayMid(0*WIDTH,0*HEIGHT,menu_7,sizeof(menu_7));
		#else
			// ПАЯЛЬНИК / IRON
			EEWriteMenuArrayMid(0*WIDTH,0*HEIGHT,menu_2,sizeof(menu_2));
		#endif
	else
		// ФЕН / HOTAIR
		EEWriteMenuArrayMid(5*WIDTH,0*HEIGHT,menu_3,sizeof(menu_3));
	// УСТ / SET
	EEWriteMenuArray(2*WIDTH+4,2*HEIGHT,menu_0,sizeof(menu_0));
	// ТЕК / GET
	EEWriteMenuArray(10*WIDTH+4,2*HEIGHT,menu_1,sizeof(menu_1));
	// СОН
	if (sleep)
		EEWriteMenuArray(8*WIDTH,6*HEIGHT,menu_8,sizeof(menu_8));
	if (cfg&0x04) {
		// ОБОРОТЫ / SPEED
		EEWriteMenuArray(WIDTH,7*HEIGHT,menu_4,sizeof(menu_4));
		// %
		EEWriteMenuArray(15*WIDTH,7*HEIGHT,menu_5,sizeof(menu_5));
	}	
#endif
return;
}
#endif

#ifndef _4K
void EE_Config_Menu(void) {
#ifdef	_ST7920
	// СОН
	LCDWriteMenuArray(0x01,cmenu_5,sizeof(cmenu_5));
	// Т ОТКЛ
	LCDWriteMenuArray(0x11,cmenu_6,sizeof(cmenu_6));
	// ПРЯТАТЬ
	LCDWriteMenuArray(0x09,cmenu_4,sizeof(cmenu_4));
	#ifdef _1CH
		// Фен
		LCDWriteMenuArray(0x19,menu_3,sizeof(menu_3));
	#endif
//unsigned int * temp;

/*	// ПАЯЛЬНИК / IRON
	EEWriteMenuArray(6*WIDTH,0*HEIGHT,menu_2,1);
	// ФЕН / HOTAIR
	EEWriteMenuArray(10*WIDTH,0*HEIGHT,menu_3,1);
	// ОБОРОТЫ / SPEED
	EEWriteMenuArray(14*WIDTH,0*HEIGHT,menu_4,1);
	// ПР0-2 / PR0-2
	for (i=0; i<3; i++) {
		EEWriteMenuArray(WIDTH,(i+1)*HEIGHT,menu_6,3);
		menu_6[2]++;
	}*/
#elif defined(_PCD8544) || defined (_SSD1828)	// Nokia 3310 || Motorola C330
	// КОНТРАСТ
	EEWriteMenuArray(WIDTH,0*HEIGHT,cmenu_0,sizeof(cmenu_0));
	// ИНВЕРСИЯ
	EEWriteMenuArray(WIDTH,1*HEIGHT,cmenu_3,sizeof(cmenu_3));
	// СОН
	EEWriteMenuArray(WIDTH,2*HEIGHT,cmenu_5,sizeof(cmenu_5));
	// Т ОТКЛ
	EEWriteMenuArray(WIDTH,3*HEIGHT,cmenu_6,sizeof(cmenu_6));
	// T12 СОН
	EEWriteMenuArray(WIDTH,4*HEIGHT,cmenu_4,sizeof(cmenu_4));
	#ifdef _1CH
		// Фен
		EEWriteMenuArray(WIDTH,5*HEIGHT,menu_3,sizeof(menu_3));
	#endif
#elif	_KS0108
	// СОН
	EEWriteMenuArray(WIDTH,0*HEIGHT,cmenu_5,sizeof(cmenu_5));
	// Т ОТКЛ
	EEWriteMenuArray(WIDTH,1*HEIGHT,cmenu_6,sizeof(cmenu_6));
	// T12 СОН
	EEWriteMenuArray(WIDTH,2*HEIGHT,cmenu_4,sizeof(cmenu_4));
	#ifdef _1CH
		// Фен
		EEWriteMenuArray(WIDTH,3*HEIGHT,menu_3,sizeof(menu_3));
	#elif _4CH
		if(cfg&0x04) {
			// Подогрев / Preheat
			EEWriteMenuArray(WIDTH,3*HEIGHT,menu_7,sizeof(menu_7));
			EE_Char_Small(ee_plus,(sizeof(menu_7)+1)*WIDTH,3*HEIGHT);
		} else { 
			EEWriteMenuArray(WIDTH,3*HEIGHT,menu_2,sizeof(menu_2));
			EE_Char_Small(ee_plus,(sizeof(menu_2)+1)*WIDTH,3*HEIGHT);
		}
	#endif
#elif	_LARGE
	// ИНВЕРСИЯ
	EEWriteMenuArray(WIDTH,0*HEIGHT,cmenu_3,sizeof(cmenu_3));
	// СОН
	EEWriteMenuArray(WIDTH,1*HEIGHT,cmenu_5,sizeof(cmenu_5));
	// Т ОТКЛ
	EEWriteMenuArray(WIDTH,2*HEIGHT,cmenu_6,sizeof(cmenu_6));
	// T12 СОН
	EEWriteMenuArray(WIDTH,3*HEIGHT,cmenu_4,sizeof(cmenu_4));
	#ifdef _1CH
		// Фен
		EEWriteMenuArray(WIDTH,4*HEIGHT,menu_3,sizeof(menu_3));
	#elif _4CH
		if(cfg&0x04) {
			// Подогрев / Preheat
			EEWriteMenuArray(WIDTH,4*HEIGHT,menu_7,sizeof(menu_7));
			EE_Char_Small(ee_plus,(sizeof(menu_7)+1)*WIDTH,4*HEIGHT);
		} else { 
			EEWriteMenuArray(WIDTH,4*HEIGHT,menu_2,sizeof(menu_2));
			EE_Char_Small(ee_plus,(sizeof(menu_2)+1)*WIDTH,4*HEIGHT);
		}
	#endif
#else
	// КОНТРАСТ
	EEWriteMenuArray(WIDTH,0*HEIGHT,cmenu_0,sizeof(cmenu_0));
	// MX
	EEWriteMenuArray(WIDTH,1*HEIGHT,cmenu_1,sizeof(cmenu_1));
	// MY
	EEWriteMenuArray(WIDTH,2*HEIGHT,cmenu_2,sizeof(cmenu_2));
	// ИНВЕРСИЯ
	EEWriteMenuArray(WIDTH,3*HEIGHT,cmenu_3,sizeof(cmenu_3));
	// СОН
	EEWriteMenuArray(WIDTH,4*HEIGHT,cmenu_5,sizeof(cmenu_5));
	// Т ОТКЛ
	EEWriteMenuArray(WIDTH,5*HEIGHT,cmenu_6,sizeof(cmenu_6));
	// T12 СОН
	EEWriteMenuArray(WIDTH,6*HEIGHT,cmenu_4,sizeof(cmenu_4));
	#ifdef _1CH
		// Фен
		EEWriteMenuArray(WIDTH,7*HEIGHT,menu_3,sizeof(menu_3));
	#elif _4CH
		if(cfg&0x04) {
			// Подогрев / Preheat
			EEWriteMenuArray(WIDTH,7*HEIGHT,menu_7,sizeof(menu_7));
			EE_Char_Small(ee_plus,(sizeof(menu_7)+1)*WIDTH,7*HEIGHT);
		} else { 
			EEWriteMenuArray(WIDTH,7*HEIGHT,menu_2,sizeof(menu_2));
			EE_Char_Small(ee_plus,(sizeof(menu_2)+1)*WIDTH,7*HEIGHT);
		}
	#endif
#endif

return;
}
#endif


/*void EEWriteMenu(unsigned char x, unsigned char y, unsigned char msg, unsigned char len) {
unsigned char i;
	SetX(x);
	SetY(y);
	for (i=0;i<6*len;i++)
		Lcd_Write(DATA,EERead(msg+i));
	return;
}*/

void EEWriteMenuArray(unsigned char x, unsigned char y, const unsigned char * msg, unsigned char len) {
unsigned char i;
	for (i=0;i<len;i++) {
		EE_Char_Small(msg[i], x+i*6, y);
	}
	return;
}

#ifndef _4K
void EEWriteMenuArrayMid(unsigned char x, unsigned char y, const unsigned char * msg, unsigned char len) {
unsigned char i;
	for (i=0;i<len;i++) {
		EE_Char_Mid(msg[i], x+i*2*WIDTH, y);
	}
	return;
}
#endif

#ifdef _1CH
	void ON_OFF(unsigned char selected, unsigned char x, unsigned char y) {
	unsigned char i,k;
	#if defined(_COLOR)	|| defined(_COLOR_16)
		unsigned char j;
	
		SetXY(x, y, sizeof(iron_pic), 0x07);
		for(i=0;i<sizeof(iron_pic);i++) {
		if(!selected)
			k=0x00;
		else
			if(!(cfg&0x04))
				k=iron_pic[i];
			else
				k=hotair_pic[i];
			for (j=0; j<8 ; j++) {
				if(k&0x01)                   
				{
					Lcd_Write(DATA,LTC);
					#ifdef _COLOR_16
						Lcd_Write(DATA,BGC);
					#endif
				}
				else
				{
					Lcd_Write(DATA,BGC);
					#ifdef _COLOR_16
						Lcd_Write(DATA,BGC);
					#endif
				}
				k>>=1;
			}
		}
	#else
		SetXY(x,y);
		for(i=0;i<sizeof(iron_pic);i++) {
		if(!selected)
			k=0x00;
		else
			if(!(cfg&0x04))
				k=iron_pic[i];
			else
				k=hotair_pic[i];
				Lcd_Write(DATA,k);
		}
	#endif
	return;
	}
#else
	void ON_OFF(unsigned char selected, unsigned char y, unsigned char w) {
	#ifndef _ENCODER
		#ifdef _LARGE
			#ifdef	_SINGLE
				EE_Char_Small((selected)?ee_ls:0xA, 0, y);
			#else
				EE_Char_Mid((selected)?ee_ls:0xA, 0, y);
			#endif
		#else
			EE_Char_Small((selected)?ee_ls:0xA, 0, y);
		#endif
	#endif
		#ifdef _LARGE
			#ifdef	_SINGLE
				EE_Char_Small((selected)?ee_gt:0xA, WIDTH*w, y);
			#else
				EE_Char_Mid((selected)?ee_gt:0xA, MID_WIDTH*w, y);
			#endif
		#else
			EE_Char_Small((selected)?ee_gt:0xA, WIDTH*w, y);
		#endif
	return;
	}
#endif

void Menu_Selected(unsigned char selected) {
unsigned char mi;

#ifdef _1CH
	if(cfg&0x04)
#endif
	{
		mi = (!selected)?MMAX:selected-1;
			
		#if defined(_COLOR) || defined(_COLOR_16)
			#ifdef _SINGLE
				EE_Char_Small(0xA, 0, menu_position[mi]); // Clear
				EE_Char_Small(ee_dot, 0, menu_position[selected]); // Draw
			#else
				EE_Char_Mid(0xA, 0, menu_position[mi]); // Clear
				EE_Char_Mid(ee_dot, 0, menu_position[selected]); // Draw
			#endif
		#else
			EE_Char_Small(0xA, 0, menu_position[mi]); // Clear
			EE_Char_Small(ee_dot, 0, menu_position[selected]); // Draw
		#endif
	}
	return;
}

void CMenu_Selected(unsigned char selected) {
unsigned char i;

	for (i=0;i<CMAX;i++) {
		#if defined(_COLOR) || defined(_COLOR_16)
			EE_Char_Small((i==selected)?ee_dot:0xA, 0, i*HEIGHT);
		#else
			EE_Char_Small((i==selected)?ee_dot:0xA, 0, i);
		#endif
	}
	return;
}
