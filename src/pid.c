/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */

#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif

#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif

#ifdef	_LOGO
#include "logo_ss.h"
#endif

int idt[2]={0,0};
unsigned int prevtemp[2]={0,0};

// PID iLimits
#define	IMIN	-0x1FFF
#define	IMAX	0x1FFF

#define PropGain_Tempr 3
#define PropGain_TemprDiv 0
#define IntegGain_Tempr 0
#define IntegGain_TemprDiv 1
#define DiffGain_TemprDiv 2

unsigned char PID(unsigned int settemp, unsigned int currenttemp, unsigned char ch) {

int PropComp_Tempr, IntegComp_Tempr, DiffComp_Tempr, TotalPID_Tempr;
unsigned char pwmval=0x00;
int dt;

/*
EE_Dec_Small(PropComp_Tempr,12,4);
EE_Dec_Small(IntegComp_Tempr,12,5);
EE_Dec_Small(DiffComp_Tempr,12,6);
EE_Dec_Small(TotalPID_Tempr,12,7);
*/
	dt = settemp - currenttemp;
	if (dt > 32) {
		idt[ch] = 0;
		pwmval = MAXPWM;
	} else {
		// Calculate Proportional component of PID
		PropComp_Tempr = dt << PropGain_Tempr >> PropGain_TemprDiv;
		// Calculate Integral component of PID
		idt[ch] += dt;
		// Since Integral component tends to windup, limit its max value
		if (idt[ch] > IMAX)
			idt[ch] = IMAX;
		if (idt[ch] < IMIN)
			idt[ch] = IMIN;
		IntegComp_Tempr = idt[ch] >> IntegGain_TemprDiv;
		// Calculate Integral component of PID
		DiffComp_Tempr = (currenttemp - prevtemp[ch]) << DiffGain_TemprDiv ;
		// Calculate PID
		TotalPID_Tempr = PropComp_Tempr + IntegComp_Tempr - DiffComp_Tempr;
//		TotalPID_Tempr = PropComp_Tempr - DiffComp_Tempr;
		prevtemp[ch] = currenttemp;
//------------------------------------------------------------------------------------
// Limit the PWM value to MAXPWM
		if (TotalPID_Tempr > MAXPWM)
			TotalPID_Tempr = MAXPWM;
		if (TotalPID_Tempr < 0)
			TotalPID_Tempr = 0;
		pwmval = (char)TotalPID_Tempr;
//------------------------------------------------------------------------------------
	}
	return pwmval;
}

// Presets
void loadPreset(unsigned char addr) {
unsigned char i;
unsigned char * pt;
	addr<<=3;
	addr+=eepr0;

	for (i=0;i<6;i++) {
		pt = (unsigned char *)(&prog[0])+i;
		*pt = EERead(addr++);
		defaults[0] = *pt;
	}
	fan_defaults=fan_speed=EERead(addr++);
	pwm_defaults=pwm_set=EERead(addr);
	
}

void savePreset(unsigned char addr) {
unsigned char i;
	EEWrite(&addr, eeprog); // Save preset number
	// Calculate EEPROM preset address
	addr<<=3;
	addr+=eepr0;
	// Save preset
	for (i=0;i<6;i++) {
		EEWrite( (unsigned char *)(&prog[0])+i, addr++);
	}
	EEWrite(&fan_speed, addr++);
	EEWrite(&pwm_set, addr++);
}

#ifndef	_AVR
// Delays
void delay_us(unsigned char count) {
unsigned char i;

	for (i=0;i<count;i++);
	return;
}

void delay_ms(unsigned char count) {
unsigned char i,j;

	for (i=0;i<count;i++) 
		for (j=0;j<0xF0;j++);
	return;
}

void delay_s(unsigned char count) {
unsigned char i,j,k;

    for (i=0;i<count;i++) {
    	wdt_reset();
		for (j=0;j<100;j++)
			for (k=0;k<100;k++);
    }
    return;
}

void debug(unsigned char count) {
	led=0;
	delay_s(count);
	led=1;
	delay_s(count);
	return;
}
#endif