/* ----------------------------------------------------------------------- */
/* --------------- Universal soldering station with hot air -------------- */
/* --------------------- Author: Alexey Grachov -------------------------- */
/* ---------------------------- gav@bmstu.ru ----------------------------- */
/* ----------------------------------------------------------------------- */
/* ----------------------- MCU dependant config -------------------------- */
/* ----------------------------------------------------------------------- */

#ifdef	_AVR
#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay_basic.h>

typedef struct Bits_t
{
	unsigned char Bit0 :1;
	unsigned char Bit1 :1;
	unsigned char Bit2 :1;
	unsigned char Bit3 :1;
	unsigned char Bit4 :1;
	unsigned char Bit5 :1;
	unsigned char Bit6 :1;
	unsigned char Bit7 :1;
} Bits;

#define	PortBBits (*((volatile Bits*)&PORTB))
#define PinBBits (*((volatile Bits*)&PINB))

#define iron	PortBBits.Bit1
#define hotAir	PortBBits.Bit2
#define pwm	PortBBits.Bit5
#define hotAirSwitch	PinBBits.Bit4
#define ironSwitch	PinBBits.Bit0
#define fan	PortBBits.Bit3
#define led	PortBBits.Bit6
#define dc	PortBBits.Bit7

#define PortCBits (*((volatile Bits*)&PORTC))
#define	pwmPlus	PortCBits.Bit1
#define rst	PortCBits.Bit2
#define cs	PortCBits.Bit3
#define sda	PortCBits.Bit4
#define sclk	PortCBits.Bit5

#define PinDBits (*((volatile Bits*)&PIND))
// Buttons
#define HAP	PinDBits.Bit0
#define HAM	PinDBits.Bit1
#define FP	PinDBits.Bit2
#define FM	PinDBits.Bit3
#define HAON	PinDBits.Bit4
#define IP	PinDBits.Bit5
#define IM	PinDBits.Bit6
#define ION	PinDBits.Bit7
// Encoder
#define ENCA	PinDBits.Bit2
#define ENCC	PinDBits.Bit1
#define ENCBT	PinDBits.Bit0
#define PROG0	PinDBits.Bit3
#define PROG1	PinDBits.Bit4
#define PROG2	PinDBits.Bit5
//#define PLUS	PinDBits.Bit3
//#define MINUS	PinDBits.Bit4

#define BUTTONS	PIND
#define	INTF	INT0_vect();
/*
#ifdef	atmega8
	#define	INTF	GIFR |= (1 << INTF0)
#else
	#define	INTF	EIFR |= (1 << INTF0)
#endif
*/
#ifdef	_4CH
#define	pwmPlusAD 0x00
#define ironAD	0x40
#define hotAirAD	0x80
#else
#define ironAD	0x00
#define hotAirAD	0x01
#endif

	#define ironTPWM	TRISC2
	#define iron2	RA
	#define fanT	TRISC1

#define	delay_us	_delay_loop_1
#define	delay_ms	_delay_loop_2

/* ------------------------- Minute counter ------------------------------ */

#define MINUTE	0x1D

/* ----------------------------------------------------------------------- */

#else
#ifdef PIC16F887
#define PIC16F88x
#define _PIC40
#include <pic16f887.h>
#endif

#ifdef PIC16F886
#define PIC16F88x
#include "pic16f886.h"
#endif

#ifdef PIC16F884
#define PIC16F88x
#define _PIC40
#define _4K
#include "pic16f884.h"
#endif

#ifdef PIC16F883
#define PIC16F88x
#define _4K
#include "pic16f883.h"
#endif

#ifdef PIC16F877
#include "pic16f877.h"
#define PIC16F87x
#define _PIC40
#endif

#ifdef PIC16F876
#include "pic16f876.h"
#define PIC16F87x
#endif

#ifdef PIC16F874
#define _4K
#define _24C
#include "pic16f874.h"
#define PIC16F87x
#define _PIC40
#endif

#ifdef PIC16F873
#define _4K
#define _24C
#include "pic16f873.h"
#define PIC16F87x
#endif

#ifdef PIC16F877A
#include "pic16f877a.h"
#define PIC16F87xa
#define _PIC40
#endif

#ifdef PIC16F876A
#include "pic16f876a.h"
#define PIC16F87xa
#endif

#ifdef PIC16F874A
#define _4K
#define _24C
#include "pic16f874a.h"
#define PIC16F87xa
#define _PIC40
#endif

#ifdef PIC16F873A
#define _4K
#define _24C
#include "pic16f873a.h"
#define PIC16F87xa
#endif

#ifdef PIC16F946
#include "pic16f946.h"
#define PIC16F91x
#endif

#ifdef PIC16F917
#include "pic16f917.h"
#define PIC16F91x
#endif

#ifdef PIC16F916
#include "pic16f916.h"
#define PIC16F91x
#endif

#ifdef PIC16F914
#define _4K
#include "pic16f914.h"
#define PIC16F91x
#endif

#ifdef PIC16F913
#define _4K
#include "pic16f913.h"
#define PIC16F91x
#endif

#ifdef PIC16F1789
#include "pic16f1789.h"
#define PIC16F178x
#endif

#ifdef PIC16F1788
#include "pic16f1788.h"
#define PIC16F178x
#endif

#ifdef PIC16F1787
#include "pic16f1787.h"
#define PIC16F178x
#endif

#ifdef PIC16F1786
#include "pic16f1786.h"
#define PIC16F178x
#endif

#ifdef PIC16F1784
#define _4K
#include "pic16f1784.h"
#define PIC16F178x
#endif

#ifdef PIC16F1783
#define _4K
#include "pic16f1783.h"
#define PIC16F178x
#endif

#ifdef PIC16F1939
#include "pic16f1939.h"
#define PIC16F193x
#endif

#ifdef PIC16F1938
#include "pic16f1938.h"
#define PIC16F193x
#endif

#ifdef PIC16F1937
#include "pic16f1937.h"
#define PIC16F193x
#endif

#ifdef PIC16F1936
#include "pic16f1936.h"
#define PIC16F193x
#endif

#ifdef PIC16F1936
#include "pic16f1936.h"
#define PIC16F193x
#endif

#ifdef PIC16F819
#include "pic16f819.h"
#define PIC16F81x
#endif

/* PIC16F7xx PIC16F7x not tested */
#ifdef PIC16F707
#define _24C
#define PIC16F70x
#include "pic16f707.h"
#endif

#ifdef PIC16F723
#define _24C
#define _4K
#define _ADC8bit
#define PIC16F72x
#include "pic16f723.h"
#endif

#ifdef PIC16F724
#define _24C
#define _4K
#define _ADC8bit
#define PIC16F72x
#include "pic16f724.h"
#endif

#ifdef PIC16F726
#define _24C
#define _ADC8bit
#define PIC16F72x
#include "pic16f726.h"
#endif

#ifdef PIC16F727
#define _24C
#define _ADC8bit
#define PIC16F72x
#include "pic16f727.h"
#endif

#ifdef PIC16F737
#define _24C
#define _4K
#define PIC16F7x7
#include "pic16f737.h"
#endif

#ifdef PIC16F747
#define _24C
#define _4K
#define PIC16F7x7
#include "pic16f747.h"
#endif

#ifdef PIC16F767
#define _24C
#define PIC16F7x7
#include "pic16f767.h"
#endif

#ifdef PIC16F777
#define _24C
#define PIC16F7x7
#include "pic16f777.h"
#endif

#ifdef PIC16F76
#define _24C
#define _ADC8bit
#define PIC16F7x
#include "pic16f76.h"
#endif

#ifdef PIC16F77
#define _24C
#define _ADC8bit
#define PIC16F7x
#include "pic16f77.h"
#endif

#ifdef _LEGACY
// set output pins for lcd here
	#define rst 	RC6
	#define cs 	RC5
	#define sda 	RD3
	#define sclk 	RD2
	#define led	RC3

	#define ironA	RE1
	#define ironAN	ANS6
	#define ironT	TRISE1
	#define ironAD	0x98

	#define hotAirA	RE0
	#define hotAirAN	ANS5
	#define hotAirT	TRISE0
	#define hotAirAD	0x94
	#define hotAirSwitchAN	ANS2

	#define BUTTONS PORTB
	#define IP	RB0
	#define IM	RB5
	#define HAP	RB2
	#define HAM	RB3
	#define FP	RB1
	#define FM	RB4
	#define ION	RB7
	#define HAON	RB6
// encoder
	#define ENCA	RB0
	#define ENCC	RB1
	#define ENCBT	RB2

		#define iron	RC2
		#define ironTPWM	TRISC2
		#define pwmPlus	RA4
		#define fanT	TRISC1
		#define fan	RC1

#else
	#ifdef PIC16F91x
		#define sda 	RC7
		#define sdaT 	TRISC7
		#define sclk 	RC6
		#define led	RC4
		#define cs 	RC3
		#define rst 	RC2
		#define dc	RA5
	#elif _AQUA
		#define sclk 	RD0
		#define sda 	RD1
		#define sdaT 	TRISD1
		#define cs 		RD2
		#define rst 	RD3
//		#define dc		RD4
		#define led	RD4
	#elif _USART
		#define rst 	RC5
		#define cs 	RC4
		#define sda 	RC7
		#define sdaT 	TRISC7
		#define sclk 	RC6
		#define isclk 	RA4
		#define led	RC3
		#define dc	RA5
		#define dcT	TRISA5
	#elif _SPI
		#define rst 	RC7
		#define cs 	RC6
		#define sda 	RC5
		#define sclk 	RC3
		#define led	RC4
		#define dc	RA5
		#define dcT	TRISA5
	#else
		#define rst 	RC7
		#define cs 	RC6
		#define sda 	RC5
		#define sdaT 	TRISC5
		#define sclk 	RC4
		#define isclk 	RA4
		#define led	RC3
		#define dc	RA5
		#define dcT	TRISA5
	#endif
	

	#ifdef PIC16F91x
		#define iron	RC5
		#define ironTPWM	TRISC5
		#define fanT	TRISD2
		#define fan	RD2
	#else
		#define iron	RC2
		#define ironTPWM	TRISC2
		#define pwmPlus	RA4
		#define fanT	TRISC1
		#define fan	RC1
	#endif
	
	#define ironA	RA0
	#define ironT	TRISA0
	#define ironAD	0x80
	
	#define pwmPlusA	RA3
	#define pwmPlusT	TRISA3
	#ifdef PIC16F88x
		#define pwmPlusAD	0x8C
	#else
		#define pwmPlusAD		0x98
	#endif
	
	#define hotAirA	RA1
	#define hotAirT	TRISA1
	#ifdef PIC16F88x
		#define hotAirAD	0x84
		#define ironAN	ANS0
		#define hotAirAN	ANS1
		#define pwmPlusAN	ANS3
	#else
		#define hotAirAD	0x88
	#endif

	#define BUTTONS PORTB
	#define IP	RB0
	#define IM	RB1
	#define HAP	RB2
	#define HAM	RB3
	#define FP	RB4
	#define FM	RB5
	#define ION	RB6
	#define HAON	RB7
// encoder
	#define ENCA	RB0
	#define ENCC	RB1
	#define ENCBT	RB2
	#define PROG0	RB3
	#define PROG1	RB4
	#define PROG2	RB5
	
	#ifdef _COMPRESSOR
		#ifdef	HAON
			#undef	HAON
			#define	HAON	RA5
			#define HAONT	TRISA5
			#define	HAPORT	PORTA
		#endif
		#define zero	RB7
	#endif
	
#endif

#define hotAir	RC0
#ifdef	_RAV133
	#define hotAirSwitch	RD4
	#define hotAirSwitchT	TRISD4
#else
	#define hotAirSwitch	RA2
	#define hotAirSwitchT	TRISA2
#endif

#ifdef _PIC40
	#define ironSwitch	RD0
	#define ironSwitchT	TRISD0
	#define pwm	RD2
	#define pwmT	TRISD2
	#define PWMPORT PORTD
	#define PWMP	RD5
	#define PWMM	RD6
	#define PWMON	RD7
	#define PWMPPORT PORTE
	#define PWMPP	RE0
	#define PWMPM	RE1
	#define PWMPON	RE2
#else
	#define ironSwitch	RA5
	#define ironSwitchT	TRISA5
	#define pwm	RA4
#endif

#ifdef _KS0108
	#undef pwm
	#define pwm RA6
	#undef _COMPRESSOR
#endif

#ifdef _4CH
	#ifndef _PIC40
		#undef _IRON_SW
	#endif
	#undef _COMPRESSOR
#endif

#ifdef _IRON_SW
	#undef _COMPRESSOR
	#define IRON_SLEEP	0x05
#endif

#ifdef _COMPRESSOR
	#undef _4CH
	#undef _IRON_SW
#endif

#define wdt_reset() \
	__asm__ ("clrwdt")

/* ------------------------- Minute counter ------------------------------ */

#define MINUTE	0x0F

/* ----------------------------------------------------------------------- */

#endif