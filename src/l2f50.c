/*
	SIEMENS S65 LCD
	L2F50
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

volatile unsigned char ch,mch;

const unsigned char disctl[11] = {0x4C, 0x01, 0x53, 0x00, 0x02, 0xB4, 0xB0, 0x02, 0x00, 0x00, 0x00};
/*
const unsigned char gcp64_0[29] =
				  {0x11,0x27,0x3C,0x4C,0x5D,0x6C,0x78,0x84,0x90,0x99,0xA2,0xAA,0xB2,0xBA,
				   0xC0,0xC7,0xCC,0xD2,0xD7,0xDC,0xE0,0xE4,0xE8,0xED,0xF0,0xF4,0xF7,0xFB,
				   0xFE};
const unsigned char gcp64_1[34] =
				 {0x01,0x03,0x06,0x09,0x0B,0x0E,0x10,0x13,0x15,0x17,0x19,0x1C,0x1E,0x20,
				  0x22,0x24,0x26,0x28,0x2A,0x2C,0x2D,0x2F,0x31,0x33,0x35,0x37,0x39,0x3B,
				  0x3D,0x3F,0x42,0x44,0x47,0x5E};
				 */
const unsigned char gcp16[15] =
				  {0x13,0x23,0x2D,0x33,0x38,0x3C,0x40,0x43,0x46,0x48,0x4A,0x4C,0x4E,0x50,0x64};
#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
unsigned char i,j,k,ch;

Lcd_Write(CMD,0xAE); // disable display;
	for (i=0; i<8; i++) {
		SetXY(40,32+8*i,40+96,7);
		for (j=0; j<96; j++) {
			ch = logo_ss[(unsigned int)96*i+j];
			for (k=0; k<8; k++) {
				if (ch & 0x01)
					Lcd_B();
				else
					Lcd_H();
				ch >>= 1;
			}
		}
	}
Lcd_Write(CMD,0xAF); // enable display;
return;
}
#endif

// init LCD
void Lcd_Init(void) {
unsigned char i;
sclk = 0;
sda = 0;
cs = 1;
dc = 1;
delay_s(1);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 0;
delay_s(1);
rst = 1;
delay_s(1);	// 5mS so says the stop watch(less than 5ms will not work)
// --- INIT  0 ---
// -- LCD off --
//cs = 0;

// -- LCD options
Lcd_Write(CMD,0xBC); // Data control
Lcd_Write(DATA,0x2B); // 565 mode, 0x2A=normal, 0x2B=180
//Lcd_Write(DATA,0x04);

//Lcd_Write(DATA,0x00);

Lcd_Write(CMD,0xCA);
for (i=0;i<sizeof(disctl);i++)
Lcd_Write(DATA,disctl[i]);
/*
Lcd_Write(CMD,0xCB);
for (i=0;i<sizeof(gcp64_0);i++) {
Lcd_Write(DATA,gcp64_0[i]);
Lcd_Write(DATA,0x00);
}
for (i=0;i<sizeof(gcp64_1);i++) {
Lcd_Write(DATA,gcp64_1[i]);
Lcd_Write(DATA,0x01);
}
*/
Lcd_Write(CMD,0xCC);
for (i=0;i<sizeof(gcp16);i++) {
Lcd_Write(DATA,gcp16[i]);
}

// GSSET
Lcd_Write(CMD,0xCD);
Lcd_Write(DATA,0x01);

/*
// OSSET
Lcd_Write(CMD,0xD0);
Lcd_Write(DATA,0x00);

Lcd_Write(CMD,0x15);
Lcd_Write(DATA,0x08);
Lcd_Write(DATA,0x01);
Lcd_Write(DATA,0x8B);
Lcd_Write(DATA,0x01);

Lcd_Write(CMD,0x75);
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0x8F);
*/

// ASCSET
Lcd_Write(CMD,0xAA);
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0xAF);
Lcd_Write(DATA,0xAF);
Lcd_Write(DATA,0x03);

// SCSTART
Lcd_Write(CMD,0xAB);
Lcd_Write(DATA,0x01);

//Lcd_Write(CMD,0xA9);
// -- LCD no sleep
Lcd_Write(CMD,0x94);
delay_s(0x01);

/*
for (i=0;i<0xFF;i++) {
EE_Dec(i, 0, 0);
// -- LCD options
Lcd_Write(CMD,0xBC); // Data control
Lcd_Write(DATA,i); // 565 mode, 0x2A=normal, 0x2B=180
delay_s(10);
}
*/
/*
cs = 1;
;
cs = 0;
Lcd_Write(CMD,0x15); // column address set
Lcd_Write(DATA,0x08);
Lcd_Write(DATA,0x01);
Lcd_Write(DATA,0x08+131);
Lcd_Write(DATA,0x01);
Lcd_Write(CMD,0x75); // page address set
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,131);
debug(5);
// -- LCD on --
Lcd_Write(CMD,0xAF);
debug(5);
*/
//cs = 1;
Lcd_Clear(); // clear LCD
// -- LCD on --
Lcd_Write(CMD,0xAF);
delay_s(0x01);
return;
}

void Lcd_Reinit(unsigned char contrast) {
contrast=0;
//Lcd_Write(CMD,0x81); /* CMD   Contrast Lvl & Int. Regul. Resistor Ratio */
//Lcd_Write(DATA,contrast); /* DATA: contrast = 0x29 */
//Lcd_Write(DATA,0x05); /* DATA: 0x05 = 0b101 -> 1+R2/R1 = 11.37 */
return;
}

#ifdef _SPI
	//clear LCD
	void Lcd_Clear(void) {
	unsigned char i,j;
	
	Lcd_Write(CMD,0xAE);
	SetXY(0,0xFF,175,131);
	dc = DATA;
	cs = 0;
	for (i=0;i<242;i++) {
		for (j=0;j<192;j++) {
			SSPBUF = 0xFF;
		}
	}
	SSPIF = 0;
	cs = 1;

	Lcd_Write(CMD,0xAF);
	
	return;
	}
	void Lcd_Write(unsigned char cd, volatile unsigned char c){
		__asm__ ("CLRWDT");
		dc = cd;
		cs = 0;
		SSPBUF = c;
		while (!SSPIF) ;
		SSPIF = 0;
		SSPBUF = 0xFF;
		while (!SSPIF) ;
		SSPIF = 0;
		cs = 1;
	return;
	}
	
	void Lcd_Write_Color(volatile unsigned char ch, unsigned char cl){
		__asm__ ("CLRWDT");
		dc = DATA;
		cs = 0;
		SSPBUF = ch;
		while (!SSPIF) ;
		SSPIF = 0;
		SSPBUF = cl;
		while (!SSPIF) ;
		SSPIF = 0;
		cs = 1;
	return;
	}
#else
	// Buggy
	void Lcd_Write(unsigned char cd, unsigned char c){
	unsigned char li;
		__asm__ ("CLRWDT");
		dc = cd;
		cs = 0;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(c & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			c <<= 1;
		}
		// Dummy write
		sclk=0;
		sda=0;
		c  = 0xFF;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(c & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			c <<= 1;
		}
		sclk=0;
		sda=0;
		cs = 1;
	return;
	}
	
	void Lcd_Write_Color(volatile unsigned char ch, unsigned char cl){
	unsigned char li;
		__asm__ ("CLRWDT");
		dc = DATA;
		cs = 0;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(ch & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			ch <<= 1;
		}
		sclk=0;
		sda=0;
		for(li=0;li<8;li++) {
			sclk = 0;
				if(cl & 0x80)
					sda = 1;
				else
					sda = 0;
			sclk = 1;
			cl <<= 1;
		}
		sclk=0;
		sda=0;
		cs = 1;
	return;
	}

	//clear LCD
	void Lcd_Clear(void) {
	unsigned char i,j,k;
	
	SetXY(0,0xFF,175,131);
	dc = DATA;
	cs = 0;
	sda = 1;
	for (i=0;i<242;i++) {
		for (j=0;j<192;j++) {
			for (k=0;k<16;k++) {
				sclk=0;
				sclk=1;
			}
		}
	}
	cs = 1;
	
	return;
	}
#endif

void Lcd_B(void) {
	Lcd_Write_Color(0x00, 0x1F);
return;
}
void Lcd_S(void) {
	Lcd_Write_Color(0x00, 0x00);
return;
}

void Lcd_H(void) {
	Lcd_Write_Color(0xFF, 0xFF);
return;
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) { // Set buffer
__asm__ ("CLRWDT");
y++;
	Lcd_Write(CMD,0x15);
	Lcd_Write(DATA,0x08+y); // x-address
	Lcd_Write(DATA,0x01);
	Lcd_Write(DATA,0x08+y+dy);
	Lcd_Write(DATA,0x01);

	Lcd_Write(CMD,0x75); // y-address
	Lcd_Write(DATA,x);
	Lcd_Write(DATA,x+dx);
	Lcd_Write(CMD,0x5C);
return;
}