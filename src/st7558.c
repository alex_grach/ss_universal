/*
	MOTOROLA ST7558 LCD
	C113 / C115 / C116 / C117 / C118 / C123	
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

#ifdef	_LOGO
extern const unsigned char logo_ss [];

void Lcd_Logo(void) {
unsigned char i,j;

	SetXY(0,0);
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x40);
	for (i=0; i<8; i++)
		for (j=0; j<96; j++)
			I2CWrite(logo_ss[(unsigned int)96*i+j]);
	I2CStop();
return;
}
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
	//Lcd_Write(CMD,0xAE); // disable display;
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x40); // write data?
	for(i=0;i<12;i++)
		for(j=0;j<72;j++)
	I2CWrite(0x00);
	I2CStop();
	//Lcd_Write(CMD,0xAF); // enable display;
return;
}


// init LCD
void Lcd_Init(void) {
	sclk = 0;
	sda = 0;
	rst = 0;
	delay_us(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
	rst = 1;
	delay_us(0x20);
	
	I2CStart();
	I2CWrite(0x78); // ST7558 Device address
	I2CWrite(0x00); // write command?
	I2CWrite(0x2E); // MX, MY
	I2CWrite(0x21); // Extended instruction set H=1
	I2CWrite(0x12); // Set bais system
	I2CWrite(0xC0); // Set Vop
	I2CWrite(0x0B); // Set booster voltage multiplication
	I2CWrite(0x20); // Basic instruction set H=0
	I2CWrite(0x11); // Set Vlcd range to H
	I2CWrite(0x0C); // Set display control to normal display
	I2CWrite(0x40); // Set Y=0
	I2CWrite(0x80); // Set X=0
	I2CStop();
	
	I2CStart();
	I2CWrite(0x78); // ST7558 Device address & write command
	I2CWrite(0x00); // write command?
	I2CWrite(0x20); // Basic instruction set H=0
	I2CWrite(0x08); // Set Vlcd range to L
	I2CStop();
	
	Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
	I2CStart();
	I2CWrite(0x78); // ST7558 Device address
	I2CWrite(0x00); // write command?
	I2CWrite(0x21); // Extended instruction set H=1
	I2CWrite(contrast); // Set contrast Vop
	I2CStop();

/*	Lcd_Write(CMD,0x24); // write VOP register 
	Lcd_Write(CMD,contrast);
	Lcd_Write(CMD,0xA6^((cfg&0x02)>>1)); // Invert display option
	Lcd_Write(CMD,0xA0^(cfg&0x01)); // Mirror X axis option
	Lcd_Write(CMD,0xC0^(cfg&0x08)); // Mirror Y axis option*/
}

void Lcd_Write(unsigned char cd, unsigned char c){
	__asm__ ("clrwdt");	
	cd=0;
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x40);
	I2CWrite(c);
	I2CStop();
return;
}


void SetXY(unsigned char x, unsigned char y){
	I2CStart();
	I2CWrite(0x78);
	I2CWrite(0x00);
	I2CWrite(0x20);
	I2CWrite(0x80 | x);//column
	I2CWrite(0x40 | y);//row
	I2CStop();
return;
}
