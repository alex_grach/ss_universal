#/bin/sh
mcus=( atmega8 atmega48 atmega88 atmega168 atmega328 )
channels=( _1CH _2CH _4CH )
langs=( _RU _EN )
controls=( _BUTTONS _ENCODER )
lcds=( pcf8814 pcd8544 ssd1828 )
for mcu in ${mcus[@]}
	do
	for lcd in ${lcds[@]}
		do
		for ch in ${channels[@]}
			do
			for lang in ${langs[@]}
				do
				for ctl in ${controls[@]}
					do
					make -e MCU=${mcu} LCDD=${lcd} LANG=${lang} CH=${ch} CONTROL=${ctl} clean ;
					make -e MCU=${mcu} LCDD=${lcd} LANG=${lang} CH=${ch} CONTROL=${ctl} ;
					echo "Building `echo ss_${mcu}${ch}_${lcd}${ctl}${lang}.hex | tr '[:upper:]' '[:lower:]'` finished!"
					mv ss.avr_${mcu}.eep `echo ss.avr_${mcu}_${lcd}${lang}.eep | tr '[:upper:]' '[:lower:]'`
					mv ss.avr_${mcu}.bin `echo ss.avr_${mcu}_${lcd}${lang}.bin | tr '[:upper:]' '[:lower:]'`
					mv ss.avr_${mcu}.hex `echo ss.avr_${mcu}${ch}_${lcd}${ctl}${lang}.hex | tr '[:upper:]' '[:lower:]'`
					done
				done
			done
		done
		echo "Creating archive ..."
		zip -9r ss_${mcu}.zip *.c *.h *.hex *.eep *.php ss.avr_${mcu}* &&
		echo "Done!"
		mv *.zip ../firmware/
		rm *.lst *.cod *.map *.cof *.o *.asm *.adb *.hex *.p *.bin *.eep *.elf *~;
		echo "Cleaned"
	done