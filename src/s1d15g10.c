/*
	NOKIA S1D15G10 LCD
	2600/6100
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//clear LCD
void Lcd_Clear(void) {
unsigned char i,j;

SSPEN=1;
SetXY(0xFF,0xFF,132,132);
cs=0;
//sda=DATA;
	for(i=0;i<198;i++)   
		for(j=0;j<99;j++) {
			SSPBUF = 0xFF;
			/*sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			sclk = 0;
			sclk = 1;
			*/
		}
cs=1;
SSPEN=0;
SSPIF=0;
return;
}

// init LCD
void Lcd_Init(void) {
SSPEN = 0; // Disable SPI
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_s(1);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_s(1);

Lcd_Write(CMD,0xCA);   // display control(EPSON)
Lcd_Write(DATA,0x0C);        // 12 = 1100 - CL dividing ratio [don't divide] switching period 8H (default)
Lcd_Write(DATA,0x20);
Lcd_Write(DATA,0x00);
Lcd_Write(DATA,0x01);

Lcd_Write(CMD,0xBB);   // common scanning direction(EPSON)
Lcd_Write(DATA,0x01);

Lcd_Write(CMD,0xD1);    // internal oscialltor ON(EPSON)

Lcd_Write(CMD,0x94);   // sleep out(EPSON)

Lcd_Write(CMD,0x20);   // power ctrl(EPSON)
Lcd_Write(DATA,0x0F);        //everything on, no external reference resistors

Lcd_Write(CMD,0xA7);   // invert display mode(EPSON)

Lcd_Write(CMD,0xCA);   // data control(EPSON)
Lcd_Write(DATA,0x03);        // correct for normal sin7
Lcd_Write(DATA,0x00);        // normal RGB arrangement
Lcd_Write(DATA,0x01);        // 256 Colors
//Lcd_Write(DATA,0x02);        // 16-bit Grayscale Type A

Lcd_Write(CMD,0x81);   // electronic volume, this is the contrast/brightness(EPSON)
Lcd_Write(DATA,0x24);        // volume (contrast) setting - fine tuning, original
Lcd_Write(DATA,0x03);        // internal resistor ratio - coarse adjustment

Lcd_Write(CMD,0x25);      // nop(EPSON)

delay_s(1);

Lcd_Write(CMD,0xAF);    // display on(EPSON)

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
Lcd_Write(CMD,0x81); /* CMD   Contrast Lvl & Int. Regul. Resistor Ratio */
Lcd_Write(DATA,contrast); /* DATA: contrast = 0x29 */
Lcd_Write(DATA,0x03);        // internal resistor ratio - coarse adjustment
return;
}

void Lcd_Write(unsigned char cd, volatile unsigned char c){
	__asm__ ("clrwdt");	
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = cd;
	sclk = 1;
	SSPEN=1;
	SSPBUF = c;
return;
}

void Lcd_S(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0x00;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}

void Lcd_H(void) {
	cs = 1;
	SSPEN=0;
	cs = 0;
	sclk = 0;
	sda = DATA;
	sclk = 1;
	SSPEN=1;
	SSPBUF = 0xFF;
//	while (!SSPIF) ;
//	SSPIF = 0;
return;
}

void SetXY(unsigned char x, unsigned char y, unsigned char dx, unsigned char dy) { // Set buffer
	x++;
	y++;
	Lcd_Write(CMD,0x15);     // x-address
	Lcd_Write(DATA,x);
	Lcd_Write(DATA,x+dx);
	
	Lcd_Write(CMD,0x75);	// y-address
	Lcd_Write(DATA,y);
	Lcd_Write(DATA,y+dy);
	Lcd_Write(CMD,0x5C); // Write diplay RAM
return;
}