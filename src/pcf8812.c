/*
	SIEMENS PCF8812 LCD
	A52/A55/A56/A57/C55
 	A70/A71
 */
 
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "mcu.h"
#include "config.h"
#endif
#ifndef __LCD_H__
#define __LCD_H__
#include "lcd.h"
#include "logo_ss.h"
#endif
#ifndef __EEPROM_H__
#define __EEPROM_H__
#include "eeprom.h"
#endif
#ifndef __PID_H__
#define __PID_H__
#include "pid.h"
#endif
#ifndef __SS_H__
#define __SS_H__
#include "ss.h"
#endif

//clear LCD
void Lcd_Clear(void){
unsigned char i,j;
Lcd_Write(CMD,0x04); // blank display;
for(i=0;i<8;i++) {
	SetXY(0,i);
	for(j=0;j<96;j++) {
		Lcd_Write(DATA,0x00);
	}
}
Lcd_Write(CMD,0x04^((cfg&0x02)>>1)); // enable display;
return;
}

void Lcd_Logo(void) {
unsigned char i,j;
Lcd_Write(CMD,0x04); // blank display;
	for (i=0; i<12; i++) {
		for (j=0; j<64; j++)
			Lcd_Write(DATA,logo_ss[(unsigned int)64*i+j]);
	}
Lcd_Write(CMD,0x0C^((cfg&0x02)>>1)); // enable display;
return;
}

// init LCD
void Lcd_Init(void) {
sclk = 0;
sda = 0;
cs = 0;
rst = 0;
delay_us(0x20);			// 5mS so says the stop watch(less than 5ms will not work)
rst = 1;
delay_us(0x20);

Lcd_Write(CMD,0x21); // Chip is active, vertical addressing, H = 1 mode
Lcd_Write(CMD,0x11); // Set charge pump range HIGH PRS = 1
Lcd_Write(CMD,EERead(eelcd)); // Set Contrast Level 
Lcd_Write(CMD,0x20^((cfg&0x08)>>2)); // Chip is active, vertical addressing, H = 0 mode, Mirror Y axis option

Lcd_Clear(); // clear LCD
return;
}

void Lcd_Reinit(unsigned char contrast) {
Lcd_Write(CMD,0x21); // Chip is active, vertical addressing, H = 1 mode
Lcd_Write(CMD,contrast); // Set Contrast Level 
Lcd_Write(CMD,0x20^((cfg&0x08)>>2)); // Chip is active, vertical addressing, H = 0 mode, Mirror Y axis option
return;
}

void Lcd_Write(unsigned char cd,unsigned char c){
unsigned char li;
	__asm__ ("clrwdt");	
	cs = 0;
	dc=cd;
	for(li=0;li<8;li++) {
		sclk = 0;
			if(c & 0x80)
				sda = 1;
			else
				sda = 0;
		sclk = 1;
		c <<= 1;
	}
	if (cd==DATA) { // Dummy write
	sclk=0;
	sda=0;
		for(li=0;li<8;li++) {
			sclk = 0;
			sclk = 1;
		}
	}
	sclk=0;
	sda=0;
	cs = 1;
return;
}

void SetXY(unsigned char x, unsigned char y){
	Lcd_Write(CMD,x^0x80);
	Lcd_Write(CMD,y^0x40);
return;
}
