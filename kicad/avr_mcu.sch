EESchema Schematic File Version 2
LIBS:ss-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:microchip_pic16mcu
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:driver
LIBS:stm8
LIBS:ss-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "AVR MCU (atmega8a/88/168/328)"
Date ""
Rev "2.0"
Comp ""
Comment1 ""
Comment2 "gav@bmstu.ru"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +5V #PWR1
U 1 1 573EBD54
P 3700 1300
F 0 "#PWR1" H 3700 1150 50  0001 C CNN
F 1 "+5V" H 3700 1440 50  0000 C CNN
F 2 "" H 3700 1300 50  0000 C CNN
F 3 "" H 3700 1300 50  0000 C CNN
	1    3700 1300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 573EBD92
P 3700 4525
F 0 "#PWR8" H 3700 4275 50  0001 C CNN
F 1 "GND" H 3700 4375 50  0000 C CNN
F 2 "" H 3700 4525 50  0000 C CNN
F 3 "" H 3700 4525 50  0000 C CNN
	1    3700 4525
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X05 P1
U 1 1 574099E5
P 2550 5975
F 0 "P1" H 2550 6275 50  0000 C CNN
F 1 "ICSP" H 2550 5675 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05" H 2550 4775 50  0001 C CNN
F 3 "" H 2550 4775 50  0000 C CNN
	1    2550 5975
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR9
U 1 1 57409E1F
P 2900 6275
F 0 "#PWR9" H 2900 6025 50  0001 C CNN
F 1 "GND" H 2900 6125 50  0000 C CNN
F 2 "" H 2900 6275 50  0000 C CNN
F 3 "" H 2900 6275 50  0000 C CNN
	1    2900 6275
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR7
U 1 1 5740A28F
P 2900 5675
F 0 "#PWR7" H 2900 5525 50  0001 C CNN
F 1 "+5V" H 2900 5815 50  0000 C CNN
F 2 "" H 2900 5675 50  0000 C CNN
F 3 "" H 2900 5675 50  0000 C CNN
	1    2900 5675
	1    0    0    -1  
$EndComp
Entry Wire Line
	1700 5675 1800 5775
Entry Wire Line
	1700 5875 1800 5975
Entry Wire Line
	1700 5975 1800 6075
Entry Wire Line
	1700 6075 1800 6175
Entry Wire Line
	1700 1675 1800 1775
Text Label 1900 1775 0    60   ~ 0
RESET
Text Label 1900 5975 0    60   ~ 0
RESET
Text Label 1900 6075 0    60   ~ 0
PWM
Text Label 1900 6175 0    60   ~ 0
HotAirSW
Text Label 1900 5775 0    60   ~ 0
Fan
$Comp
L R R1
U 1 1 574157A2
P 2200 5500
F 0 "R1" V 2280 5500 50  0000 C CNN
F 1 "10K" V 2200 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2130 5500 50  0001 C CNN
F 3 "" H 2200 5500 50  0000 C CNN
	1    2200 5500
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR3
U 1 1 5741758F
P 2750 2225
F 0 "#PWR3" H 2750 1975 50  0001 C CNN
F 1 "GND" H 2750 2075 50  0000 C CNN
F 2 "" H 2750 2225 50  0000 C CNN
F 3 "" H 2750 2225 50  0000 C CNN
	1    2750 2225
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 57418AC4
P 3225 6900
F 0 "C1" H 3235 6970 50  0000 L CNN
F 1 "0.1" H 3235 6820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3225 6900 50  0001 C CNN
F 3 "" H 3225 6900 50  0000 C CNN
	1    3225 6900
	1    0    0    -1  
$EndComp
$Comp
L L_Small L1
U 1 1 5741924D
P 3500 6400
F 0 "L1" H 3530 6440 50  0000 L CNN
F 1 "10uH" H 3530 6360 50  0000 L CNN
F 2 "Choke_Radial_ThroughHole:Choke_Radial_27mm_2,8mm-Drills_CD_Bobin_Type_No1410311" H 3500 6400 50  0001 C CNN
F 3 "" H 3500 6400 50  0000 C CNN
	1    3500 6400
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 57419A90
P 3500 6900
F 0 "C2" H 3510 6970 50  0000 L CNN
F 1 "0.1" H 3510 6820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3500 6900 50  0001 C CNN
F 3 "" H 3500 6900 50  0000 C CNN
	1    3500 6900
	1    0    0    -1  
$EndComp
NoConn ~ 2300 5875
$Comp
L ATMEGA8A-PU U2
U 1 1 58F3BAC1
P 7800 2900
F 0 "U2" H 7050 4200 50  0000 L BNN
F 1 "ATMEGA8A-PU" H 8300 1450 50  0000 L BNN
F 2 "DIL28" H 7800 2900 50  0001 C CIN
F 3 "" H 7800 2900 50  0001 C CNN
	1    7800 2900
	1    0    0    -1  
$EndComp
Entry Wire Line
	9800 3700 9900 3800
Entry Wire Line
	9800 3800 9900 3900
Entry Wire Line
	9800 3200 9900 3300
Entry Wire Line
	9800 3300 9900 3400
Entry Wire Line
	9800 3400 9900 3500
Entry Wire Line
	9800 3500 9900 3600
Entry Wire Line
	9800 3900 9900 4000
Entry Wire Line
	9800 3600 9900 3700
Text Label 9400 3700 0    60   ~ 0
Iron+
Text Label 9400 3800 0    60   ~ 0
Iron-
Text Label 9400 3200 0    60   ~ 0
HotAir+
Text Label 9400 3400 0    60   ~ 0
Fan+
Text Label 9400 3300 0    60   ~ 0
HotAir-
Text Label 9400 3500 0    60   ~ 0
Fan-
Text Label 9400 3900 0    60   ~ 0
IronOn
Text Label 9400 3600 0    60   ~ 0
HotAirOn
Entry Wire Line
	9800 2100 9900 2200
Entry Wire Line
	9800 2000 9900 2100
Entry Wire Line
	9800 1900 9900 2000
Entry Wire Line
	9800 2300 9900 2400
Entry Wire Line
	9800 3000 9900 3100
Entry Wire Line
	9800 2900 9900 3000
Entry Wire Line
	9800 2800 9900 2900
Entry Wire Line
	9800 2700 9900 2800
Text Label 9700 2000 2    60   ~ 0
HotAir
Text Label 9700 2100 2    60   ~ 0
Fan
Text Label 9700 1900 2    60   ~ 0
Iron
Text Label 9700 2300 2    60   ~ 0
PWM
Text Label 9700 3000 2    60   ~ 0
SCLK
Text Label 9700 2900 2    60   ~ 0
SDA
Text Label 9700 2800 2    60   ~ 0
CS
Text Label 9700 2700 2    60   ~ 0
Rst
Entry Wire Line
	9800 2500 9900 2600
Entry Wire Line
	9800 2600 9900 2700
Entry Wire Line
	9800 2200 9900 2300
Entry Wire Line
	5700 3175 5800 3275
Text Label 9700 2500 2    60   ~ 0
IronAD
Text Label 9700 2600 2    60   ~ 0
HoAirAD
Text Label 9700 2200 2    60   ~ 0
HotAirSw
Entry Wire Line
	5700 3075 5800 3175
Text Label 5600 2475 2    60   ~ 0
PWMPlusAD
Text Label 5600 2575 2    60   ~ 0
PWMPlus
Entry Wire Line
	9800 1800 9900 1900
Text Label 9700 1800 2    60   ~ 0
IronSw
Entry Wire Line
	5900 2500 5800 2400
Entry Wire Line
	5900 2700 5800 2600
Text Label 6000 2700 0    60   ~ 0
D/C
Text Label 6000 2500 0    60   ~ 0
Led
$Comp
L GND #PWR6
U 1 1 58F3FA6F
P 7800 4600
F 0 "#PWR6" H 7800 4350 50  0001 C CNN
F 1 "GND" H 7800 4450 50  0000 C CNN
F 2 "" H 7800 4600 50  0001 C CNN
F 3 "" H 7800 4600 50  0001 C CNN
	1    7800 4600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR2
U 1 1 58F3FC3A
P 7800 1300
F 0 "#PWR2" H 7800 1150 50  0001 C CNN
F 1 "+5V" H 7800 1440 50  0000 C CNN
F 2 "" H 7800 1300 50  0001 C CNN
F 3 "" H 7800 1300 50  0001 C CNN
	1    7800 1300
	1    0    0    -1  
$EndComp
Entry Wire Line
	5800 4050 5900 4150
Entry Wire Line
	5800 4175 5900 4275
Entry Wire Line
	5800 4300 5900 4400
Entry Wire Line
	5800 4425 5900 4525
Entry Wire Line
	5800 4550 5900 4650
Text Label 6000 4150 0    60   ~ 0
D/C
Text Label 6000 4275 0    60   ~ 0
Rst
Text Label 6000 4400 0    60   ~ 0
CS
Text Label 6000 4525 0    60   ~ 0
SDA
Text Label 6000 4650 0    60   ~ 0
SCLK
Entry Wire Line
	5800 3925 5900 4025
Text Label 6000 4025 0    60   ~ 0
Led
Text GLabel 6300 4025 2    60   Output ~ 0
Led
Text GLabel 6300 4150 2    60   Output ~ 0
D/C
Text GLabel 6300 4275 2    60   Output ~ 0
Rst
Text GLabel 6300 4400 2    60   Output ~ 0
CS
Text GLabel 6300 4525 2    60   Output ~ 0
SDA
Text GLabel 6300 4650 2    60   Output ~ 0
SCLK
Text GLabel 6600 5100 2    60   Input ~ 0
IronAD
Text GLabel 6600 4975 2    60   Output ~ 0
Iron
Text GLabel 6600 5225 2    60   Input ~ 0
IronSw
Text GLabel 6600 5475 2    60   Input ~ 0
HotAirAD
Text GLabel 6600 5350 2    60   Output ~ 0
HotAir
Text GLabel 6600 5600 2    60   Input ~ 0
HotAirSw
Text GLabel 6600 5725 2    60   Output ~ 0
Fan
Text GLabel 6600 5850 2    60   Output ~ 0
PWMPlus
Text GLabel 6600 6225 2    60   Input ~ 0
VAD
Text GLabel 6600 6100 2    60   Output ~ 0
PWM
Text GLabel 6600 6350 2    60   Input ~ 0
IAD
Text GLabel 6600 5975 2    60   Input ~ 0
PWMPlusAD
Entry Wire Line
	5800 5000 5900 5100
Entry Wire Line
	5800 5125 5900 5225
Entry Wire Line
	5800 5250 5900 5350
Entry Wire Line
	5800 5375 5900 5475
Entry Wire Line
	5800 5500 5900 5600
Entry Wire Line
	5800 4875 5900 4975
Entry Wire Line
	5800 5625 5900 5725
Entry Wire Line
	5800 5750 5900 5850
Entry Wire Line
	5800 5875 5900 5975
Entry Wire Line
	5800 6000 5900 6100
Entry Wire Line
	5800 6125 5900 6225
Entry Wire Line
	5800 5500 5900 5600
Entry Wire Line
	5800 6250 5900 6350
Text Label 6000 4975 0    60   ~ 0
Iron
Text Label 6000 5100 0    60   ~ 0
IronAD
Text Label 6000 5225 0    60   ~ 0
IronSw
Text Label 6000 5350 0    60   ~ 0
HotAir
Text Label 6000 5475 0    60   ~ 0
HotAirAD
Text Label 6000 5600 0    60   ~ 0
HotAirSw
Text Label 6000 5725 0    60   ~ 0
Fan
Text Label 6000 5850 0    60   ~ 0
PWMPlus
Text Label 6000 5975 0    60   ~ 0
PWMPlusAD
Text Label 6000 6100 0    60   ~ 0
PWM
Text Label 6000 6225 0    60   ~ 0
VAD
Text Label 6000 6350 0    60   ~ 0
IAD
Text GLabel 5000 4900 0    60   Input ~ 0
Iron+
Text GLabel 5000 5025 0    60   Input ~ 0
Iron-
Text GLabel 5000 5150 0    60   Input ~ 0
IronOn
Text GLabel 5000 5275 0    60   Input ~ 0
HotAir+
Text GLabel 5000 5400 0    60   Input ~ 0
HotAir-
Text GLabel 5000 5525 0    60   Input ~ 0
Fan+
Text GLabel 5000 5650 0    60   Input ~ 0
Fan-
Text GLabel 5000 5775 0    60   Input ~ 0
HotAirOn
Entry Wire Line
	5700 4900 5800 5000
Entry Wire Line
	5700 5025 5800 5125
Entry Wire Line
	5700 5150 5800 5250
Entry Wire Line
	5700 5275 5800 5375
Entry Wire Line
	5700 5400 5800 5500
Entry Wire Line
	5700 5525 5800 5625
Entry Wire Line
	5700 5650 5800 5750
Entry Wire Line
	5700 5775 5800 5875
Text Label 5650 5275 2    60   ~ 0
HotAir+
Text Label 5650 4900 2    60   ~ 0
Iron+
Text Label 5650 5025 2    60   ~ 0
Iron-
Text Label 5650 5150 2    60   ~ 0
IronOn
Text Label 5650 5775 2    60   ~ 0
HotAirOn
Text Label 5650 5400 2    60   ~ 0
HotAir+
Text Label 5650 5650 2    60   ~ 0
Fan-
Text Label 5650 5525 2    60   ~ 0
Fan+
Text GLabel 5000 5900 0    60   Input ~ 0
EncA
Text GLabel 5000 6025 0    60   Input ~ 0
EncB
Text GLabel 5000 6150 0    60   Input ~ 0
EncSw
Entry Wire Line
	5700 5900 5800 6000
Entry Wire Line
	5700 6025 5800 6125
Entry Wire Line
	5700 6150 5800 6250
Text Label 5650 5900 2    60   ~ 0
Fan+
Text Label 5650 6025 2    60   ~ 0
HotAir-
Text Label 5650 6150 2    60   ~ 0
HotAir+
Entry Wire Line
	5700 3875 5800 3975
Entry Wire Line
	5700 3975 5800 4075
Entry Wire Line
	5700 3375 5800 3475
Entry Wire Line
	5700 3475 5800 3575
Entry Wire Line
	5700 3575 5800 3675
Entry Wire Line
	5700 3675 5800 3775
Entry Wire Line
	5700 4075 5800 4175
Entry Wire Line
	5700 3775 5800 3875
Text Label 5300 3875 0    60   ~ 0
Iron+
Text Label 5300 3975 0    60   ~ 0
Iron-
Text Label 5300 3375 0    60   ~ 0
HotAir+
Text Label 5300 3575 0    60   ~ 0
Fan+
Text Label 5300 3475 0    60   ~ 0
HotAir-
Text Label 5300 3675 0    60   ~ 0
Fan-
Text Label 5300 4075 0    60   ~ 0
IronOn
Text Label 5300 3775 0    60   ~ 0
HotAirOn
Entry Wire Line
	5700 2075 5800 2175
Entry Wire Line
	5700 1975 5800 2075
Entry Wire Line
	5700 1875 5800 1975
Entry Wire Line
	5700 2275 5800 2375
Entry Wire Line
	5700 2975 5800 3075
Entry Wire Line
	5700 2875 5800 2975
Entry Wire Line
	5700 2775 5800 2875
Entry Wire Line
	5700 2675 5800 2775
Text Label 5600 1975 2    60   ~ 0
HotAir
Text Label 5600 2075 2    60   ~ 0
Fan
Text Label 5600 1875 2    60   ~ 0
Iron
Text Label 5600 2275 2    60   ~ 0
PWM
Text Label 5600 2975 2    60   ~ 0
SCLK
Text Label 5600 2875 2    60   ~ 0
SDA
Text Label 5600 2775 2    60   ~ 0
CS
Text Label 5600 2675 2    60   ~ 0
Rst
Entry Wire Line
	5700 2475 5800 2575
Entry Wire Line
	5700 2575 5800 2675
Entry Wire Line
	5700 2175 5800 2275
Text Label 5600 3075 2    60   ~ 0
IronAD
Text Label 5600 3175 2    60   ~ 0
HoAirAD
Text Label 5600 2175 2    60   ~ 0
HotAirSw
Entry Wire Line
	5700 1775 5800 1875
Text Label 5600 1775 2    60   ~ 0
IronSw
Entry Wire Line
	1800 2475 1700 2375
Entry Wire Line
	1800 2675 1700 2575
Text Label 1900 2675 0    60   ~ 0
D/C
Text Label 1900 2475 0    60   ~ 0
Led
$Comp
L GND #PWR4
U 1 1 58F534B1
P 6825 2250
F 0 "#PWR4" H 6825 2000 50  0001 C CNN
F 1 "GND" H 6825 2100 50  0000 C CNN
F 2 "" H 6825 2250 50  0000 C CNN
F 3 "" H 6825 2250 50  0000 C CNN
	1    6825 2250
	1    0    0    -1  
$EndComp
Text GLabel 2600 1950 0    60   Input ~ 0
AVCC
Text GLabel 2600 2075 0    60   Input ~ 0
AREF
$Comp
L +5V #PWR10
U 1 1 58F540D8
P 3500 6225
F 0 "#PWR10" H 3500 6075 50  0001 C CNN
F 1 "+5V" H 3500 6365 50  0000 C CNN
F 2 "" H 3500 6225 50  0000 C CNN
F 3 "" H 3500 6225 50  0000 C CNN
	1    3500 6225
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR11
U 1 1 58F54567
P 3500 7150
F 0 "#PWR11" H 3500 6900 50  0001 C CNN
F 1 "GND" H 3500 7000 50  0000 C CNN
F 2 "" H 3500 7150 50  0000 C CNN
F 3 "" H 3500 7150 50  0000 C CNN
	1    3500 7150
	1    0    0    -1  
$EndComp
Text GLabel 2850 6600 0    60   Input ~ 0
AVCC
Text GLabel 2850 6725 0    60   Input ~ 0
AREF
Text GLabel 6700 1975 0    60   Input ~ 0
AVCC
Text GLabel 6700 2100 0    60   Input ~ 0
AREF
Wire Wire Line
	2800 5875 2900 5875
Wire Wire Line
	2900 5875 2900 6275
Wire Wire Line
	2800 5975 2900 5975
Connection ~ 2900 5975
Wire Wire Line
	2800 6075 2900 6075
Connection ~ 2900 6075
Wire Wire Line
	2800 6175 2900 6175
Connection ~ 2900 6175
Wire Wire Line
	2800 5775 2900 5775
Wire Wire Line
	2900 5775 2900 5675
Wire Wire Line
	1800 5775 2300 5775
Wire Wire Line
	1800 5975 2300 5975
Wire Wire Line
	1800 6075 2300 6075
Wire Wire Line
	2300 6175 1800 6175
Wire Wire Line
	1800 1775 2800 1775
Wire Wire Line
	2600 2075 2800 2075
Wire Wire Line
	2700 1975 2800 1975
Wire Wire Line
	8800 3600 9800 3600
Wire Wire Line
	8800 3900 9800 3900
Wire Wire Line
	8800 3500 9800 3500
Wire Wire Line
	8800 3400 9800 3400
Wire Wire Line
	8800 3300 9800 3300
Wire Wire Line
	8800 3200 9800 3200
Wire Wire Line
	8800 3800 9800 3800
Wire Wire Line
	8800 3700 9800 3700
Wire Wire Line
	8800 2000 9800 2000
Wire Wire Line
	8800 2100 9800 2100
Wire Wire Line
	8800 1900 9800 1900
Wire Wire Line
	8800 2300 9800 2300
Wire Wire Line
	8800 3000 9800 3000
Wire Wire Line
	8800 2900 9800 2900
Wire Wire Line
	8800 2800 9800 2800
Wire Wire Line
	8800 2700 9800 2700
Wire Wire Line
	8800 2500 9800 2500
Wire Wire Line
	8800 2600 9800 2600
Wire Wire Line
	8800 2200 9800 2200
Wire Wire Line
	4700 3075 5700 3075
Wire Wire Line
	4700 3175 5700 3175
Wire Wire Line
	8800 1800 9800 1800
Wire Wire Line
	6900 2700 5900 2700
Wire Wire Line
	6900 2500 5900 2500
Wire Wire Line
	7800 4400 7800 4600
Wire Wire Line
	7800 1300 7800 1500
Wire Bus Line
	9900 800  9900 5400
Wire Bus Line
	1700 800  9900 800 
Wire Bus Line
	5800 800  5800 7650
Wire Wire Line
	5900 4275 6300 4275
Wire Wire Line
	6300 4400 5900 4400
Wire Wire Line
	5900 4525 6300 4525
Wire Wire Line
	6300 4650 5900 4650
Wire Wire Line
	5900 4150 6300 4150
Wire Wire Line
	6300 4025 5900 4025
Wire Wire Line
	5900 5225 6600 5225
Wire Wire Line
	6600 5350 5900 5350
Wire Wire Line
	5900 5475 6600 5475
Wire Wire Line
	6600 5600 5900 5600
Wire Wire Line
	5900 5100 6600 5100
Wire Wire Line
	6600 4975 5900 4975
Wire Wire Line
	5900 5850 6600 5850
Wire Wire Line
	6600 5975 5900 5975
Wire Wire Line
	5900 6100 6600 6100
Wire Wire Line
	6600 6225 5900 6225
Wire Wire Line
	5900 5725 6600 5725
Wire Wire Line
	6600 6350 5900 6350
Wire Wire Line
	5000 4900 5700 4900
Wire Wire Line
	5000 5025 5700 5025
Wire Wire Line
	5000 5150 5700 5150
Wire Wire Line
	5000 5275 5700 5275
Wire Wire Line
	5000 5400 5700 5400
Wire Wire Line
	5000 5525 5700 5525
Wire Wire Line
	5000 5650 5700 5650
Wire Wire Line
	5000 5775 5700 5775
Wire Wire Line
	5000 6150 5700 6150
Wire Wire Line
	5000 6025 5700 6025
Wire Wire Line
	5000 5900 5700 5900
Wire Wire Line
	4700 3775 5700 3775
Wire Wire Line
	4700 4075 5700 4075
Wire Wire Line
	4700 3675 5700 3675
Wire Wire Line
	4700 3575 5700 3575
Wire Wire Line
	4700 3475 5700 3475
Wire Wire Line
	4700 3375 5700 3375
Wire Wire Line
	4700 3975 5700 3975
Wire Wire Line
	4700 3875 5700 3875
Wire Wire Line
	4700 1975 5700 1975
Wire Wire Line
	4700 2075 5700 2075
Wire Wire Line
	4700 1875 5700 1875
Wire Wire Line
	4700 2275 5700 2275
Wire Wire Line
	4700 2975 5700 2975
Wire Wire Line
	4700 2875 5700 2875
Wire Wire Line
	4700 2775 5700 2775
Wire Wire Line
	4700 2675 5700 2675
Wire Wire Line
	4700 2475 5700 2475
Wire Wire Line
	4700 2575 5700 2575
Wire Wire Line
	4700 2175 5700 2175
Wire Wire Line
	4700 1775 5700 1775
Wire Wire Line
	2800 2675 1800 2675
Wire Wire Line
	2800 2475 1800 2475
Wire Bus Line
	1700 800  1700 7600
Wire Wire Line
	2800 2175 2750 2175
Wire Wire Line
	2750 2175 2750 2225
Wire Wire Line
	6825 2200 6825 2250
Wire Wire Line
	6900 2200 6825 2200
Wire Wire Line
	2600 1950 2700 1950
Wire Wire Line
	2700 1950 2700 1975
Wire Wire Line
	3500 6225 3500 6300
Wire Wire Line
	3500 6500 3500 6800
Wire Wire Line
	2850 6600 3500 6600
Connection ~ 3500 6600
Wire Wire Line
	3500 7000 3500 7150
Wire Wire Line
	3225 6725 3225 6800
Wire Wire Line
	3225 7000 3225 7100
Wire Wire Line
	3225 7100 3500 7100
Connection ~ 3500 7100
Wire Wire Line
	2850 6725 3225 6725
Wire Wire Line
	6700 2100 6900 2100
Wire Wire Line
	6800 2000 6900 2000
Wire Wire Line
	6700 1975 6800 1975
Wire Wire Line
	6800 1975 6800 2000
Wire Wire Line
	2200 5650 2200 5975
Connection ~ 2200 5975
$Comp
L +5V #PWR5
U 1 1 58F57266
P 2200 5275
F 0 "#PWR5" H 2200 5125 50  0001 C CNN
F 1 "+5V" H 2200 5415 50  0000 C CNN
F 2 "" H 2200 5275 50  0000 C CNN
F 3 "" H 2200 5275 50  0000 C CNN
	1    2200 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5275 2200 5350
Wire Wire Line
	3700 1300 3700 1400
Wire Wire Line
	3650 1400 3750 1400
Wire Wire Line
	3650 1400 3650 1475
Wire Wire Line
	3750 1400 3750 1475
Connection ~ 3700 1400
$Comp
L ATMEGA8A-A U1
U 1 1 573EB8E3
P 3700 2775
F 0 "U1" H 2950 3975 50  0000 L BNN
F 1 "ATMEGA8A-A" H 4200 1225 50  0000 L BNN
F 2 "Housings_QFP:TQFP-32_7x7mm_Pitch0.8mm" H 3700 2775 50  0001 C CIN
F 3 "" H 3700 2775 50  0000 C CNN
	1    3700 2775
	1    0    0    -1  
$EndComp
Entry Wire Line
	5800 1700 5900 1800
Text Label 6000 1800 0    60   ~ 0
RESET
Wire Wire Line
	5900 1800 6900 1800
Wire Wire Line
	3650 4375 3650 4450
Wire Wire Line
	3650 4450 3750 4450
Wire Wire Line
	3700 4450 3700 4525
Wire Wire Line
	3750 4450 3750 4375
Connection ~ 3700 4450
Wire Notes Line
	1750 900  1750 4775
Wire Notes Line
	1750 4775 5750 4775
Wire Notes Line
	5750 4775 5750 900 
Wire Notes Line
	5750 900  1750 900 
Text Notes 1800 4725 0    60   ~ 0
_4CH
$EndSCHEMATC
