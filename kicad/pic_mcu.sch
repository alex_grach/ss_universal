EESchema Schematic File Version 2
LIBS:ss-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:microchip_pic16mcu
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:driver
LIBS:stm8
LIBS:ss-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "PIC MCU (pic16f88x/87x(a)/7x) "
Date ""
Rev "2.0"
Comp ""
Comment1 ""
Comment2 "gav@bmstu.ru"
Comment3 ""
Comment4 ""
$EndDescr
Entry Wire Line
	5600 3650 5700 3750
Entry Wire Line
	5600 3550 5700 3650
Entry Wire Line
	5600 3750 5700 3850
Entry Wire Line
	5600 3850 5700 3950
Entry Wire Line
	5600 3950 5700 4050
Entry Wire Line
	5600 4050 5700 4150
Entry Wire Line
	5600 4150 5700 4250
Entry Wire Line
	5600 4250 5700 4350
Entry Wire Line
	5600 1950 5700 2050
Entry Wire Line
	5600 2050 5700 2150
Entry Wire Line
	5600 2150 5700 2250
Entry Wire Line
	5600 2450 5700 2550
Entry Wire Line
	5600 2350 5700 2450
Text Label 5500 2450 2    60   ~ 0
D/C
Text Label 5500 1950 2    60   ~ 0
IronAD
Text Label 5500 2050 2    60   ~ 0
HoAirAD
Text Label 5500 2150 2    60   ~ 0
HotAirSw
Entry Wire Line
	5600 2250 5700 2350
Text Label 5500 2250 2    60   ~ 0
PWMPlusAD
Text Label 5500 2350 2    60   ~ 0
PWMPlus
Text Label 5500 3550 2    60   ~ 0
HotAir
Text Label 5500 3650 2    60   ~ 0
Fan
Text Label 5500 3750 2    60   ~ 0
Iron
Text Label 5500 3850 2    60   ~ 0
Led
Text Label 5500 3950 2    60   ~ 0
SCLK
Text Label 5500 4050 2    60   ~ 0
SDA
Text Label 5500 4150 2    60   ~ 0
CS
Text Label 5500 4250 2    60   ~ 0
Rst
$Comp
L GND #PWR7
U 1 1 58F3FE2D
P 3450 4850
F 0 "#PWR7" H 3450 4600 50  0001 C CNN
F 1 "GND" H 3450 4700 50  0000 C CNN
F 2 "" H 3450 4850 50  0000 C CNN
F 3 "" H 3450 4850 50  0000 C CNN
	1    3450 4850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR2
U 1 1 58F3FE33
P 3450 1500
F 0 "#PWR2" H 3450 1350 50  0001 C CNN
F 1 "+5V" H 3450 1640 50  0000 C CNN
F 2 "" H 3450 1500 50  0000 C CNN
F 3 "" H 3450 1500 50  0000 C CNN
	1    3450 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 58F3FE39
P 2300 5000
F 0 "#PWR8" H 2300 4750 50  0001 C CNN
F 1 "GND" H 2300 4850 50  0000 C CNN
F 2 "" H 2300 5000 50  0000 C CNN
F 3 "" H 2300 5000 50  0000 C CNN
	1    2300 5000
	-1   0    0    -1  
$EndComp
Text Label 1500 1950 0    60   ~ 0
Vpp
$Comp
L R R1
U 1 1 58F3FE5B
P 2300 4300
F 0 "R1" V 2380 4300 50  0000 C CNN
F 1 "10K" V 2300 4300 50  0000 C CNN
F 2 "" V 2230 4300 50  0000 C CNN
F 3 "" H 2300 4300 50  0000 C CNN
	1    2300 4300
	-1   0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 58F3FE62
P 1950 4550
F 0 "R2" V 2030 4550 50  0000 C CNN
F 1 "4K7" V 1950 4550 50  0000 C CNN
F 2 "" V 1880 4550 50  0000 C CNN
F 3 "" H 1950 4550 50  0000 C CNN
	1    1950 4550
	0    -1   1    0   
$EndComp
$Comp
L C C3
U 1 1 58F3FE69
P 2300 4850
F 0 "C3" V 2350 4900 50  0000 L CNN
F 1 "0.1" V 2350 4650 50  0000 L CNN
F 2 "" H 2338 4700 50  0000 C CNN
F 3 "" H 2300 4850 50  0000 C CNN
	1    2300 4850
	1    0    0    1   
$EndComp
$Comp
L PIC16(L)F887-I/P U1
U 1 1 58F3FE70
P 3500 3150
F 0 "U1" H 3750 4650 50  0000 L CNN
F 1 "PIC16(L)F8x7(a)-I/P" H 3750 4550 50  0000 L CNN
F 2 "" H 3500 3150 50  0000 C CIN
F 3 "" H 3500 3150 50  0000 C CNN
	1    3500 3150
	1    0    0    -1  
$EndComp
Text Label 1500 2650 0    60   ~ 0
IronSw
Text Label 1500 3550 0    60   ~ 0
PWMPlus+
Text Label 1500 3650 0    60   ~ 0
PWMPlus-
Text Label 1500 3750 0    60   ~ 0
PWMPlusOn
Text Label 1500 2850 0    60   ~ 0
PWM
Text Label 1500 3350 0    60   ~ 0
PWMOn
Text Label 1500 3250 0    60   ~ 0
PWM-
Text Label 1500 3150 0    60   ~ 0
PWM+
Text Notes 9650 4650 0    60   ~ 0
pic16f87x(a)\npic16f7x
NoConn ~ 2400 2750
NoConn ~ 2400 2950
NoConn ~ 2400 3050
Entry Wire Line
	1300 3450 1400 3550
Entry Wire Line
	1300 3550 1400 3650
Entry Wire Line
	1300 3650 1400 3750
Entry Wire Line
	1300 2550 1400 2650
Entry Wire Line
	1300 1850 1400 1950
Entry Wire Line
	1300 3050 1400 3150
Entry Wire Line
	1300 3150 1400 3250
Entry Wire Line
	1300 3250 1400 3350
Entry Wire Line
	1300 2750 1400 2850
$Comp
L CONN_01X05 P1
U 1 1 58F8B486
P 6650 6850
F 0 "P1" H 6650 7150 50  0000 C CNN
F 1 "ICSP" V 6750 6850 50  0000 C CNN
F 2 "" H 6650 6850 50  0000 C CNN
F 3 "" H 6650 6850 50  0000 C CNN
	1    6650 6850
	1    0    0    -1  
$EndComp
Entry Wire Line
	5700 6550 5800 6650
Entry Wire Line
	5700 6950 5800 7050
Entry Wire Line
	5700 6850 5800 6950
Text Label 5950 6650 0    60   ~ 0
Vpp
$Comp
L +5V #PWR10
U 1 1 58F8B491
P 6350 6500
F 0 "#PWR10" H 6350 6350 50  0001 C CNN
F 1 "+5V" H 6350 6640 50  0000 C CNN
F 2 "" H 6350 6500 50  0000 C CNN
F 3 "" H 6350 6500 50  0000 C CNN
	1    6350 6500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR11
U 1 1 58F8B497
P 6350 7150
F 0 "#PWR11" H 6350 6900 50  0001 C CNN
F 1 "GND" H 6350 7000 50  0000 C CNN
F 2 "" H 6350 7150 50  0000 C CNN
F 3 "" H 6350 7150 50  0000 C CNN
	1    6350 7150
	1    0    0    -1  
$EndComp
Text Label 5950 6950 0    60   ~ 0
HotAirOn
Text Label 5950 7050 0    60   ~ 0
IronOn
Text GLabel 4900 4825 0    60   Input ~ 0
Iron+
Text GLabel 4900 4950 0    60   Input ~ 0
Iron-
Text GLabel 4900 5075 0    60   Input ~ 0
IronOn
Text GLabel 4900 5200 0    60   Input ~ 0
HotAir+
Text GLabel 4900 5325 0    60   Input ~ 0
HotAir-
Text GLabel 4900 5450 0    60   Input ~ 0
Fan+
Text GLabel 4900 5575 0    60   Input ~ 0
Fan-
Text GLabel 4900 5700 0    60   Input ~ 0
HotAirOn
Entry Wire Line
	5600 4825 5700 4925
Entry Wire Line
	5600 4950 5700 5050
Entry Wire Line
	5600 5075 5700 5175
Entry Wire Line
	5600 5200 5700 5300
Entry Wire Line
	5600 5325 5700 5425
Entry Wire Line
	5600 5450 5700 5550
Entry Wire Line
	5600 5575 5700 5675
Entry Wire Line
	5600 5700 5700 5800
Text Label 5550 5200 2    60   ~ 0
HotAir+
Text Label 5550 4825 2    60   ~ 0
Iron+
Text Label 5550 4950 2    60   ~ 0
Iron-
Text Label 5550 5075 2    60   ~ 0
IronOn
Text Label 5550 5700 2    60   ~ 0
HotAirOn
Text Label 5550 5325 2    60   ~ 0
HotAir+
Text Label 5550 5575 2    60   ~ 0
Fan-
Text Label 5550 5450 2    60   ~ 0
Fan+
Text GLabel 4900 5825 0    60   Input ~ 0
PWMPlus+
Text GLabel 4900 5950 0    60   Input ~ 0
PWMPlus-
Text GLabel 4900 6075 0    60   Input ~ 0
PWMPlusOn
Text GLabel 4900 6200 0    60   Input ~ 0
PWM+
Text GLabel 4900 6325 0    60   Input ~ 0
PWM-
Text GLabel 4900 6450 0    60   Input ~ 0
PWMOn
Entry Wire Line
	5600 5825 5700 5925
Entry Wire Line
	5600 5950 5700 6050
Entry Wire Line
	5600 6075 5700 6175
Entry Wire Line
	5600 6200 5700 6300
Entry Wire Line
	5600 6325 5700 6425
Entry Wire Line
	5600 6450 5700 6550
Text Label 5550 5825 2    60   ~ 0
PWMPlus+
Text Label 5550 5950 2    60   ~ 0
PWMPlus-
Text Label 5550 6075 2    60   ~ 0
PWMPlusOn
Text Label 5550 6200 2    60   ~ 0
PWM+
Text Label 5550 6325 2    60   ~ 0
PWM-
Text Label 5550 6450 2    60   ~ 0
PWMOn
$Comp
L PIC16F886-I/P U2
U 1 1 59067C06
P 8100 2750
F 0 "U2" H 8650 3850 50  0000 L CNN
F 1 "PIC16F8x6(a)-I/P" H 8650 3750 50  0000 L CNN
F 2 "" H 8100 2750 50  0001 C CIN
F 3 "" H 8100 2550 50  0001 C CNN
	1    8100 2750
	1    0    0    -1  
$EndComp
Entry Wire Line
	10400 1950 10500 2050
Entry Wire Line
	10400 2050 10500 2150
Entry Wire Line
	10400 2150 10500 2250
Entry Wire Line
	10400 2450 10500 2550
Entry Wire Line
	10400 2350 10500 2450
Text Label 10300 2450 2    60   ~ 0
D/C
Text Label 10300 1950 2    60   ~ 0
IronAD
Text Label 10300 2050 2    60   ~ 0
HoAirAD
Text Label 10300 2150 2    60   ~ 0
HotAirSw
Entry Wire Line
	10400 2250 10500 2350
Text Label 10300 2250 2    60   ~ 0
PWMPlusAD
Text Label 10300 2350 2    60   ~ 0
PWMPlus
Text Label 5900 1950 0    60   ~ 0
Vpp
Entry Wire Line
	5700 1850 5800 1950
Entry Wire Line
	5700 2850 5800 2950
Entry Wire Line
	5700 2750 5800 2850
Entry Wire Line
	5700 2950 5800 3050
Entry Wire Line
	5700 3050 5800 3150
Entry Wire Line
	5700 3150 5800 3250
Entry Wire Line
	5700 3250 5800 3350
Entry Wire Line
	5700 3350 5800 3450
Entry Wire Line
	5700 3450 5800 3550
Text Label 5900 2850 0    60   ~ 0
HotAir
Text Label 5900 2950 0    60   ~ 0
Fan
Text Label 5900 3050 0    60   ~ 0
Iron
Text Label 5900 3150 0    60   ~ 0
Led
Text Label 5900 3250 0    60   ~ 0
SCLK
Text Label 5900 3350 0    60   ~ 0
SDA
Text Label 5900 3450 0    60   ~ 0
CS
Text Label 5900 3550 0    60   ~ 0
Rst
$Comp
L Crystal_Small Y1
U 1 1 59068559
P 9800 4150
F 0 "Y1" V 9750 4000 50  0000 C CNN
F 1 "8MHz" V 9850 3950 50  0000 C CNN
F 2 "" H 9800 4150 50  0000 C CNN
F 3 "" H 9800 4150 50  0000 C CNN
	1    9800 4150
	0    -1   1    0   
$EndComp
$Comp
L C_Small C1
U 1 1 5906855F
P 9600 4050
F 0 "C1" V 9500 4100 50  0000 L CNN
F 1 "22pF" V 9500 3850 50  0000 L CNN
F 2 "" H 9600 4050 50  0000 C CNN
F 3 "" H 9600 4050 50  0000 C CNN
	1    9600 4050
	0    -1   1    0   
$EndComp
$Comp
L C_Small C2
U 1 1 59068565
P 9600 4250
F 0 "C2" V 9700 4300 50  0000 L CNN
F 1 "22pF" V 9700 4050 50  0000 L CNN
F 2 "" H 9600 4250 50  0000 C CNN
F 3 "" H 9600 4250 50  0000 C CNN
	1    9600 4250
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR6
U 1 1 5906856B
P 9350 4400
F 0 "#PWR6" H 9350 4150 50  0001 C CNN
F 1 "GND" V 9350 4200 50  0000 C CNN
F 2 "" H 9350 4400 50  0000 C CNN
F 3 "" H 9350 4400 50  0000 C CNN
	1    9350 4400
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR1
U 1 1 59068C73
P 8100 1450
F 0 "#PWR1" H 8100 1300 50  0001 C CNN
F 1 "+5V" H 8100 1590 50  0000 C CNN
F 2 "" H 8100 1450 50  0000 C CNN
F 3 "" H 8100 1450 50  0000 C CNN
	1    8100 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR4
U 1 1 59068F92
P 8150 4050
F 0 "#PWR4" H 8150 3800 50  0001 C CNN
F 1 "GND" H 8150 3900 50  0000 C CNN
F 2 "" H 8150 4050 50  0000 C CNN
F 3 "" H 8150 4050 50  0000 C CNN
	1    8150 4050
	1    0    0    -1  
$EndComp
Entry Wire Line
	5700 3850 5800 3950
Entry Wire Line
	5700 3975 5800 4075
Entry Wire Line
	5700 4100 5800 4200
Entry Wire Line
	5700 4225 5800 4325
Entry Wire Line
	5700 4350 5800 4450
Text Label 5900 3950 0    60   ~ 0
D/C
Text Label 5900 4075 0    60   ~ 0
Rst
Text Label 5900 4200 0    60   ~ 0
CS
Text Label 5900 4325 0    60   ~ 0
SDA
Text Label 5900 4450 0    60   ~ 0
SCLK
Entry Wire Line
	5700 3725 5800 3825
Text Label 5900 3825 0    60   ~ 0
Led
Text GLabel 6200 3825 2    60   Output ~ 0
Led
Text GLabel 6200 3950 2    60   Output ~ 0
D/C
Text GLabel 6200 4075 2    60   Output ~ 0
Rst
Text GLabel 6200 4200 2    60   Output ~ 0
CS
Text GLabel 6200 4325 2    60   Output ~ 0
SDA
Text GLabel 6200 4450 2    60   Output ~ 0
SCLK
Entry Wire Line
	1300 4450 1400 4550
Text Label 1500 4550 0    60   ~ 0
Vpp
$Comp
L +5V #PWR3
U 1 1 58F43AF5
P 2300 4050
F 0 "#PWR3" H 2300 3900 50  0001 C CNN
F 1 "+5V" H 2300 4190 50  0000 C CNN
F 2 "" H 2300 4050 50  0000 C CNN
F 3 "" H 2300 4050 50  0000 C CNN
	1    2300 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3550 5600 3550
Wire Wire Line
	4600 3650 5600 3650
Wire Wire Line
	4600 3750 5600 3750
Wire Wire Line
	4600 3850 5600 3850
Wire Wire Line
	4600 3950 5600 3950
Wire Wire Line
	4600 4050 5600 4050
Wire Wire Line
	4600 4150 5600 4150
Wire Wire Line
	4600 4250 5600 4250
Wire Wire Line
	4600 1950 5600 1950
Wire Wire Line
	4600 2050 5600 2050
Wire Wire Line
	4600 2150 5600 2150
Wire Wire Line
	4600 2450 5600 2450
Wire Wire Line
	4600 2250 5600 2250
Wire Wire Line
	4600 2350 5600 2350
Wire Wire Line
	3400 4650 3400 4750
Wire Wire Line
	3400 4750 3500 4750
Wire Wire Line
	3450 4750 3450 4850
Wire Wire Line
	3500 4750 3500 4650
Connection ~ 3450 4750
Wire Wire Line
	3450 1500 3450 1550
Wire Wire Line
	1400 1950 2400 1950
Wire Wire Line
	2300 4450 2300 4700
Wire Wire Line
	2300 4550 2100 4550
Connection ~ 2300 4550
Wire Wire Line
	1400 3550 2400 3550
Wire Wire Line
	2400 3650 1400 3650
Wire Wire Line
	1400 3750 2400 3750
Wire Wire Line
	1400 2650 2400 2650
Wire Wire Line
	3400 1550 3500 1550
Wire Wire Line
	3400 1550 3400 1650
Wire Wire Line
	3500 1550 3500 1650
Connection ~ 3450 1550
Wire Wire Line
	1400 3150 2400 3150
Wire Wire Line
	2400 3250 1400 3250
Wire Wire Line
	1400 3350 2400 3350
Wire Wire Line
	1400 2850 2400 2850
Wire Notes Line
	9200 3850 10450 3850
Wire Notes Line
	10450 3850 10450 4750
Wire Notes Line
	10450 4750 9200 4750
Wire Notes Line
	9200 4750 9200 3850
Wire Wire Line
	5800 6650 6450 6650
Wire Wire Line
	6450 6950 5800 6950
Wire Wire Line
	5800 7050 6450 7050
Wire Wire Line
	6350 6500 6350 6750
Wire Wire Line
	6350 6750 6450 6750
Wire Wire Line
	6450 6850 6350 6850
Wire Wire Line
	6350 6850 6350 7150
Wire Wire Line
	4900 4825 5600 4825
Wire Wire Line
	4900 4950 5600 4950
Wire Wire Line
	4900 5075 5600 5075
Wire Wire Line
	4900 5200 5600 5200
Wire Wire Line
	4900 5325 5600 5325
Wire Wire Line
	4900 5450 5600 5450
Wire Wire Line
	4900 5575 5600 5575
Wire Wire Line
	4900 5700 5600 5700
Wire Wire Line
	4900 6450 5600 6450
Wire Wire Line
	4900 6325 5600 6325
Wire Wire Line
	4900 6200 5600 6200
Wire Wire Line
	4900 6075 5600 6075
Wire Wire Line
	4900 5950 5600 5950
Wire Wire Line
	4900 5825 5600 5825
Wire Bus Line
	5700 1200 5700 7100
Wire Bus Line
	1300 1200 10500 1200
Wire Bus Line
	1300 1200 1300 7100
Wire Wire Line
	9400 1950 10400 1950
Wire Wire Line
	9400 2050 10400 2050
Wire Wire Line
	9400 2150 10400 2150
Wire Wire Line
	9400 2450 10400 2450
Wire Wire Line
	9400 2250 10400 2250
Wire Wire Line
	9400 2350 10400 2350
Wire Wire Line
	5800 1950 6800 1950
Wire Wire Line
	6800 2850 5800 2850
Wire Wire Line
	6800 2950 5800 2950
Wire Wire Line
	6800 3050 5800 3050
Wire Wire Line
	6800 3150 5800 3150
Wire Wire Line
	6800 3250 5800 3250
Wire Wire Line
	6800 3350 5800 3350
Wire Wire Line
	6800 3450 5800 3450
Wire Wire Line
	6800 3550 5800 3550
Wire Wire Line
	9700 4250 10400 4250
Wire Wire Line
	9700 4050 10400 4050
Connection ~ 9800 4050
Connection ~ 9800 4250
Wire Wire Line
	8100 1450 8100 1650
Wire Wire Line
	8100 3850 8100 3950
Wire Wire Line
	8100 3950 8200 3950
Wire Wire Line
	8150 3950 8150 4050
Wire Wire Line
	8200 3950 8200 3850
Connection ~ 8150 3950
Wire Wire Line
	5800 4075 6200 4075
Wire Wire Line
	6200 4200 5800 4200
Wire Wire Line
	5800 4325 6200 4325
Wire Wire Line
	6200 4450 5800 4450
Wire Wire Line
	5800 3950 6200 3950
Wire Wire Line
	6200 3825 5800 3825
Wire Wire Line
	1800 4550 1400 4550
Wire Wire Line
	2300 4050 2300 4150
Wire Bus Line
	10500 1200 10500 6200
Entry Wire Line
	10400 2550 10500 2650
Wire Wire Line
	10400 2650 9400 2650
Wire Wire Line
	9400 2550 10400 2550
Text Label 10300 2550 2    60   ~ 0
OSC2
Text Label 10300 2650 2    60   ~ 0
OSC1
Wire Wire Line
	9500 4050 9350 4050
Wire Wire Line
	9350 4050 9350 4400
Wire Wire Line
	9500 4250 9350 4250
Connection ~ 9350 4250
Entry Wire Line
	10400 4050 10500 4150
Entry Wire Line
	10400 4250 10500 4350
Text Label 10350 4250 2    60   ~ 0
OSC1
Text Label 10350 4050 2    60   ~ 0
OSC2
Text GLabel 6500 4900 2    60   Input ~ 0
IronAD
Text GLabel 6500 4775 2    60   Output ~ 0
Iron
Text GLabel 7300 5025 2    60   Input ~ 0
IronSw
Text GLabel 6500 5275 2    60   Input ~ 0
HotAirAD
Text GLabel 6500 5150 2    60   Output ~ 0
HotAir
Text GLabel 7300 5400 2    60   Input ~ 0
HotAirSw
Text GLabel 6500 5525 2    60   Output ~ 0
Fan
Text GLabel 6500 5650 2    60   Output ~ 0
PWMPlus
Text GLabel 6500 6025 2    60   Input ~ 0
VAD
Text GLabel 6500 5900 2    60   Output ~ 0
PWM
Text GLabel 6500 6150 2    60   Input ~ 0
IAD
Text GLabel 6500 5775 2    60   Input ~ 0
PWMPlusAD
Entry Wire Line
	5700 4800 5800 4900
Entry Wire Line
	5700 4925 5800 5025
Entry Wire Line
	5700 5050 5800 5150
Entry Wire Line
	5700 5175 5800 5275
Entry Wire Line
	5700 5300 5800 5400
Entry Wire Line
	5700 4675 5800 4775
Wire Wire Line
	5800 5025 7300 5025
Wire Wire Line
	6500 5150 5800 5150
Wire Wire Line
	5800 5275 6500 5275
Wire Wire Line
	5800 5400 7300 5400
Wire Wire Line
	5800 4900 6500 4900
Wire Wire Line
	6500 4775 5800 4775
Entry Wire Line
	5700 5425 5800 5525
Entry Wire Line
	5700 5550 5800 5650
Entry Wire Line
	5700 5675 5800 5775
Entry Wire Line
	5700 5800 5800 5900
Entry Wire Line
	5700 5925 5800 6025
Entry Wire Line
	5700 5300 5800 5400
Wire Wire Line
	5800 5650 6500 5650
Wire Wire Line
	6500 5775 5800 5775
Wire Wire Line
	5800 5900 6500 5900
Wire Wire Line
	6500 6025 5800 6025
Wire Wire Line
	5800 5525 6500 5525
Entry Wire Line
	5700 6050 5800 6150
Wire Wire Line
	6500 6150 5800 6150
Text Label 5900 4775 0    60   ~ 0
Iron
Text Label 5900 4900 0    60   ~ 0
IronAD
Text Label 5900 5025 0    60   ~ 0
IronSw
Text Label 5900 5150 0    60   ~ 0
HotAir
Text Label 5900 5275 0    60   ~ 0
HotAirAD
Text Label 5900 5400 0    60   ~ 0
HotAirSw
Text Label 5900 5525 0    60   ~ 0
Fan
Text Label 5900 5650 0    60   ~ 0
PWMPlus
Text Label 5900 5775 0    60   ~ 0
PWMPlusAD
Text Label 5900 5900 0    60   ~ 0
PWM
Text Label 5900 6025 0    60   ~ 0
VAD
Text Label 5900 6150 0    60   ~ 0
IAD
Entry Wire Line
	1300 2150 1400 2250
Wire Wire Line
	1400 2250 2400 2250
Wire Wire Line
	2400 2450 1400 2450
Entry Wire Line
	1300 2350 1400 2450
Text Label 1500 2250 0    60   ~ 0
OSC1
Text Label 1500 2450 0    60   ~ 0
OSC2
Entry Wire Line
	10400 2650 10500 2750
Entry Wire Line
	10400 2850 10500 2950
Entry Wire Line
	10400 2950 10500 3050
Entry Wire Line
	10400 3050 10500 3150
Entry Wire Line
	10400 3150 10500 3250
Entry Wire Line
	10400 3250 10500 3350
Entry Wire Line
	10400 3350 10500 3450
Entry Wire Line
	10400 3450 10500 3550
Entry Wire Line
	10400 3550 10500 3650
Text Label 10300 2850 2    60   ~ 0
Iron+
Text Label 10300 2950 2    60   ~ 0
Iron-
Text Label 10300 3050 2    60   ~ 0
HotAir+
Text Label 10300 3250 2    60   ~ 0
Fan+
Text Label 10300 3150 2    60   ~ 0
HotAir-
Text Label 10300 3350 2    60   ~ 0
Fan-
Text Label 10300 3450 2    60   ~ 0
IronOn
Text Label 10300 3550 2    60   ~ 0
HotAirOn
Wire Wire Line
	9400 3550 10400 3550
Wire Wire Line
	9400 3450 10400 3450
Wire Wire Line
	9400 3350 10400 3350
Wire Wire Line
	9400 3250 10400 3250
Wire Wire Line
	9400 3150 10400 3150
Wire Wire Line
	9400 3050 10400 3050
Wire Wire Line
	9400 2950 10400 2950
Wire Wire Line
	9400 2850 10400 2850
$Comp
L R R3
U 1 1 58F3B8B9
P 7000 4650
F 0 "R3" V 7080 4650 50  0000 C CNN
F 1 "10K" V 7000 4650 50  0000 C CNN
F 2 "" V 6930 4650 50  0000 C CNN
F 3 "" H 7000 4650 50  0000 C CNN
	1    7000 4650
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR5
U 1 1 58F3B8C0
P 7100 4350
F 0 "#PWR5" H 7100 4200 50  0001 C CNN
F 1 "+5V" H 7100 4490 50  0000 C CNN
F 2 "" H 7100 4350 50  0000 C CNN
F 3 "" H 7100 4350 50  0000 C CNN
	1    7100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4350 7100 4400
$Comp
L R R4
U 1 1 58F3BC76
P 7200 4650
F 0 "R4" V 7280 4650 50  0000 C CNN
F 1 "10K" V 7200 4650 50  0000 C CNN
F 2 "" V 7130 4650 50  0000 C CNN
F 3 "" H 7200 4650 50  0000 C CNN
	1    7200 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4400 7200 4500
Wire Wire Line
	7000 4400 7200 4400
Connection ~ 7100 4400
Wire Wire Line
	7200 4800 7200 5400
Wire Wire Line
	7000 4800 7000 5025
Entry Wire Line
	5600 2650 5700 2750
Entry Wire Line
	5600 2750 5700 2850
Entry Wire Line
	5600 2850 5700 2950
Entry Wire Line
	5600 2950 5700 3050
Entry Wire Line
	5600 3050 5700 3150
Entry Wire Line
	5600 3150 5700 3250
Entry Wire Line
	5600 3250 5700 3350
Entry Wire Line
	5600 3350 5700 3450
Text Label 5500 2650 2    60   ~ 0
Iron+
Text Label 5500 2750 2    60   ~ 0
Iron-
Text Label 5500 2850 2    60   ~ 0
HotAir+
Text Label 5500 3050 2    60   ~ 0
Fan+
Text Label 5500 2950 2    60   ~ 0
HotAir-
Text Label 5500 3150 2    60   ~ 0
Fan-
Text Label 5500 3250 2    60   ~ 0
IronOn
Text Label 5500 3350 2    60   ~ 0
HotAirOn
Wire Wire Line
	4600 3350 5600 3350
Wire Wire Line
	4600 3250 5600 3250
Wire Wire Line
	4600 3150 5600 3150
Wire Wire Line
	4600 3050 5600 3050
Wire Wire Line
	4600 2950 5600 2950
Wire Wire Line
	4600 2850 5600 2850
Wire Wire Line
	4600 2750 5600 2750
Wire Wire Line
	4600 2650 5600 2650
Connection ~ 7000 5025
Connection ~ 7200 5400
Wire Wire Line
	7000 4500 7000 4400
Entry Wire Line
	1300 6175 1400 6275
Entry Wire Line
	1300 6275 1400 6375
Entry Wire Line
	1300 6375 1400 6475
Entry Wire Line
	1300 6475 1400 6575
Entry Wire Line
	1300 6575 1400 6675
Entry Wire Line
	1300 6675 1400 6775
Text Label 1450 6275 0    60   ~ 0
PWMPlus+
Text Label 1450 6375 0    60   ~ 0
PWMPlus-
Text Label 1450 6475 0    60   ~ 0
PWMPlusOn
Text Label 1450 6575 0    60   ~ 0
PWM+
Text Label 1450 6675 0    60   ~ 0
PWM-
Text Label 1450 6775 0    60   ~ 0
PWMOn
$Comp
L R R5
U 1 1 58F40895
P 2050 6050
F 0 "R5" V 2130 6050 50  0000 C CNN
F 1 "10K" V 2050 6050 50  0000 C CNN
F 2 "" V 1980 6050 50  0000 C CNN
F 3 "" H 2050 6050 50  0000 C CNN
	1    2050 6050
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 58F4089C
P 2250 6050
F 0 "R6" V 2330 6050 50  0000 C CNN
F 1 "10K" V 2250 6050 50  0000 C CNN
F 2 "" V 2180 6050 50  0000 C CNN
F 3 "" H 2250 6050 50  0000 C CNN
	1    2250 6050
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 58F408A3
P 2450 6050
F 0 "R7" V 2530 6050 50  0000 C CNN
F 1 "10K" V 2450 6050 50  0000 C CNN
F 2 "" V 2380 6050 50  0000 C CNN
F 3 "" H 2450 6050 50  0000 C CNN
	1    2450 6050
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 58F408AA
P 2650 6050
F 0 "R8" V 2730 6050 50  0000 C CNN
F 1 "10K" V 2650 6050 50  0000 C CNN
F 2 "" V 2580 6050 50  0000 C CNN
F 3 "" H 2650 6050 50  0000 C CNN
	1    2650 6050
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 58F408B1
P 2850 6050
F 0 "R9" V 2930 6050 50  0000 C CNN
F 1 "10K" V 2850 6050 50  0000 C CNN
F 2 "" V 2780 6050 50  0000 C CNN
F 3 "" H 2850 6050 50  0000 C CNN
	1    2850 6050
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 58F408B8
P 3050 6050
F 0 "R10" V 3130 6050 50  0000 C CNN
F 1 "10K" V 3050 6050 50  0000 C CNN
F 2 "" V 2980 6050 50  0000 C CNN
F 3 "" H 3050 6050 50  0000 C CNN
	1    3050 6050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR9
U 1 1 58F408BF
P 2550 5575
F 0 "#PWR9" H 2550 5425 50  0001 C CNN
F 1 "+5V" H 2550 5715 50  0000 C CNN
F 2 "" H 2550 5575 50  0000 C CNN
F 3 "" H 2550 5575 50  0000 C CNN
	1    2550 5575
	1    0    0    -1  
$EndComp
Entry Wire Line
	1300 6775 1400 6875
$Comp
L R R11
U 1 1 58F408C6
P 3350 6050
F 0 "R11" V 3430 6050 50  0000 C CNN
F 1 "10K" V 3350 6050 50  0000 C CNN
F 2 "" V 3280 6050 50  0000 C CNN
F 3 "" H 3350 6050 50  0000 C CNN
	1    3350 6050
	1    0    0    -1  
$EndComp
Text Label 1450 6875 0    60   ~ 0
PWMPlus
Text Notes 3575 6750 1    60   ~ 0
pic16f87x(a)\npic16f7x
Text Notes 1400 5550 0    60   ~ 0
4 channels only\n_4CH
Text Notes 1425 6075 0    60   ~ 0
14 buttons\n_BUTTONS14
Wire Wire Line
	1400 6275 2050 6275
Wire Wire Line
	1400 6375 2250 6375
Wire Wire Line
	1400 6475 2450 6475
Wire Wire Line
	1400 6575 2650 6575
Wire Wire Line
	1400 6675 2850 6675
Wire Wire Line
	1400 6775 3050 6775
Wire Wire Line
	2050 6275 2050 6200
Wire Wire Line
	2250 6375 2250 6200
Wire Wire Line
	2450 6475 2450 6200
Wire Wire Line
	2650 6575 2650 6200
Wire Wire Line
	2850 6675 2850 6200
Wire Wire Line
	3050 6775 3050 6200
Wire Wire Line
	2550 5675 2550 5575
Wire Wire Line
	2050 5675 3350 5675
Wire Wire Line
	2650 5675 2650 5900
Wire Wire Line
	2850 5675 2850 5900
Connection ~ 2650 5675
Wire Wire Line
	3050 5675 3050 5900
Connection ~ 2850 5675
Wire Wire Line
	2450 5675 2450 5900
Connection ~ 2550 5675
Wire Wire Line
	2250 5675 2250 5900
Connection ~ 2450 5675
Wire Wire Line
	2050 5675 2050 5900
Connection ~ 2250 5675
Wire Wire Line
	3350 5675 3350 5900
Connection ~ 3050 5675
Wire Wire Line
	1400 6875 3350 6875
Wire Wire Line
	3350 6875 3350 6200
Wire Notes Line
	3225 5875 3225 6925
Wire Notes Line
	3225 6925 3600 6925
Wire Notes Line
	3600 6925 3600 5875
Wire Notes Line
	3600 5875 3225 5875
Wire Notes Line
	1350 5325 1350 6975
Wire Notes Line
	1350 6975 3650 6975
Wire Notes Line
	3650 6975 3650 5325
Wire Notes Line
	3650 5325 1350 5325
Wire Notes Line
	3200 5875 1400 5875
Wire Notes Line
	1400 5875 1400 6925
Wire Notes Line
	1400 6925 3200 6925
Wire Notes Line
	3200 6925 3200 5875
Text GLabel 4900 6575 0    60   Input ~ 0
EncA
Text GLabel 4900 6700 0    60   Input ~ 0
EncB
Text GLabel 4900 6825 0    60   Input ~ 0
EncSw
Entry Wire Line
	5600 6575 5700 6675
Entry Wire Line
	5600 6700 5700 6800
Entry Wire Line
	5600 6825 5700 6925
Text Label 5550 6575 2    60   ~ 0
Iron+
Text Label 5550 6700 2    60   ~ 0
Iron-
Text Label 5550 6825 2    60   ~ 0
HotAir+
Wire Wire Line
	4900 6825 5600 6825
Wire Wire Line
	4900 6700 5600 6700
Wire Wire Line
	4900 6575 5600 6575
$EndSCHEMATC
