EESchema Schematic File Version 2
LIBS:ss-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:microchip_pic16mcu
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:driver
LIBS:stm8
LIBS:ss-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "PIC MCU _SPI Lcd (pic16f88x/87x(a)/7x)"
Date ""
Rev "2.0"
Comp ""
Comment1 ""
Comment2 "gav@bmstu.ru"
Comment3 ""
Comment4 ""
$EndDescr
Entry Wire Line
	5450 3550 5550 3650
Entry Wire Line
	5450 3450 5550 3550
Entry Wire Line
	5450 3650 5550 3750
Entry Wire Line
	5450 3750 5550 3850
Entry Wire Line
	5450 3850 5550 3950
Entry Wire Line
	5450 3950 5550 4050
Entry Wire Line
	5450 4050 5550 4150
Entry Wire Line
	5450 4150 5550 4250
Entry Wire Line
	5450 1850 5550 1950
Entry Wire Line
	5450 1950 5550 2050
Entry Wire Line
	5450 2050 5550 2150
Entry Wire Line
	5450 2350 5550 2450
Entry Wire Line
	5450 2250 5550 2350
Text Label 5350 2350 2    60   ~ 0
D/C
Text Label 5350 1850 2    60   ~ 0
IronAD
Text Label 5350 1950 2    60   ~ 0
HoAirAD
Text Label 5350 2050 2    60   ~ 0
HotAirSw
Entry Wire Line
	5450 2150 5550 2250
Text Label 5350 2150 2    60   ~ 0
PWMPlusAD
Text Label 5350 2250 2    60   ~ 0
PWMPlus
Text Label 5350 3450 2    60   ~ 0
HotAir
Text Label 5350 3550 2    60   ~ 0
Fan
Text Label 5350 3650 2    60   ~ 0
Iron
Text Label 5350 3850 2    60   ~ 0
Led
Text Label 5350 3750 2    60   ~ 0
SCLK
Text Label 5350 3950 2    60   ~ 0
SDA
Text Label 5350 4050 2    60   ~ 0
CS
Text Label 5350 4150 2    60   ~ 0
Rst
$Comp
L GND #PWR5
U 1 1 58FCA9F1
P 3300 4750
F 0 "#PWR5" H 3300 4500 50  0001 C CNN
F 1 "GND" H 3300 4600 50  0000 C CNN
F 2 "" H 3300 4750 50  0000 C CNN
F 3 "" H 3300 4750 50  0000 C CNN
	1    3300 4750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR4
U 1 1 58FCA9F7
P 3300 1400
F 0 "#PWR4" H 3300 1250 50  0001 C CNN
F 1 "+5V" H 3300 1540 50  0000 C CNN
F 2 "" H 3300 1400 50  0000 C CNN
F 3 "" H 3300 1400 50  0000 C CNN
	1    3300 1400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 58FCA9FD
P 2150 4900
F 0 "#PWR2" H 2150 4650 50  0001 C CNN
F 1 "GND" H 2150 4750 50  0000 C CNN
F 2 "" H 2150 4900 50  0000 C CNN
F 3 "" H 2150 4900 50  0000 C CNN
	1    2150 4900
	-1   0    0    -1  
$EndComp
Text Label 1350 1850 0    60   ~ 0
Vpp
$Comp
L R R4
U 1 1 58FCAA04
P 2150 4200
F 0 "R4" V 2230 4200 50  0000 C CNN
F 1 "10K" V 2150 4200 50  0000 C CNN
F 2 "" V 2080 4200 50  0000 C CNN
F 3 "" H 2150 4200 50  0000 C CNN
	1    2150 4200
	-1   0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 58FCAA0B
P 1800 4450
F 0 "R1" V 1880 4450 50  0000 C CNN
F 1 "4K7" V 1800 4450 50  0000 C CNN
F 2 "" V 1730 4450 50  0000 C CNN
F 3 "" H 1800 4450 50  0000 C CNN
	1    1800 4450
	0    -1   1    0   
$EndComp
$Comp
L C C1
U 1 1 58FCAA12
P 2150 4750
F 0 "C1" V 2200 4800 50  0000 L CNN
F 1 "0.1" V 2200 4550 50  0000 L CNN
F 2 "" H 2188 4600 50  0000 C CNN
F 3 "" H 2150 4750 50  0000 C CNN
	1    2150 4750
	1    0    0    1   
$EndComp
$Comp
L PIC16(L)F887-I/P U1
U 1 1 58FCAA19
P 3350 3050
F 0 "U1" H 3600 4550 50  0000 L CNN
F 1 "PIC16(L)F8x7(a)-I/P" H 3600 4450 50  0000 L CNN
F 2 "" H 3350 3050 50  0000 C CIN
F 3 "" H 3350 3050 50  0000 C CNN
	1    3350 3050
	1    0    0    -1  
$EndComp
Text Label 1350 2550 0    60   ~ 0
IronSw
Text Label 1350 3450 0    60   ~ 0
PWMPlus+
Text Label 1350 3550 0    60   ~ 0
PWMPlus-
Text Label 1350 3650 0    60   ~ 0
PWMPlusOn
Text Label 1350 2750 0    60   ~ 0
PWM
Text Label 1350 3250 0    60   ~ 0
PWMOn
Text Label 1350 3150 0    60   ~ 0
PWM-
Text Label 1350 3050 0    60   ~ 0
PWM+
Text Notes 9500 4550 0    60   ~ 0
pic16f87x(a)\npic16f7x
NoConn ~ 2250 2650
NoConn ~ 2250 2850
NoConn ~ 2250 2950
Entry Wire Line
	1150 3350 1250 3450
Entry Wire Line
	1150 3450 1250 3550
Entry Wire Line
	1150 3550 1250 3650
Entry Wire Line
	1150 2450 1250 2550
Entry Wire Line
	1150 1750 1250 1850
Entry Wire Line
	1150 2950 1250 3050
Entry Wire Line
	1150 3050 1250 3150
Entry Wire Line
	1150 3150 1250 3250
Entry Wire Line
	1150 2650 1250 2750
$Comp
L CONN_01X05 P1
U 1 1 58FCAA35
P 6500 6750
F 0 "P1" H 6500 7050 50  0000 C CNN
F 1 "ICSP" V 6600 6750 50  0000 C CNN
F 2 "" H 6500 6750 50  0000 C CNN
F 3 "" H 6500 6750 50  0000 C CNN
	1    6500 6750
	1    0    0    -1  
$EndComp
Entry Wire Line
	5550 6450 5650 6550
Entry Wire Line
	5550 6850 5650 6950
Entry Wire Line
	5550 6750 5650 6850
Text Label 5800 6550 0    60   ~ 0
Vpp
$Comp
L +5V #PWR6
U 1 1 58FCAA40
P 6200 6400
F 0 "#PWR6" H 6200 6250 50  0001 C CNN
F 1 "+5V" H 6200 6540 50  0000 C CNN
F 2 "" H 6200 6400 50  0000 C CNN
F 3 "" H 6200 6400 50  0000 C CNN
	1    6200 6400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR7
U 1 1 58FCAA46
P 6200 7050
F 0 "#PWR7" H 6200 6800 50  0001 C CNN
F 1 "GND" H 6200 6900 50  0000 C CNN
F 2 "" H 6200 7050 50  0000 C CNN
F 3 "" H 6200 7050 50  0000 C CNN
	1    6200 7050
	1    0    0    -1  
$EndComp
Text Label 5800 6850 0    60   ~ 0
HotAirOn
Text Label 5800 6950 0    60   ~ 0
IronOn
Text GLabel 4750 4725 0    60   Input ~ 0
Iron+
Text GLabel 4750 4850 0    60   Input ~ 0
Iron-
Text GLabel 4750 4975 0    60   Input ~ 0
IronOn
Text GLabel 4750 5100 0    60   Input ~ 0
HotAir+
Text GLabel 4750 5225 0    60   Input ~ 0
HotAir-
Text GLabel 4750 5350 0    60   Input ~ 0
Fan+
Text GLabel 4750 5475 0    60   Input ~ 0
Fan-
Text GLabel 4750 5600 0    60   Input ~ 0
HotAirOn
Entry Wire Line
	5450 4725 5550 4825
Entry Wire Line
	5450 4850 5550 4950
Entry Wire Line
	5450 4975 5550 5075
Entry Wire Line
	5450 5100 5550 5200
Entry Wire Line
	5450 5225 5550 5325
Entry Wire Line
	5450 5350 5550 5450
Entry Wire Line
	5450 5475 5550 5575
Entry Wire Line
	5450 5600 5550 5700
Text Label 5400 5100 2    60   ~ 0
HotAir+
Text Label 5400 4725 2    60   ~ 0
Iron+
Text Label 5400 4850 2    60   ~ 0
Iron-
Text Label 5400 4975 2    60   ~ 0
IronOn
Text Label 5400 5600 2    60   ~ 0
HotAirOn
Text Label 5400 5225 2    60   ~ 0
HotAir+
Text Label 5400 5475 2    60   ~ 0
Fan-
Text Label 5400 5350 2    60   ~ 0
Fan+
Text GLabel 4750 5725 0    60   Input ~ 0
PWMPlus+
Text GLabel 4750 5850 0    60   Input ~ 0
PWMPlus-
Text GLabel 4750 5975 0    60   Input ~ 0
PWMPlusOn
Text GLabel 4750 6100 0    60   Input ~ 0
PWM+
Text GLabel 4750 6225 0    60   Input ~ 0
PWM-
Text GLabel 4750 6350 0    60   Input ~ 0
PWMOn
Entry Wire Line
	5450 5725 5550 5825
Entry Wire Line
	5450 5850 5550 5950
Entry Wire Line
	5450 5975 5550 6075
Entry Wire Line
	5450 6100 5550 6200
Entry Wire Line
	5450 6225 5550 6325
Entry Wire Line
	5450 6350 5550 6450
Text Label 5400 5725 2    60   ~ 0
PWMPlus+
Text Label 5400 5850 2    60   ~ 0
PWMPlus-
Text Label 5400 5975 2    60   ~ 0
PWMPlusOn
Text Label 5400 6100 2    60   ~ 0
PWM+
Text Label 5400 6225 2    60   ~ 0
PWM-
Text Label 5400 6350 2    60   ~ 0
PWMOn
$Comp
L PIC16F886-I/P U2
U 1 1 58FCAA78
P 7950 2650
F 0 "U2" H 8500 3750 50  0000 L CNN
F 1 "PIC16F8x6(a)-I/P" H 8500 3650 50  0000 L CNN
F 2 "" H 7950 2650 50  0001 C CIN
F 3 "" H 7950 2450 50  0001 C CNN
	1    7950 2650
	1    0    0    -1  
$EndComp
Entry Wire Line
	10250 1850 10350 1950
Entry Wire Line
	10250 1950 10350 2050
Entry Wire Line
	10250 2050 10350 2150
Entry Wire Line
	10250 2350 10350 2450
Entry Wire Line
	10250 2250 10350 2350
Text Label 10150 2350 2    60   ~ 0
D/C
Text Label 10150 1850 2    60   ~ 0
IronAD
Text Label 10150 1950 2    60   ~ 0
HoAirAD
Text Label 10150 2050 2    60   ~ 0
HotAirSw
Entry Wire Line
	10250 2150 10350 2250
Text Label 10150 2150 2    60   ~ 0
PWMPlusAD
Text Label 10150 2250 2    60   ~ 0
PWMPlus
Text Label 5750 1850 0    60   ~ 0
Vpp
Entry Wire Line
	5550 1750 5650 1850
Entry Wire Line
	5550 2750 5650 2850
Entry Wire Line
	5550 2650 5650 2750
Entry Wire Line
	5550 2850 5650 2950
Entry Wire Line
	5550 2950 5650 3050
Entry Wire Line
	5550 3050 5650 3150
Entry Wire Line
	5550 3150 5650 3250
Entry Wire Line
	5550 3250 5650 3350
Entry Wire Line
	5550 3350 5650 3450
Text Label 5750 2750 0    60   ~ 0
HotAir
Text Label 5750 2850 0    60   ~ 0
Fan
Text Label 5750 2950 0    60   ~ 0
Iron
Text Label 5750 3150 0    60   ~ 0
Led
Text Label 5750 3050 0    60   ~ 0
SCLK
Text Label 5750 3250 0    60   ~ 0
SDA
Text Label 5750 3350 0    60   ~ 0
CS
Text Label 5750 3450 0    60   ~ 0
Rst
$Comp
L Crystal_Small Y1
U 1 1 58FCAA9D
P 9650 4050
F 0 "Y1" V 9600 3900 50  0000 C CNN
F 1 "8MHz" V 9700 3850 50  0000 C CNN
F 2 "" H 9650 4050 50  0000 C CNN
F 3 "" H 9650 4050 50  0000 C CNN
	1    9650 4050
	0    -1   1    0   
$EndComp
$Comp
L C_Small C2
U 1 1 58FCAAA4
P 9450 3950
F 0 "C2" V 9350 4000 50  0000 L CNN
F 1 "22pF" V 9350 3750 50  0000 L CNN
F 2 "" H 9450 3950 50  0000 C CNN
F 3 "" H 9450 3950 50  0000 C CNN
	1    9450 3950
	0    -1   1    0   
$EndComp
$Comp
L C_Small C3
U 1 1 58FCAAAB
P 9450 4150
F 0 "C3" V 9550 4200 50  0000 L CNN
F 1 "22pF" V 9550 3950 50  0000 L CNN
F 2 "" H 9450 4150 50  0000 C CNN
F 3 "" H 9450 4150 50  0000 C CNN
	1    9450 4150
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR11
U 1 1 58FCAAB2
P 9200 4300
F 0 "#PWR11" H 9200 4050 50  0001 C CNN
F 1 "GND" V 9200 4100 50  0000 C CNN
F 2 "" H 9200 4300 50  0000 C CNN
F 3 "" H 9200 4300 50  0000 C CNN
	1    9200 4300
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR9
U 1 1 58FCAAB8
P 7950 1350
F 0 "#PWR9" H 7950 1200 50  0001 C CNN
F 1 "+5V" H 7950 1490 50  0000 C CNN
F 2 "" H 7950 1350 50  0000 C CNN
F 3 "" H 7950 1350 50  0000 C CNN
	1    7950 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR10
U 1 1 58FCAABE
P 8000 3950
F 0 "#PWR10" H 8000 3700 50  0001 C CNN
F 1 "GND" H 8000 3800 50  0000 C CNN
F 2 "" H 8000 3950 50  0000 C CNN
F 3 "" H 8000 3950 50  0000 C CNN
	1    8000 3950
	1    0    0    -1  
$EndComp
Entry Wire Line
	5550 3750 5650 3850
Entry Wire Line
	5550 3875 5650 3975
Entry Wire Line
	5550 4000 5650 4100
Entry Wire Line
	5550 4125 5650 4225
Entry Wire Line
	5550 4250 5650 4350
Text Label 5750 3850 0    60   ~ 0
D/C
Text Label 5750 3975 0    60   ~ 0
Rst
Text Label 5750 4100 0    60   ~ 0
CS
Text Label 5750 4225 0    60   ~ 0
SDA
Text Label 5750 4350 0    60   ~ 0
SCLK
Entry Wire Line
	5550 3625 5650 3725
Text Label 5750 3725 0    60   ~ 0
Led
Text GLabel 6050 3725 2    60   Output ~ 0
Led
Text GLabel 6050 3850 2    60   Output ~ 0
D/C
Text GLabel 6050 3975 2    60   Output ~ 0
Rst
Text GLabel 6050 4100 2    60   Output ~ 0
CS
Text GLabel 6050 4225 2    60   Output ~ 0
SDA
Text GLabel 6050 4350 2    60   Output ~ 0
SCLK
Entry Wire Line
	1150 4350 1250 4450
Text Label 1350 4450 0    60   ~ 0
Vpp
$Comp
L +5V #PWR1
U 1 1 58FCAAD8
P 2150 3950
F 0 "#PWR1" H 2150 3800 50  0001 C CNN
F 1 "+5V" H 2150 4090 50  0000 C CNN
F 2 "" H 2150 3950 50  0000 C CNN
F 3 "" H 2150 3950 50  0000 C CNN
	1    2150 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3450 5450 3450
Wire Wire Line
	4450 3550 5450 3550
Wire Wire Line
	4450 3650 5450 3650
Wire Wire Line
	4450 3750 5450 3750
Wire Wire Line
	4450 3850 5450 3850
Wire Wire Line
	4450 3950 5450 3950
Wire Wire Line
	4450 4050 5450 4050
Wire Wire Line
	4450 4150 5450 4150
Wire Wire Line
	4450 1850 5450 1850
Wire Wire Line
	4450 1950 5450 1950
Wire Wire Line
	4450 2050 5450 2050
Wire Wire Line
	4450 2350 5450 2350
Wire Wire Line
	4450 2150 5450 2150
Wire Wire Line
	4450 2250 5450 2250
Wire Wire Line
	3250 4550 3250 4650
Wire Wire Line
	3250 4650 3350 4650
Wire Wire Line
	3300 4650 3300 4750
Wire Wire Line
	3350 4650 3350 4550
Connection ~ 3300 4650
Wire Wire Line
	3300 1400 3300 1450
Wire Wire Line
	1250 1850 2250 1850
Wire Wire Line
	2150 4350 2150 4600
Wire Wire Line
	2150 4450 1950 4450
Connection ~ 2150 4450
Wire Wire Line
	1250 3450 2250 3450
Wire Wire Line
	2250 3550 1250 3550
Wire Wire Line
	1250 3650 2250 3650
Wire Wire Line
	1250 2550 2250 2550
Wire Wire Line
	3250 1450 3350 1450
Wire Wire Line
	3250 1450 3250 1550
Wire Wire Line
	3350 1450 3350 1550
Connection ~ 3300 1450
Wire Wire Line
	1250 3050 2250 3050
Wire Wire Line
	2250 3150 1250 3150
Wire Wire Line
	1250 3250 2250 3250
Wire Wire Line
	1250 2750 2250 2750
Wire Notes Line
	9050 3750 10300 3750
Wire Notes Line
	10300 3750 10300 4650
Wire Notes Line
	10300 4650 9050 4650
Wire Notes Line
	9050 4650 9050 3750
Wire Wire Line
	5650 6550 6300 6550
Wire Wire Line
	6300 6850 5650 6850
Wire Wire Line
	5650 6950 6300 6950
Wire Wire Line
	6200 6400 6200 6650
Wire Wire Line
	6200 6650 6300 6650
Wire Wire Line
	6300 6750 6200 6750
Wire Wire Line
	6200 6750 6200 7050
Wire Wire Line
	4750 4725 5450 4725
Wire Wire Line
	4750 4850 5450 4850
Wire Wire Line
	4750 4975 5450 4975
Wire Wire Line
	4750 5100 5450 5100
Wire Wire Line
	4750 5225 5450 5225
Wire Wire Line
	4750 5350 5450 5350
Wire Wire Line
	4750 5475 5450 5475
Wire Wire Line
	4750 5600 5450 5600
Wire Wire Line
	4750 6350 5450 6350
Wire Wire Line
	4750 6225 5450 6225
Wire Wire Line
	4750 6100 5450 6100
Wire Wire Line
	4750 5975 5450 5975
Wire Wire Line
	4750 5850 5450 5850
Wire Wire Line
	4750 5725 5450 5725
Wire Bus Line
	5550 1100 5550 7000
Wire Bus Line
	1150 1100 10350 1100
Wire Bus Line
	1150 1100 1150 7000
Wire Wire Line
	9250 1850 10250 1850
Wire Wire Line
	9250 1950 10250 1950
Wire Wire Line
	9250 2050 10250 2050
Wire Wire Line
	9250 2350 10250 2350
Wire Wire Line
	9250 2150 10250 2150
Wire Wire Line
	9250 2250 10250 2250
Wire Wire Line
	5650 1850 6650 1850
Wire Wire Line
	6650 2750 5650 2750
Wire Wire Line
	6650 2850 5650 2850
Wire Wire Line
	6650 2950 5650 2950
Wire Wire Line
	6650 3050 5650 3050
Wire Wire Line
	6650 3150 5650 3150
Wire Wire Line
	6650 3250 5650 3250
Wire Wire Line
	6650 3350 5650 3350
Wire Wire Line
	6650 3450 5650 3450
Wire Wire Line
	9550 4150 10250 4150
Wire Wire Line
	9550 3950 10250 3950
Connection ~ 9650 3950
Connection ~ 9650 4150
Wire Wire Line
	7950 1350 7950 1550
Wire Wire Line
	7950 3750 7950 3850
Wire Wire Line
	7950 3850 8050 3850
Wire Wire Line
	8000 3850 8000 3950
Wire Wire Line
	8050 3850 8050 3750
Connection ~ 8000 3850
Wire Wire Line
	5650 3975 6050 3975
Wire Wire Line
	6050 4100 5650 4100
Wire Wire Line
	5650 4225 6050 4225
Wire Wire Line
	6050 4350 5650 4350
Wire Wire Line
	5650 3850 6050 3850
Wire Wire Line
	6050 3725 5650 3725
Wire Wire Line
	1650 4450 1250 4450
Wire Wire Line
	2150 3950 2150 4050
Wire Bus Line
	10350 1100 10350 6100
Entry Wire Line
	10250 2450 10350 2550
Wire Wire Line
	10250 2550 9250 2550
Wire Wire Line
	9250 2450 10250 2450
Text Label 10150 2450 2    60   ~ 0
OSC2
Text Label 10150 2550 2    60   ~ 0
OSC1
Wire Wire Line
	9350 3950 9200 3950
Wire Wire Line
	9200 3950 9200 4300
Wire Wire Line
	9350 4150 9200 4150
Connection ~ 9200 4150
Entry Wire Line
	10250 3950 10350 4050
Entry Wire Line
	10250 4150 10350 4250
Text Label 10200 4150 2    60   ~ 0
OSC1
Text Label 10200 3950 2    60   ~ 0
OSC2
Text GLabel 6350 4800 2    60   Input ~ 0
IronAD
Text GLabel 6350 4675 2    60   Output ~ 0
Iron
Text GLabel 7150 4925 2    60   Input ~ 0
IronSw
Text GLabel 6350 5175 2    60   Input ~ 0
HotAirAD
Text GLabel 6350 5050 2    60   Output ~ 0
HotAir
Text GLabel 7150 5300 2    60   Input ~ 0
HotAirSw
Text GLabel 6350 5425 2    60   Output ~ 0
Fan
Text GLabel 6350 5550 2    60   Output ~ 0
PWMPlus
Text GLabel 6350 5925 2    60   Input ~ 0
VAD
Text GLabel 6350 5800 2    60   Output ~ 0
PWM
Text GLabel 6350 6050 2    60   Input ~ 0
IAD
Text GLabel 6350 5675 2    60   Input ~ 0
PWMPlusAD
Entry Wire Line
	5550 4700 5650 4800
Entry Wire Line
	5550 4825 5650 4925
Entry Wire Line
	5550 4950 5650 5050
Entry Wire Line
	5550 5075 5650 5175
Entry Wire Line
	5550 5200 5650 5300
Entry Wire Line
	5550 4575 5650 4675
Wire Wire Line
	5650 4925 7150 4925
Wire Wire Line
	6350 5050 5650 5050
Wire Wire Line
	5650 5175 6350 5175
Wire Wire Line
	5650 5300 7150 5300
Wire Wire Line
	5650 4800 6350 4800
Wire Wire Line
	6350 4675 5650 4675
Entry Wire Line
	5550 5325 5650 5425
Entry Wire Line
	5550 5450 5650 5550
Entry Wire Line
	5550 5575 5650 5675
Entry Wire Line
	5550 5700 5650 5800
Entry Wire Line
	5550 5825 5650 5925
Entry Wire Line
	5550 5200 5650 5300
Wire Wire Line
	5650 5550 6350 5550
Wire Wire Line
	6350 5675 5650 5675
Wire Wire Line
	5650 5800 6350 5800
Wire Wire Line
	6350 5925 5650 5925
Wire Wire Line
	5650 5425 6350 5425
Entry Wire Line
	5550 5950 5650 6050
Wire Wire Line
	6350 6050 5650 6050
Text Label 5750 4675 0    60   ~ 0
Iron
Text Label 5750 4800 0    60   ~ 0
IronAD
Text Label 5750 4925 0    60   ~ 0
IronSw
Text Label 5750 5050 0    60   ~ 0
HotAir
Text Label 5750 5175 0    60   ~ 0
HotAirAD
Text Label 5750 5300 0    60   ~ 0
HotAirSw
Text Label 5750 5425 0    60   ~ 0
Fan
Text Label 5750 5550 0    60   ~ 0
PWMPlus
Text Label 5750 5675 0    60   ~ 0
PWMPlusAD
Text Label 5750 5800 0    60   ~ 0
PWM
Text Label 5750 5925 0    60   ~ 0
VAD
Text Label 5750 6050 0    60   ~ 0
IAD
Entry Wire Line
	1150 2050 1250 2150
Wire Wire Line
	1250 2150 2250 2150
Wire Wire Line
	2250 2350 1250 2350
Entry Wire Line
	1150 2250 1250 2350
Text Label 1350 2150 0    60   ~ 0
OSC1
Text Label 1350 2350 0    60   ~ 0
OSC2
Entry Wire Line
	10250 2550 10350 2650
Entry Wire Line
	10250 2750 10350 2850
Entry Wire Line
	10250 2850 10350 2950
Entry Wire Line
	10250 2950 10350 3050
Entry Wire Line
	10250 3050 10350 3150
Entry Wire Line
	10250 3150 10350 3250
Entry Wire Line
	10250 3250 10350 3350
Entry Wire Line
	10250 3350 10350 3450
Entry Wire Line
	10250 3450 10350 3550
Text Label 10150 2750 2    60   ~ 0
Iron+
Text Label 10150 2850 2    60   ~ 0
Iron-
Text Label 10150 2950 2    60   ~ 0
HotAir+
Text Label 10150 3150 2    60   ~ 0
Fan+
Text Label 10150 3050 2    60   ~ 0
HotAir-
Text Label 10150 3250 2    60   ~ 0
Fan-
Text Label 10150 3350 2    60   ~ 0
IronOn
Text Label 10150 3450 2    60   ~ 0
HotAirOn
Wire Wire Line
	9250 3450 10250 3450
Wire Wire Line
	9250 3350 10250 3350
Wire Wire Line
	9250 3250 10250 3250
Wire Wire Line
	9250 3150 10250 3150
Wire Wire Line
	9250 3050 10250 3050
Wire Wire Line
	9250 2950 10250 2950
Wire Wire Line
	9250 2850 10250 2850
Wire Wire Line
	9250 2750 10250 2750
$Comp
L R R10
U 1 1 58FCAB9D
P 6850 4550
F 0 "R10" V 6930 4550 50  0000 C CNN
F 1 "10K" V 6850 4550 50  0000 C CNN
F 2 "" V 6780 4550 50  0000 C CNN
F 3 "" H 6850 4550 50  0000 C CNN
	1    6850 4550
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR8
U 1 1 58FCABA4
P 6950 4250
F 0 "#PWR8" H 6950 4100 50  0001 C CNN
F 1 "+5V" H 6950 4390 50  0000 C CNN
F 2 "" H 6950 4250 50  0000 C CNN
F 3 "" H 6950 4250 50  0000 C CNN
	1    6950 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4250 6950 4300
$Comp
L R R11
U 1 1 58FCABAB
P 7050 4550
F 0 "R11" V 7130 4550 50  0000 C CNN
F 1 "10K" V 7050 4550 50  0000 C CNN
F 2 "" V 6980 4550 50  0000 C CNN
F 3 "" H 7050 4550 50  0000 C CNN
	1    7050 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4300 7050 4400
Wire Wire Line
	6850 4300 7050 4300
Connection ~ 6950 4300
Wire Wire Line
	7050 4700 7050 5300
Wire Wire Line
	6850 4700 6850 4925
Entry Wire Line
	5450 2550 5550 2650
Entry Wire Line
	5450 2650 5550 2750
Entry Wire Line
	5450 2750 5550 2850
Entry Wire Line
	5450 2850 5550 2950
Entry Wire Line
	5450 2950 5550 3050
Entry Wire Line
	5450 3050 5550 3150
Entry Wire Line
	5450 3150 5550 3250
Entry Wire Line
	5450 3250 5550 3350
Text Label 5350 2550 2    60   ~ 0
Iron+
Text Label 5350 2650 2    60   ~ 0
Iron-
Text Label 5350 2750 2    60   ~ 0
HotAir+
Text Label 5350 2950 2    60   ~ 0
Fan+
Text Label 5350 2850 2    60   ~ 0
HotAir-
Text Label 5350 3050 2    60   ~ 0
Fan-
Text Label 5350 3150 2    60   ~ 0
IronOn
Text Label 5350 3250 2    60   ~ 0
HotAirOn
Wire Wire Line
	4450 3250 5450 3250
Wire Wire Line
	4450 3150 5450 3150
Wire Wire Line
	4450 3050 5450 3050
Wire Wire Line
	4450 2950 5450 2950
Wire Wire Line
	4450 2850 5450 2850
Wire Wire Line
	4450 2750 5450 2750
Wire Wire Line
	4450 2650 5450 2650
Wire Wire Line
	4450 2550 5450 2550
Connection ~ 6850 4925
Connection ~ 7050 5300
Wire Wire Line
	6850 4400 6850 4300
Entry Wire Line
	1150 6075 1250 6175
Entry Wire Line
	1150 6175 1250 6275
Entry Wire Line
	1150 6275 1250 6375
Entry Wire Line
	1150 6375 1250 6475
Entry Wire Line
	1150 6475 1250 6575
Entry Wire Line
	1150 6575 1250 6675
Text Label 1300 6175 0    60   ~ 0
PWMPlus+
Text Label 1300 6275 0    60   ~ 0
PWMPlus-
Text Label 1300 6375 0    60   ~ 0
PWMPlusOn
Text Label 1300 6475 0    60   ~ 0
PWM+
Text Label 1300 6575 0    60   ~ 0
PWM-
Text Label 1300 6675 0    60   ~ 0
PWMOn
$Comp
L R R2
U 1 1 58FCABDE
P 1900 5950
F 0 "R2" V 1980 5950 50  0000 C CNN
F 1 "10K" V 1900 5950 50  0000 C CNN
F 2 "" V 1830 5950 50  0000 C CNN
F 3 "" H 1900 5950 50  0000 C CNN
	1    1900 5950
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 58FCABE5
P 2100 5950
F 0 "R3" V 2180 5950 50  0000 C CNN
F 1 "10K" V 2100 5950 50  0000 C CNN
F 2 "" V 2030 5950 50  0000 C CNN
F 3 "" H 2100 5950 50  0000 C CNN
	1    2100 5950
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 58FCABEC
P 2300 5950
F 0 "R5" V 2380 5950 50  0000 C CNN
F 1 "10K" V 2300 5950 50  0000 C CNN
F 2 "" V 2230 5950 50  0000 C CNN
F 3 "" H 2300 5950 50  0000 C CNN
	1    2300 5950
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 58FCABF3
P 2500 5950
F 0 "R6" V 2580 5950 50  0000 C CNN
F 1 "10K" V 2500 5950 50  0000 C CNN
F 2 "" V 2430 5950 50  0000 C CNN
F 3 "" H 2500 5950 50  0000 C CNN
	1    2500 5950
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 58FCABFA
P 2700 5950
F 0 "R7" V 2780 5950 50  0000 C CNN
F 1 "10K" V 2700 5950 50  0000 C CNN
F 2 "" V 2630 5950 50  0000 C CNN
F 3 "" H 2700 5950 50  0000 C CNN
	1    2700 5950
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 58FCAC01
P 2900 5950
F 0 "R8" V 2980 5950 50  0000 C CNN
F 1 "10K" V 2900 5950 50  0000 C CNN
F 2 "" V 2830 5950 50  0000 C CNN
F 3 "" H 2900 5950 50  0000 C CNN
	1    2900 5950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR3
U 1 1 58FCAC08
P 2400 5475
F 0 "#PWR3" H 2400 5325 50  0001 C CNN
F 1 "+5V" H 2400 5615 50  0000 C CNN
F 2 "" H 2400 5475 50  0000 C CNN
F 3 "" H 2400 5475 50  0000 C CNN
	1    2400 5475
	1    0    0    -1  
$EndComp
Entry Wire Line
	1150 6675 1250 6775
$Comp
L R R9
U 1 1 58FCAC0F
P 3200 5950
F 0 "R9" V 3280 5950 50  0000 C CNN
F 1 "10K" V 3200 5950 50  0000 C CNN
F 2 "" V 3130 5950 50  0000 C CNN
F 3 "" H 3200 5950 50  0000 C CNN
	1    3200 5950
	1    0    0    -1  
$EndComp
Text Label 1300 6775 0    60   ~ 0
PWMPlus
Text Notes 3425 6650 1    60   ~ 0
pic16f87x(a)\npic16f7x
Text Notes 1250 5450 0    60   ~ 0
4 channels only\n_4CH
Text Notes 1275 5975 0    60   ~ 0
14 buttons\n_BUTTONS14
Wire Wire Line
	1250 6175 1900 6175
Wire Wire Line
	1250 6275 2100 6275
Wire Wire Line
	1250 6375 2300 6375
Wire Wire Line
	1250 6475 2500 6475
Wire Wire Line
	1250 6575 2700 6575
Wire Wire Line
	1250 6675 2900 6675
Wire Wire Line
	1900 6175 1900 6100
Wire Wire Line
	2100 6275 2100 6100
Wire Wire Line
	2300 6375 2300 6100
Wire Wire Line
	2500 6475 2500 6100
Wire Wire Line
	2700 6575 2700 6100
Wire Wire Line
	2900 6675 2900 6100
Wire Wire Line
	2400 5575 2400 5475
Wire Wire Line
	1900 5575 3200 5575
Wire Wire Line
	2500 5575 2500 5800
Wire Wire Line
	2700 5575 2700 5800
Connection ~ 2500 5575
Wire Wire Line
	2900 5575 2900 5800
Connection ~ 2700 5575
Wire Wire Line
	2300 5575 2300 5800
Connection ~ 2400 5575
Wire Wire Line
	2100 5575 2100 5800
Connection ~ 2300 5575
Wire Wire Line
	1900 5575 1900 5800
Connection ~ 2100 5575
Wire Wire Line
	3200 5575 3200 5800
Connection ~ 2900 5575
Wire Wire Line
	1250 6775 3200 6775
Wire Wire Line
	3200 6775 3200 6100
Wire Notes Line
	3075 5775 3075 6825
Wire Notes Line
	3075 6825 3450 6825
Wire Notes Line
	3450 6825 3450 5775
Wire Notes Line
	3450 5775 3075 5775
Wire Notes Line
	1200 5225 1200 6875
Wire Notes Line
	1200 6875 3500 6875
Wire Notes Line
	3500 6875 3500 5225
Wire Notes Line
	3500 5225 1200 5225
Wire Notes Line
	3050 5775 1250 5775
Wire Notes Line
	1250 5775 1250 6825
Wire Notes Line
	1250 6825 3050 6825
Wire Notes Line
	3050 6825 3050 5775
Text GLabel 4750 6475 0    60   Input ~ 0
EncA
Text GLabel 4750 6600 0    60   Input ~ 0
EncB
Text GLabel 4750 6725 0    60   Input ~ 0
EncSw
Entry Wire Line
	5450 6475 5550 6575
Entry Wire Line
	5450 6600 5550 6700
Entry Wire Line
	5450 6725 5550 6825
Text Label 5400 6475 2    60   ~ 0
Iron+
Text Label 5400 6600 2    60   ~ 0
Iron-
Text Label 5400 6725 2    60   ~ 0
HotAir+
Wire Wire Line
	4750 6725 5450 6725
Wire Wire Line
	4750 6600 5450 6600
Wire Wire Line
	4750 6475 5450 6475
$EndSCHEMATC
